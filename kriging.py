import argparse
import json
import os

import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import time
import pickle

from pykrige.ok import OrdinaryKriging
from pykrige.uk import UniversalKriging
import datetime
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from piq import VSILoss, MDSILoss, MultiScaleGMSDLoss, FSIMLoss

from dataset_generation import DatasetCamsKri, DatasetAladinEurKri, Distrib, Filter, PlotPrep, PrepKriDataFromSparse
import warnings
warnings.filterwarnings("ignore", 'The original MDSI supports only RGB images. The input images were converted to RGB by copying the grey channel 3 times.')
warnings.filterwarnings("ignore", 'The original VSI supports only RGB images. The input images were converted to RGB by copying the grey channel 3 times.')
warnings.filterwarnings("ignore", "torch.meshgrid: in an upcoming release, it will be required to pass the indexing argument.")
warnings.filterwarnings("ignore", 'Matplotlib is currently using agg, which is a non-GUI backend, so cannot show the figure.')

matplotlib.use('Agg')
plt.ioff()

# If you can, run this example on a GPU, it will be a lot faster.
cuda = torch.cuda.is_available()
device_name = "cuda" if cuda else "cpu"
device = torch.device(device_name)

parser = argparse.ArgumentParser()
# Choosing the version of the model (useful for saving)
parser.add_argument("--version", type=str, default="", help="version of the model")
# Choosing the aerosol type that will be considered here
aer_arg = parser.add_argument("--aer", type=str, default='pm', help="name of the aerosol considered "
        "(du for dust, bc for black carbon, om for  organic matter, ss for sea salt, su for sulphate aerosol or pm for total aerosol/particulate matter)")
# Choosing the type of kriging
parser.add_argument("--type", type=str, default="ordinary", help="type of kriging (one of ordinary, universal or co")
# Choosing the variogram model
parser.add_argument("--variogram", type=str, default="gaussian", help="variogram model (one of linear, power, exponential, gaussian, spherical, hole-effect) ")
# Choosing the percentage of values that should have a label (useful for random or randomOnLand bcType only)
p_arg = parser.add_argument("--prob_bc", type=float, default=5.0, help="percentage of values that should have a label (useful for random or randomOnLand bcType only)")
# Choosing the kind of boundary conditions we should be using (possible values are : naive, file, random, randomOnLand) (only useful for semi-supervised learning)
bct_arg = parser.add_argument("--bc_type", type=str, default="random", help="string defining the kind of boundary conditions we should be using "
                                                                            "(possible values are : naive, file, random, randomOnLand) (only useful for semi-supervised learning)")
# # Choosing the test period
# testPeriod_arg = parser.add_argument("--testPeriod", type=str, default="all", help="choosing test period (in [oct2020, jan2021, apr2021, jul2021, all]")
# Choosing data origin
parser.add_argument("--origData", type=str, default="Cams", help="data origin")
# Choosing if we apply a log to the inputs and targets or not
parser.add_argument("--log", type=bool, default=False, help="apply log scale to the inputs and targets")
# Choosing whether or not to normalize all inputs
parser.add_argument("--normal", type=bool, default=False, help="normalize all inputs")
# Choosing if we use geographic coordinates or not
parser.add_argument("--geog", type=bool, default=False, help="using geographic coordinates or not (euclidean)")
# Choosing to keep the exact values from the input or not
parser.add_argument("--keepBC", type=bool, default=True, help="keep the exact values from the input or not")
# Choosing the time step for the test set
parser.add_argument("--testStep", type=int, default=3, help="time step for test set")
# Dividing the dimensions by a ratio (Cams Forecast)
parser.add_argument("--ratio", type=int, default=1, help="dividing dim by ratio (Cams Forecast)")
# Choosing whether to load a pre-existing model or to train a new one
parser.add_argument("--load", type=bool, default=False, help="choosing whether to load a pre-existing model or to train a new one")
args = parser.parse_args()
print(args)

# Managing some errors
# if args.testPeriod not in ["oct2020", "jan2021", "apr2021", "jul2021", "all"]:
#     raise argparse.ArgumentError(testPeriod_arg, "testPeriod should be in [oct2020, jan2021, apr2021, jul2021, all]. \n "
#                                                 "Got "+str(args.testPeriod))

if args.bc_type not in ["naive", "file", "random", "randomOnLand"]:
    raise argparse.ArgumentError(bct_arg, "bc_type argument should be a string from the following list : \n"
                                          "[naive, file, random, randomOnFile] \n"
                                          "Got " + args.bc_type)

if args.prob_bc < 0 or args.prob_bc > 100:
    raise argparse.ArgumentError(p_arg, "prob_bc argument should be a number between 0 and 100. Got " + str(args.prob_bc))

if args.aer not in ["du", "bc", "om", "ss", "su", "pm"]:
    raise argparse.ArgumentError(aer_arg, "aer argument should be a string from the following list : \n"
                                 "[du, bc, om, ss, su, pm] \n"
                                 "Got " + args.aer)

dict_Data = {'YEAR':{'train':{'sfc':'Cams/cams-global-reanalysis-year_2020-07-01_2021-05-31_levtype_sfc.nc',
                               'pl':'Cams/cams-global-reanalysis-year_2020-07-01_2021-05-31_levtype_pl.nc'},
                     'trainGen':{'sfc':'Cams/cams-global-reanalysis_2021-06-01_2021-06-30_levtype_sfc.nc',
                                   'pl':'Cams/cams-global-reanalysis_2021-06-01_2021-06-30_levtype_pl.nc'},
                     'test':[{'sfc':'Cams/cams_reanalysis_year-testSet_2021-07-01_2022-07-01_levtype_sfc.nc',
                                'pl':'Cams/cams_reanalysis_year-testSet_2021-07-01_2022-07-01_levtype_pl.nc'}],
                     'testPeriod':['YEAR'],
                      'trainStartDate':datetime.datetime(2019, 7, 1), 'trainEndDate':datetime.datetime(2020, 6, 1),
                      'trainGenStartDate':datetime.datetime(2020, 6, 1), 'trainGenEndDate':datetime.datetime(2020, 7, 1),
                      'testStartDate':datetime.datetime(2020, 7, 1), 'testEndDate':datetime.datetime(2021, 7, 1),
    'testDates' : ["202007010100-202010010000", "202010010100-202101010000", "202101010100-202104010000",
                 "202104010100-202107010000"],
'trainingDates' : ["201907010100-201910010000", "201910010100-202001010000", "202001010100-202004010000",
                 "202004010100-202007010000"]}}

testP = 'YEAR'
dict_Path = dict_Data[testP]
sfc_test_path = dict_Path['test'][0]['sfc']
pl_test_path = dict_Path['test'][0]['pl']

sfc_train_path = dict_Path['train']['sfc']
pl_train_path = dict_Path['train']['pl']

sfc_train_pathGen = dict_Path['trainGen']['sfc']
pl_train_pathGen = dict_Path['trainGen']['pl']

trainStartDate = dict_Path['trainStartDate']
trainEndDate = dict_Path['trainEndDate']
trainGenStartDate = dict_Path['trainGenStartDate']
trainGenEndDate = dict_Path['trainGenEndDate']
testStartDate = dict_Path['testStartDate']
testEndDate = dict_Path['testEndDate']

trainingDates = dict_Path['trainingDates']
testDates = dict_Path['testDates']

CoKriging = (args.type == "co")

# Choosing the model's name depending on the chosen options
if args.bc_type == "naive":
    args.version = "_BCN" + args.version
elif args.bc_type == "file":
    args.version = "_BCF" + args.version
elif args.bc_type == "random":
    args.version = "_BCR" + str(args.prob_bc) + args.version
elif args.bc_type == "randomOnLand":
    args.version = "_BCLandR" + str(args.prob_bc) + args.version
if args.geog:
    args.version = "_geog" + args.version
args.version = "_" + args.aer + args.version
if args.log:
    args.version = "_log" + args.version
args.version = "k_" + args.type + "Kriging_" + args.variogram + "Vario" +"_" + args.origData + args.version
name_model = args.version

def buildKrigingModel(bcSparse, lat=None, lon=None):
    """ This function allows us to build the kriging models, corresponding to the chosen type and available data.
        Notably, we observe a difference between the case where we use latitude and longitude values (in degrees or rad), and the case where the indices of the image are used instead"""
    if args.type == "ordinary":
        xBC, yBC, zBC = PrepKriDataFromSparse(bcSparse, lat, lon)
        if lat is not None and lon is not None:
            K = OrdinaryKriging(
                xBC,
                yBC,
                zBC,
                variogram_model=args.variogram,
                verbose=False,
                enable_plotting=True,
                exact_values=args.keepBC,
                coordinates_type='geographic'
            )
        else:
            K = OrdinaryKriging(
                xBC,
                yBC,
                zBC,
                variogram_model=args.variogram,
                verbose=False,
                enable_plotting=True,
                exact_values=args.keepBC
            )
    elif args.type == "universal":
        xBC, yBC, zBC = PrepKriDataFromSparse(bcSparse)
        K = UniversalKriging(
            xBC,
            yBC,
            zBC,
            variogram_model=args.variogram,
            verbose=False,
            enable_plotting=True,
            exact_values=False
        )
    return K

def saveResults2D(results, error, bw, e_bw, bc):
    """ This function allows us to generate pdf files of our 2D results."""
    for idx in range(len(results[0])):
        # We generate one file for each tuple (target, prediction)
        path = "results/" + name_model + "/results_test_img" + str(idx + 1) + ".pdf"
        pred1 = results[1][idx][0]
        targ1 = results[0][idx][0]
        pred = pred1
        targ = targ1
        # PlotPrep prepares the variables for plotting
        pred = PlotPrep()(pred, bc=bc, bw=bw, norm=True)
        targ = PlotPrep()(targ, bw=bw, norm=True)
        # Extracting the metrics so they can be displayed too
        loss = results[2][idx].item()
        r_loss = results[4][idx].item()
        mbe = results[8][idx].item()
        mbe_r = results[9][idx].item()
        sim_idx = results[10][idx].item()
        plt.suptitle("Test results.")
        # Each file contains two images : one for the prediction and one for the target
        if results[3] != None:
            # The input file is used for cokriging only
            input1 = results[3][idx][0]
            inp = input1
            inp = PlotPrep()(inp, norm=True)
            ax0 = plt.subplot(2, 2, 1)
            ax0.imshow(inp)
            ax0.set_title("Input (AOD)")
        ax1 = plt.subplot(2, 2, 3)
        ax1.imshow(pred)
        ax1.set_title(f"Prediction. Test error : {loss:1.3f}\u03BCg/m\N{superscript three} or {r_loss*100:1.3f}%."
                      f" \n MBE : {mbe:1.3f}\u03BCg/m\N{superscript three} or {mbe_r*100:1.3f}%."
                      f" \n sim_index loss : {sim_idx*100:1.3f}%.")
        ax2 = plt.subplot(2, 2, 4)
        ax2.imshow(targ)
        ax2.set_title("Target.")
        plt.savefig(path)
        plt.close()
        if error:
            ct = results[5][idx][0]/4
            cpr = results[6][idx][0]/4
            ce = results[7][idx].item()
            ct = PlotPrep()(ct)
            cpr = PlotPrep()(cpr)
            # We generate one file for each tuple (target, prediction)
            path2 = "results/" + name_model + "/error_test_img" + str(idx + 1) + ".pdf"
            errorMat = torch.abs(targ1-pred1)
            errorMat = PlotPrep()(errorMat, bc=bc, error=True, bw=e_bw, norm=True)
            plt.suptitle("Test error visualisation.")
            # Each file contains 1 column of images, for error visualisation
            # Which means we have 1 image per file (error visualisation)
            ax0 = plt.subplot(2, 2, 1)
            if e_bw:
                ax0.imshow(errorMat, cmap='gray')
            else:
                ax0.imshow(errorMat)
            ax0.set_title(f"Error visualisation. Test error : {loss:1.3f}\u03BCg/m\N{superscript three} or {r_loss*100:1.3f}%.")
            ax1 = plt.subplot(2, 2, 3)
            ax1.imshow(cpr)
            ax1.set_title(f"Segmented prediction. \n Classification error : {ce:1.3f}.")
            ax2 = plt.subplot(2, 2, 4)
            ax2.imshow(ct)
            ax2.set_title("Segmented target.")
            plt.savefig(path2)
            plt.close()
        print("File n°" + str(idx + 1) + " saved !")

def kriModels(sfc_test_path, pl_test_path, testDates, geog=False):
    if not args.load:
        # Building the dataset
        if args.origData=="Cams":
            test_dataset = DatasetCamsKri(pathSfcList=[sfc_test_path], pathPlList=[pl_test_path],
                                                        bcType=args.bc_type, bcP=args.prob_bc, var=args.aer, cokri=CoKriging,
                                                        log=args.log)
        elif args.origData == "Al-EUR":
            test_dataset = DatasetAladinEurKri(list_dates=testDates, end_date=None, step=args.testStep, cokri=CoKriging,
                                               log=args.log, normal=args.normal)
        test_loader = DataLoader(test_dataset, batch_size=1, shuffle=False)
        models = []
        for idx_batch, tup in enumerate(test_loader):
            if CoKriging:
                (_, _, bcSparse) = tup
            else:
                (_, bcSparse) = tup
            bcSparse = bcSparse[0]
            bcSparse = bcSparse.float()
            bcSparse = bcSparse.to(device)
            # Kriging models are built from the BC, and the coordinates if they are available
            if geog:
                K = buildKrigingModel(bcSparse, lat=test_dataset.latitude, lon=test_dataset.longitude)
            else:
                K = buildKrigingModel(bcSparse)
            # One kriging model is built for each sample (necessary as there is no training, each model is built for one single inference)
            models.append(K)
        # Saving all the models
        os.makedirs("savedModels/" + name_model + "/", exist_ok=True)
        os.makedirs("savedModels/" + name_model + "/", exist_ok=True)
        pickle.dump(models, open("savedModels/" + name_model + "/model.pkl", 'wb'))
    else:
        # Loading the model
        models = pickle.load(open("savedModels/" + name_model + "/model.pkl", 'rb'))
    return models

def testKriging(sfc_test_path, pl_test_path, testDates, save=True, error=True, bw=False, e_bw=False, geog=False, origData="Cams"):
    # Building the dataset
    if origData=="Cams":
        test_dataset = DatasetCamsKri(pathSfcList=[sfc_test_path], pathPlList=[pl_test_path],
                                                    bcType=args.bc_type, bcP=args.prob_bc, var=args.aer, cokri=CoKriging,
                                                    log=args.log)
        newMax = (test_dataset.maxTest - test_dataset.minTest)
    elif origData=="Al-EUR":
        test_dataset = DatasetAladinEurKri(list_dates=testDates, end_date=None, step=args.testStep, cokri=CoKriging,
                                        log=args.log, normal=args.normal)
        newMax = (test_dataset.maxTest - test_dataset.minTest)
    simLoss = FSIMLoss(chromatic=False, data_range=newMax)
    test_loader = DataLoader(test_dataset, batch_size=1, shuffle=False)
    predictions = []
    targets = []
    if CoKriging:
        inputs = []
    else:
        inputs = None
    losses = []
    r_losses = []
    times = []
    ctarg = []
    cpred = []
    closs = []
    MBE = []
    MBE_R = []
    sim_idces = []
    mse = nn.MSELoss(reduction='none')
    if args.log:
        dis = Distrib()
    # Creating the grid for the kriging model
    if geog:
        Xpts = test_dataset.longitude
        Ypts = test_dataset.latitude
    else:
        Xpts = np.arange(0, 480, 1.)
        Ypts = np.arange(0, 241, 1.)
    # Creating all the models for the dataset (one for each sample)
    models = kriModels(sfc_test_path, pl_test_path, testDates, geog)
    for idx_batch, tup in enumerate(test_loader):
        if CoKriging:
            (input_, target, _) = tup
            #input_ = input_[0]
            input_ = input_.float()
            input_ = input_.to(device)
            if args.log:
                input_ = dis.reverse(input_)
            inputs.append(input_)
        else:
            (target, _) = tup
        #target = target[0]
        target = target.float()
        target = target.to(device)
        # Selecting the model corresponding to this sample
        K = models[idx_batch]
        start_time = time.time()
        # Executing the model
        pred, sig = K.execute("grid", Xpts, Ypts)
        pred = torch.from_numpy(pred)
        pred_time = time.time() - start_time
        pred = pred[None, :]
        times.append(pred_time)
        pred = pred.to(device)
        if args.log:
            pred = dis.reverse(pred)
            target = dis.reverse(target)
        pred = Filter(pred)
        target = Filter(target)
        predictions.append(pred)
        targets.append(target)
        # Computing the quantized error
        q1, q2, q3 = torch.quantile(target, 0.25), torch.quantile(target, 0.5), torch.quantile(target, 0.75)
        ct = torch.where(target > q1, 1, 0) + torch.where(target > q2, 1, 0) + torch.where(target > q3, 1, 0)
        cpr = torch.where(pred > q1, 1, 0) + torch.where(pred > q2, 1, 0) + torch.where(pred > q3, 1, 0)
        ce = torch.mean(torch.abs(ct - cpr).float())
        ctarg.append(ct)
        cpred.append(cpr)
        closs.append(ce)
        mt = torch.mean(target)
        # Computing absolute and relative MAE
        mae = torch.sqrt(mse(pred, target))
        relative_mae = torch.mean(mae) / mt
        losses.append(torch.mean(mae))
        r_losses.append(relative_mae)
        # Computing absolute and relative MBE
        mbe = torch.mean(pred - target)
        mbe_r = mbe / mt
        predF1 = torch.nan_to_num(pred - torch.min(pred), nan=0, posinf=newMax, neginf=0)
        predF = ((predF1 > 0) * (predF1 < newMax)) * predF1 + (predF1 >= newMax) * newMax
        targetF1 = target - test_dataset.minTest
        targetF = (targetF1 > 0) * targetF1
        # Computing FSIM (similarity index taking the image patterns into account)
        sim_idx = simLoss(predF[None, :], targetF[None, :])
        sim_idces.append(sim_idx)
        MBE.append(mbe)
        MBE_R.append(mbe_r)
    results = [targets, predictions, losses, inputs, r_losses, ctarg, cpred, closs, MBE, MBE_R, sim_idces]
    losses = [x.item() for x in losses]
    r_losses = [x.item() for x in r_losses]
    closs = [x.item() for x in closs]
    MBE = [x.item() for x in MBE]
    MBE_R = [x.item() for x in MBE_R]
    sim_idces = [x.item() for x in sim_idces]
    # Saving all metrics in a dictionary
    dict_perf = {"Average inference time (s)": np.mean(times),
                 "STD of inference time (s)": np.std(times),
                 "MAE (\u03BCg/m\N{superscript three})": np.mean(losses),
                 "STD of MAE  (\u03BCg/m\N{superscript three})": np.std(losses),
                 "Relative MAE (%)": 100 * np.mean(r_losses),
                 "Average classification error": np.mean(closs),
                 "STD of classification error": np.std(closs),
                 "Average bias error (\u03BCg/m\N{superscript three})": np.mean(MBE),
                 "STD of bias error (\u03BCg/m\N{superscript three})": np.std(MBE),
                 "Average relative bias error (%)": 100 * np.mean(MBE_R),
                 "STD of relative bias error (%)": 100 * np.std(MBE_R),
                 "Average sim-index loss (%)": 100 * np.mean(sim_idces),
                 "STD of sim-index loss (%)": 100 * np.std(sim_idces)
                 }
    if save:
        os.makedirs("results/" + name_model + "/", exist_ok=True)
        # os.makedirs("results/" + name_model + "/" + testPeriod + "/", exist_ok=True)
        saveResults2D(results=results, error=error, e_bw=e_bw, bw=bw, bc=None)
        with open("results/" + name_model + "/perf_test.txt", "w+") as f:
            json.dump(dict_perf, f, indent=2)
            f.close()
    return dict_perf

# if args.testPeriod != "all":
perf = testKriging(sfc_test_path, pl_test_path, testDates, geog=args.geog)
# else:
#     for tPer in tPeriods:
#         perfPer = testKriging(tPer, geog=args.geog)
