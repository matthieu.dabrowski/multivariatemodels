# import libraries and modules
import argparse
import datetime
import json
import os
import math
import matplotlib
import matplotlib.pyplot as plt
# PyTorch libraries and modules
import torch
from torch.utils.data import DataLoader
# local libraries and modules
from dataset_generation import DatasetCams, DatasetCamsHybrid, DatasetCamsGAN, DatasetAladinEurGAN, DatasetAladinEur, tiling
from models import UNet_2Dto2D, Cls_2Dto2D, CGAN, FFUNet_2Dto2D, FFCls_2Dto2D, DFUNet_2Dto2D, DFCls_2Dto2D, HybridUNet1FF, HybridUNet2FF
# handle warnings
import warnings
warnings.filterwarnings("ignore", 'The original MDSI supports only RGB images. The input images were converted to RGB by copying the grey channel 3 times.')
warnings.filterwarnings("ignore", 'The original VSI supports only RGB images. The input images were converted to RGB by copying the grey channel 3 times.')
warnings.filterwarnings("ignore", "torch.meshgrid: in an upcoming release, it will be required to pass the indexing argument.")
warnings.filterwarnings("ignore", 'Matplotlib is currently using agg, which is a non-GUI backend, so cannot show the figure.')

matplotlib.use('Agg')
plt.ioff()

# If you can, run this example on a GPU, it will be a lot faster.
cuda = torch.cuda.is_available()
if not cuda:
    raise EnvironmentError("No GPU available")
device_name = "cuda" if cuda else "cpu"
device = torch.device(device_name)

parser = argparse.ArgumentParser()
# Choosing the version of the model (useful for saving)
parser.add_argument("--version", type=str, default="", help="version of the model")
# Choosing if we do a GAN or not
parser.add_argument("--GAN", type=bool, default=False, help="boolean defining if we do a GAN or not")
# Choosing if the AutoEncoder contains two (symetrical) linear layers or not
parser.add_argument("--linear_layer", type=bool, default=True, help="boolean defining if the AutoEncoder contains two (symetrical) linear layers or not")
# Choosing the size of the latent space (useless if linear_layer is False)
parser.add_argument("--latent_dim", type=int, default=128, help="size of the latent space (useless if linear_layer is False)")
# Choosing if we are doing semi-supervised learning or not
parser.add_argument("--semi_sup", type=bool, default=False, help="boolean defining if the training is semi-supervised or not")
# Choosing in case of a GAN if the discriminator should have a supervised loss (useless if not using GAN)
parser.add_argument("--disc_sup", type=bool, default=False, help="boolean defining if the discriminator should have a supervised loss (useless if not using GAN)")
# Choosing the kind of boundary conditions we should be using (possible values are : naive, file, random, randomOnLand) (only useful for semi-supervised learning)
bct_arg = parser.add_argument("--bc_type", type=str, default="random", help="string defining the kind of boundary conditions we should be using "
                                                                            "(possible values are : naive, file, random, randomOnLand) (only useful for semi-supervised learning)")
# Choosing if we use the hard or soft method to deal with BC (only useful for semi-supervised learning)
parser.add_argument("--hard_bc", type=bool, default=False, help="boolean defining if we use the hard or soft method to deal with BC "
                                                               "(only useful for semi-supervised learning)")
# Choosing the percentage of values that should have a label (useful for random or randomOnLand bcType only)
p_arg = parser.add_argument("--prob_bc", type=float, default=5.0, help="percentage of values that should have a label (useful for random or randomOnLand bcType only)")
# Choosing if the model should have skip connections or not
parser.add_argument("--skip_co", type=bool, default=True, help="boolean defining if the model should have skip connections or not")
# Choosing if the model should have dense skip connections or not
parser.add_argument("--dense_skip_co", type=bool, default=False, help="boolean defining if the model should have dense skip connections or not")
# Choosing the batch_size for training and testing
parser.add_argument("--batch_size", type=int, default=32, help="size of the batches for testing and training")
# Choosing the number of epochs
parser.add_argument("--num_epochs", type=int, default=500, help="number of epochs for training")
# (nb of iterations = NUM_EPOCHS*(dataset.__len__()/BATCH_SIZE))
# Choosing the aerosol type that will be considered here
aer_arg = parser.add_argument("--aer", type=str, default='pm', help="name of the aerosol considered "
        "(du for dust, bc for black carbon, om for  organic matter, ss for sea salt, su for sulphate aerosol or pm for total aerosol/particulate matter)")
# Choosing the prediction horizon (must be 0 or a multiple of 3 as data is obtained every 3 hours)
hor_arg = parser.add_argument("--horizon", type=int, default=0, help="prediction horizon (must be 0 or a multiple of 3 as data is obtained every 3 hours)")
# Choosing the kernel sizes for convolution layers (convolution layers are symmetrical in our AEs)
kS1_arg = parser.add_argument("--kS1", type=int, default=9, help="kernel size for first convolution layer") #27
kS2_arg = parser.add_argument("--kS2", type=int, default=7, help="kernel size for second convolution layer") #14
kS3_arg = parser.add_argument("--kS3", type=int, default=7, help="kernel size for third convolution layer")
kS4_arg = parser.add_argument("--kS4", type=int, default=3, help="kernel size for fourth convolution layer")
# Stride
parser.add_argument("--stride", type=int, default=5, help="stride for convolution layers") #7
# Choosing how the number of channels in each successive convolution channel evolves
parser.add_argument("--chan_mult", type=int, default=3, help="number by which to multiply the number of channels in each successive convolution")
# Choosing the learning rate
parser.add_argument("--lr", type=float, default=1e-2, help="learning rate")
# Choosing train period (season & year)
trainPeriod_arg = parser.add_argument("--trainPeriod", type=str, default="YEAR", help="choosing train period (season & year or YEAR)")
# Choosing the test period
testPeriod_arg = parser.add_argument("--testPeriod", type=str, default="next", help="choosing test period (next or all)")
# Choosing the time step for the test set
parser.add_argument("--testStep", type=int, default=3, help="time step for test set")
# Choosing the time step for the training set
parser.add_argument("--trainStep", type=int, default=1, help="time step for training set")
# Choosing whether to take the same complete training periods for semi-supervised and supervised
parser.add_argument("--matchTrainPeriods", type=bool, default=False, help="take the same complete training periods for semi-supervised and supervised")
# Choosing if we apply a log to the inputs and targets or not
parser.add_argument("--log", type=bool, default=True, help="apply log scale to the inputs and targets")
# Choosing whether or not to normalize all inputs
parser.add_argument("--normal", type=bool, default=False, help="normalize all inputs")
# The next entries are about giving additional info to the model
parser.add_argument("--pressure", type=bool, default=False, help="use surface pressure")
parser.add_argument("--temperature", type=bool, default=False, help="use temperature")
parser.add_argument("--humidity", type=bool, default=False, help="use relative humidity")
parser.add_argument("--wind", type=bool, default=False, help="use winds")
parser.add_argument("--angstrom", type=bool, default=False, help="use angstrom coefficient")
# Choosing whether to extract norm and angle from wind data or to keep info as is (no effect is args.wind is False)
parser.add_argument("--extract_norm_wind", type=bool, default=True, help="extract norm and angle from wind data (True) or keep info as is (no effect is args.wind is False)")
# Choose data source (Cams or Al-EUR)
origData_arg = parser.add_argument("--origData", type=str, default='Cams', help="choose data source")
# Choose fusion type ('CC', 'FF', 'DF' or 'Hybrid3F', 'Hybrid2F', 'Hybrid1F')
parser.add_argument("--fusion", type=str, default='CC', help='fusion type')
# Choosing whether to use FSIM as additional loss for the training
parser.add_argument("--FSIMloss", type=bool, default=False, help="choosing whether to use FSIM as additional loss for training")
# Choosing whether to save all plot results
parser.add_argument("--unfilter", type=bool, default=False, help="choosing whether to save all plot results")
# Choosing whether to load a pre-existing model or to train a new one
parser.add_argument("--load", type=bool, default=False, help="choosing whether to load a pre-existing model or to train a new one")
# Using Air Quality as a metric
parser.add_argument("--QA", type=bool, default=False, help="using air quality as a metric")
args = parser.parse_args()
print(args)

if args.testPeriod not in ["next", "all"]:
    raise argparse.ArgumentError(testPeriod_arg, "testPeriod should be next or all. \n "
                                                "Got "+str(args.testPeriod))

if args.horizon%3 != 0:
    raise argparse.ArgumentError(hor_arg, "prediction horizon should be 0 or a multiple of 3 as data is obtained every 3 hours. \n"
                                          "Got "+str(args.horizon))

# Depending on the data source, the variables' names can be different
# This is why we create two lists of parameters
add_vars_C = []
add_vars_Al = []
n_av = 0
s_av = ""
if args.pressure:
    add_vars_C.append("sp")
    add_vars_Al.append("psl")
    n_av += 1
    s_av = s_av + "P"
if args.temperature:
    add_vars_C.append("t2m")
    add_vars_Al.append("tas")
    n_av += 1
    s_av = s_av + "T"
if args.humidity:
    add_vars_C.append("r")
    add_vars_Al.append("hurs")
    n_av += 1
    s_av = s_av + "H"
if args.wind:
    add_vars_C.append("wind")
    add_vars_Al.append("wind")
    n_av += 2
    if args.extract_norm_wind:
        s_av = s_av + "nW"
    else:
        s_av = s_av + "W"
if args.angstrom:
    add_vars_C.append("angstrom")
    add_vars_Al.append("angstrom")
    n_av += 1
    s_av = s_av + "Ang"
# Some variables are not found in the Cams data
if args.boundaryLayer:
    if args.origData == "Cams":
        raise argparse.ArgumentError(origData_arg, "This value can not be found in the chosen data")
    add_vars_Al.append("bldep")
    n_av += 1
    s_av = s_av + "BL"
if args.asym:
    if args.origData == "Cams":
        raise argparse.ArgumentError(origData_arg, "This value can not be found in the chosen data")
    add_vars_Al.append("asy550aer")
    n_av += 1
    s_av = s_av + "Asym"
if args.scat:
    if args.origData == "Cams":
        raise argparse.ArgumentError(origData_arg, "This value can not be found in the chosen data")
    add_vars_Al.append("ssa550aer")
    n_av += 1
    s_av = s_av + "Scatter"

if args.origData == "Al-LOCAL":
    args.testPeriod = 'YEAR'
    args.version = "_Al-LOCAL" + args.version
    add_vars = add_vars_Al
elif args.origData == "Al-EUR":
    args.testPeriod = 'YEAR'
    args.version = "_Al-EUR" + args.version
    add_vars = add_vars_Al
else:
    add_vars = add_vars_C

# Managing errors
if args.linear_layer:
    kSarglist = [kS1_arg, kS2_arg, kS3_arg, kS4_arg]
    kSlist = [args.kS1, args.kS2, args.kS3, args.kS4]
    for i in range(4):
        kS = kSlist[i]
        kSarg = kSarglist[i]
        if (kS <= 1) :
            raise argparse.ArgumentError(kSarg, "Kernel sizes should be greater than one. \n "
                                                  "Kernel size for layer "+str((i+1))+" is " + str(kS) + ".")

if args.bc_type not in ["naive", "file", "random", "randomOnLand"]:
    raise argparse.ArgumentError(bct_arg, "bc_type argument should be a string from the following list : \n"
                                          "[naive, file, random, randomOnFile] \n"
                                          "Got " + args.bc_type)

if args.prob_bc < 0 or args.prob_bc > 100:
    raise argparse.ArgumentError(p_arg, "prob_bc argument should be a number between 0 and 100. Got " + str(args.prob_bc))

if args.aer not in ["du", "bc", "om", "ss", "su", "pm"]:
    raise argparse.ArgumentError(aer_arg, "aer argument should be a string from the following list : \n"
                                 "[du, bc, om, ss, su, pm] \n"
                                 "Got " + args.aer)


args.semi_sup = (args.semi_sup or args.GAN)

# Managing model name
if args.semi_sup:
    if args.bc_type == "naive":
        args.version = "BCN" + args.version
    elif args.bc_type == "file":
        args.version = "BCF" + args.version
    elif args.bc_type == "random":
        args.version = "BCR" + str(args.prob_bc) + args.version
    elif args.bc_type == "randomOnLand":
        args.version = "BCLandR" + str(args.prob_bc) + args.version
    if args.hard_bc:
        args.version = "_hard" + args.version
    else:
        args.version = "_soft" + args.version
    args.version = "_SemiS" + args.version
if (args.GAN and args.disc_sup):
    args.version = "_dSup" + args.version
if args.FSIMloss:
    args.version = "_FSIMloss" + args.version
if args.linear_layer:
    args.version = "_LL" + args.version
if args.horizon != 0:
    args.version = "_pred" + str(args.horizon) + "h"+args.version
if n_av > 0:
    args.version = "_av-" + s_av + args.version
args.version = "_" + args.aer + args.version
if args.log:
    args.version = "_log" + args.version
if args.normal:
    args.version = "-Normalized" + args.version
if args.matchTrainPeriods:
    args.version = "_M" + str(args.trainPeriod) + args.version
else:
    args.version = "_"+str(args.trainPeriod)+args.version

# This dictionary contains all file names and dates necessary to build the datasets (for both training and test)
dict_Data = {
             'YEAR':{'train':{'sfc':'Cams/cams-global-reanalysis-year_2020-07-01_2021-05-31_levtype_sfc.nc',
                               'pl':'Cams/cams-global-reanalysis-year_2020-07-01_2021-05-31_levtype_pl.nc'},
                     'trainGen':{'sfc':'Cams/cams-global-reanalysis_2021-06-01_2021-06-30_levtype_sfc.nc',
                                   'pl':'Cams/cams-global-reanalysis_2021-06-01_2021-06-30_levtype_pl.nc'},
                     'test':[{'sfc':'Cams/cams_reanalysis_year-testSet_2021-07-01_2022-07-01_levtype_sfc.nc',
                                'pl':'Cams/cams_reanalysis_year-testSet_2021-07-01_2022-07-01_levtype_pl.nc'}],
                     'testPeriod':['YEAR'],
                      'trainStartDate':datetime.datetime(2019, 7, 1), 'trainEndDate':datetime.datetime(2020, 6, 1),
                      'trainGenStartDate':datetime.datetime(2020, 6, 1), 'trainGenEndDate':datetime.datetime(2020, 7, 1),
                      'testStartDate':datetime.datetime(2020, 7, 1), 'testEndDate':datetime.datetime(2021, 7, 1),
    'testDates' : ["202007010100-202010010000", "202010010100-202101010000", "202101010100-202104010000",
                 "202104010100-202107010000"],
'trainingDates' : ["201907010100-201910010000", "201910010100-202001010000", "202001010100-202004010000",
                 "202004010100-202007010000"]}
             }

if args.trainPeriod not in dict_Data.keys():
    raise argparse.ArgumentError(trainPeriod_arg, "trainPeriod should be one of [sum20, aut20, win21, spr21, YEAR]. \n"
                                                  "Got "+str(args.trainPeriod))

dict_Path = dict_Data[args.trainPeriod]

# Extracting all the necessary file names and dates
sfc_train_path = dict_Path['train']['sfc']
pl_train_path = dict_Path['train']['pl']

sfc_train_pathGen = dict_Path['trainGen']['sfc']
pl_train_pathGen = dict_Path['trainGen']['pl']

trainStartDate = dict_Path['trainStartDate']
trainEndDate = dict_Path['trainEndDate']
trainGenStartDate = dict_Path['trainGenStartDate']
trainGenEndDate = dict_Path['trainGenEndDate']
testStartDate = dict_Path['testStartDate']
testEndDate = dict_Path['testEndDate']

trainingDates = dict_Path['trainingDates']
testDates = dict_Path['testDates']

args.parallel=True


if args.GAN:
    # Creating the dataset corresponding to the chosen data and model
    if args.origData == "Al-EUR":
        if args.matchTrainPeriods:
            train_dataset = DatasetAladinEurGAN(list_dates=trainingDates, start_date_Gen=trainGenStartDate,
                                                end_date_Disc=None,
                                                add_vars=add_vars, extract_norm_wind=args.extract_norm_wind,
                                                log=args.log, horizon=args.horizon, step=args.trainStep, normal=args.normal)
        else:
            train_dataset = DatasetAladinEurGAN(list_dates=trainingDates, start_date_Gen=trainGenStartDate,
                                                end_date_Disc=trainEndDate,
                                                add_vars=add_vars, extract_norm_wind=args.extract_norm_wind,
                                                log=args.log, horizon=args.horizon, step=args.trainStep, normal=args.normal)
    else:
        if args.matchTrainPeriods:
            train_dataset = DatasetCamsGAN(pathSfcGenList=[sfc_train_pathGen],
                                           pathPlGenList=[pl_train_pathGen],
                                           pathSfcDiscList=[sfc_train_path],
                                           pathPlDiscList=[pl_train_path],
                                           var=args.aer, horizon=args.horizon, log=args.log,
                                           add_vars=add_vars, extract_norm_wind=args.extract_norm_wind, normal=args.normal)
        else:
            train_dataset = DatasetCamsGAN(pathSfcGenList=[sfc_train_pathGen],
                                           pathPlGenList=[pl_train_pathGen],
                                           pathSfcDiscList=[sfc_train_pathGen, sfc_train_path],
                                           pathPlDiscList=[pl_train_pathGen, pl_train_path],
                                           var=args.aer, horizon=args.horizon, log=args.log, add_vars=add_vars,
                                           extract_norm_wind=args.extract_norm_wind, normal=args.normal)

    if args.tiling is False:    args.height, args.width = train_dataset.height, train_dataset.width
    # Two of the model's layers include a stride. The feature obtained after these two layers needs to be big enough for the following convolution layer to work
    while math.ceil(args.height / (args.stride ** 2)) < args.kS4 or math.ceil(
            args.width / (args.stride ** 2)) < args.kS4:
        # If it's not big enough, then we need to reduce the stride
        args.stride = args.stride - 2

    if args.skip_co:
        if args.fusion == 'FF':
            # Parallel training does not seem to work with Feature Fusion models
            gen = FFUNet_2Dto2D(name_model="Aod2SfcAer-2DUFF_GAN-Gen" + args.version, device=device,
                                linear_layer=args.linear_layer, latent_dim=args.latent_dim, ws=args.semi_sup,
                                bcType=args.bc_type, hardBC=args.hard_bc, p=args.prob_bc,
                                kS1=args.kS1, kS2=args.kS2, kS3=args.kS3, kS4=args.kS4,
                                chan_mult=args.chan_mult, lr=args.lr, simLoss=args.FSIMloss, height=args.height, width=args.width, stride=args.stride, inp_chan=1+n_av, origData=args.origData)

            disc = FFCls_2Dto2D(name_model="Aod2SfcAer-2D_GAN-brDisc" + args.version, device=device,
                                chan_mult=args.chan_mult, kS1=args.kS1, kS2=args.kS2, kS3=args.kS3,
                                latent_dim=args.latent_dim, lr=args.lr, dSup=args.disc_sup, height=args.height, width=args.width, stride=args.stride, inp_chan=1+n_av)

            model = CGAN(name_model="Aod2SfcAer-2D-UFF_GAN" + args.version, device=device,
                         generator=gen, discriminator=disc)
        elif args.fusion == 'DF':
            gen = DFUNet_2Dto2D(name_model="Aod2SfcAer-2DUDF_GAN-Gen" + args.version, device=device,
                              linear_layer=args.linear_layer, latent_dim=args.latent_dim, ws=args.semi_sup,
                              bcType=args.bc_type, hardBC=args.hard_bc, p=args.prob_bc,
                              kS1=args.kS1, kS2=args.kS2, kS3=args.kS3, kS4=args.kS4,
                              chan_mult=args.chan_mult, lr=args.lr, simLoss=args.FSIMloss, height=args.height, width=args.width, stride=args.stride, inp_chan=1+n_av, origData=args.origData)

            disc = DFCls_2Dto2D(name_model="Aod2SfcAer-2D_GAN-dfDisc" + args.version, device=device,
                                       chan_mult=args.chan_mult, kS1=args.kS1, kS2=args.kS2, kS3=args.kS3,
                                       latent_dim=args.latent_dim, lr=args.lr, dSup=args.disc_sup, height=args.height, width=args.width, stride=args.stride, inp_chan=1+n_av)

            model = CGAN(name_model="Aod2SfcAer-2D-UDF_GAN" + args.version, device=device,
                         generator=gen, discriminator=disc)
        elif args.fusion == 'CC':
            # Creating the generator
            gen = UNet_2Dto2D(name_model="Aod2SfcAer-2DUNet_GAN-Gen" + args.version, device=device,
                              linear_layer=args.linear_layer, latent_dim=args.latent_dim, ws=args.semi_sup,
                              bcType=args.bc_type, hardBC=args.hard_bc, p=args.prob_bc,
                              kS1=args.kS1, kS2=args.kS2, kS3=args.kS3, kS4=args.kS4,
                              chan_mult=args.chan_mult, lr=args.lr, simLoss=args.FSIMloss, height=args.height, width=args.width, stride=args.stride, inp_chan=1+n_av, origData=args.origData)

            # Creating the discriminator
            disc = Cls_2Dto2D(name_model="Aod2SfcAer-2D_GAN-Disc" + args.version, device=device,
                              chan_mult=args.chan_mult, kS1=args.kS1, kS2=args.kS2, kS3=args.kS3,
                              latent_dim=args.latent_dim, lr=args.lr, dSup=args.disc_sup, height=args.height, width=args.width, stride=args.stride, inp_chan=1+n_av)

            # Creating the GANs from the two models
            model = CGAN(name_model="Aod2SfcAer-2D-U_GAN" + args.version, device=device,
                         generator=gen, discriminator=disc)

    # Sending all models to device
    gen = gen.to(device)
    disc = disc.to(device)
    model = model.to(device)

else:

    # Creating the dataset corresponding to the chosen data and model
    if args.origData == "Al-EUR":
        if args.matchTrainPeriods:
            train_dataset = DatasetAladinEur(list_dates=trainingDates, end_date=None, add_vars=add_vars,
                                             extract_norm_wind=args.extract_norm_wind, log=args.log,
                                             horizon=args.horizon, step=args.trainStep, normal=args.normal)
        else:
            train_dataset = DatasetAladinEur(list_dates=trainingDates, end_date=trainEndDate, add_vars=add_vars,
                                             extract_norm_wind=args.extract_norm_wind, log=args.log,
                                             horizon=args.horizon, step=args.trainStep, normal=args.normal)
    else:
        if args.matchTrainPeriods:
            if 'Hybrid' in args.fusion:
                train_dataset = DatasetCamsHybrid(pathSfcList=[sfc_train_pathGen, sfc_train_path],
                                                  pathPlList=[pl_train_pathGen, pl_train_path],
                                                  var=args.aer, horizon=args.horizon, log=args.log,
                                                  extract_norm_wind=args.extract_norm_wind, normal=args.normal)
            else:
                train_dataset = DatasetCams(pathSfcList=[sfc_train_pathGen, sfc_train_path],
                                            pathPlList=[pl_train_pathGen, pl_train_path],
                                            var=args.aer, horizon=args.horizon, log=args.log, add_vars=add_vars,
                                            extract_norm_wind=args.extract_norm_wind, normal=args.normal)

        else:
            if 'Hybrid' in args.fusion:
                train_dataset = DatasetCamsHybrid(pathSfcList=[sfc_train_path],
                                            pathPlList=[pl_train_path],
                                            var=args.aer, horizon=args.horizon, log=args.log,
                                            extract_norm_wind=args.extract_norm_wind, normal=args.normal)
            else:
                train_dataset = DatasetCams(pathSfcList=[sfc_train_path],
                                        pathPlList=[pl_train_path],
                                        var=args.aer, horizon=args.horizon, log=args.log, add_vars=add_vars,
                                        extract_norm_wind=args.extract_norm_wind, normal=args.normal)

    args.height, args.width = train_dataset.height, train_dataset.width
    # Two of the model's layers include a stride. The feature obtained after these two layers needs to be big enough for the following convolution layer to work
    while math.ceil(args.height / (args.stride ** 2)) < args.kS4 or math.ceil(
            args.width / (args.stride ** 2)) < args.kS4:
        # If it's not big enough, then we need to reduce the stride
        args.stride = args.stride - 2

    if args.skip_co:
        # Directly creating the model
        if args.fusion == 'FF':
            # Parallel training does not seem to work with Feature Fusion models
            model = FFUNet_2Dto2D(name_model="Aod2SfcAer-2DUFF" + args.version, device=device,
                                  linear_layer=args.linear_layer, latent_dim=args.latent_dim, ws=args.semi_sup,
                                  bcType=args.bc_type, hardBC=args.hard_bc, p=args.prob_bc,
                                  kS1=args.kS1, kS2=args.kS2, kS3=args.kS3, kS4=args.kS4,
                                  chan_mult=args.chan_mult, lr=args.lr, simLoss=args.FSIMloss, height=args.height, width=args.width, stride=args.stride, inp_chan=1+n_av, origData=args.origData)
        elif args.fusion == 'DF':
            model = DFUNet_2Dto2D(name_model="Aod2SfcAer-2DUDF" + args.version, device=device,
                                         linear_layer=args.linear_layer, latent_dim=args.latent_dim,
                                         ws=args.semi_sup,
                                         bcType=args.bc_type, hardBC=args.hard_bc, p=args.prob_bc,
                                         kS1=args.kS1, kS2=args.kS2, kS3=args.kS3, kS4=args.kS4,
                                         chan_mult=args.chan_mult, lr=args.lr, simLoss=args.FSIMloss, height=args.height, width=args.width, stride=args.stride, inp_chan=1+n_av, origData=args.origData)
        elif args.fusion == 'CC':
            model = UNet_2Dto2D(name_model="Aod2SfcAer-2DUNet" + args.version, device=device,
                              linear_layer=args.linear_layer, latent_dim=args.latent_dim, ws=args.semi_sup,
                              bcType=args.bc_type, hardBC=args.hard_bc, p=args.prob_bc,
                              kS1=args.kS1, kS2=args.kS2, kS3=args.kS3, kS4=args.kS4,
                              chan_mult=args.chan_mult, lr=args.lr, simLoss=args.FSIMloss, height=args.height, width=args.width, stride=args.stride, inp_chan=1+n_av, origData=args.origData)

        elif args.fusion == 'Hybrid1F':
            model = HybridUNet1FF(name_model="Aod2SfcAer-Hybrid1F" + args.version, device=device,
                                linear_layer=args.linear_layer, latent_dim=args.latent_dim, ws=args.semi_sup,
                                bcType=args.bc_type, hardBC=args.hard_bc, p=args.prob_bc,
                                kS1=args.kS1, kS2=args.kS2, kS3=args.kS3, kS4=args.kS4,
                                chan_mult=args.chan_mult, lr=args.lr, simLoss=args.FSIMloss, height=args.height,
                                width=args.width, stride=args.stride, origData=args.origData)

        elif args.fusion == 'Hybrid2F':
            model = HybridUNet2FF(name_model="Aod2SfcAer-Hybrid2F" + args.version, device=device,
                                  linear_layer=args.linear_layer, latent_dim=args.latent_dim, ws=args.semi_sup,
                                  bcType=args.bc_type, hardBC=args.hard_bc, p=args.prob_bc,
                                  kS1=args.kS1, kS2=args.kS2, kS3=args.kS3, kS4=args.kS4,
                                  chan_mult=args.chan_mult, lr=args.lr, simLoss=args.FSIMloss, height=args.height,
                                  width=args.width, stride=args.stride, origData=args.origData)

        elif args.fusion == 'Hybrid3F':
            model = HybridUNet3FF(name_model="Aod2SfcAer-Hybrid3F" + args.version, device=device,
                                  linear_layer=args.linear_layer, latent_dim=args.latent_dim, ws=args.semi_sup,
                                  bcType=args.bc_type, hardBC=args.hard_bc, p=args.prob_bc,
                                  kS1=args.kS1, kS2=args.kS2, kS3=args.kS3, kS4=args.kS4,
                                  chan_mult=args.chan_mult, lr=args.lr, simLoss=args.FSIMloss, height=args.height,
                                  width=args.width, stride=args.stride, origData=args.origData)

    # Sending the model to device
    model = model.to(device)

train_loader = DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True)

if args.GAN:
    if not args.load:
        # Training
        losses_train = model.trainGAN(dataLoader=train_loader, num_epochs=args.num_epochs, parallel=args.parallel)
    else:
        # Loading
        model.loadGAN()
    # Testing
    # if args.testPeriod == "next" or args.testPeriod == "YEAR":
    sfc_test_path = dict_Path['test'][0]['sfc']
    pl_test_path = dict_Path['test'][0]['pl']
    testP = dict_Path['testPeriod'][0]
    if args.origData == "Al-EUR":
        test_dataset = DatasetAladinEur(list_dates=testDates, end_date=None, add_vars=add_vars, step=args.testStep, horizon=args.horizon,
                                        log=args.log, extract_norm_wind=args.extract_norm_wind, normal=args.normal)
    else:
        test_dataset = DatasetCams(pathSfcList=[sfc_test_path],
                                                 pathPlList=[pl_test_path],
                                                 var=args.aer, horizon=args.horizon, log=args.log,
                                                 add_vars=add_vars, extract_norm_wind=args.extract_norm_wind, normal=args.normal)
    test_loader = DataLoader(test_dataset, batch_size=1, shuffle=False)
    model.testGAN(dataLoader=test_loader, testDateList=test_dataset.listDates, testPeriod=testP, maxTarg=test_dataset.maxTest, minTarg=test_dataset.minTest,
                                 error=True, e_bw=True, log=args.log, unfilter=args.unfilter, QA=args.QA)
    # else:
    #     for i in range(len(dict_Path['test'])):
    #         sfc_testPath_i = dict_Path['test'][i]['sfc']
    #         pl_testPath_i = dict_Path['test'][i]['pl']
    #         testPeriod_i = dict_Path['testPeriod'][i]
    #         test_dataset = DatasetCams(pathSfcList=[sfc_testPath_i],
    #                                                  pathPlList=[pl_testPath_i],
    #                                                  var=args.aer, horizon=args.horizon, log=args.log, add_vars=add_vars, extract_norm_wind=args.extract_norm_wind, normal=args.normal)
    #         test_loader = DataLoader(test_dataset, batch_size=1, shuffle=False)
    #         model.testGAN(dataLoader=test_loader, testDateList=test_dataset.listDates, testPeriod=testPeriod_i, maxTarg=test_dataset.maxTest, minTarg=test_dataset.minTest,
    #                                      error=True, e_bw=True, log=args.log, unfilter=args.unfilter)
else:
    if not args.load:
        # Training
        losses_train = model.trainModel(dataLoader=train_loader, trainDateList=train_dataset.listDates, num_epochs=args.num_epochs, maxTarg=train_dataset.maxTrain, minTarg=train_dataset.minTrain, parallel=args.parallel)
    else:
        # Loading
        model.loadModel()
    # Testing
    # if args.testPeriod == "next" or args.testPeriod == "YEAR":
    sfc_test_path = dict_Path['test'][0]['sfc']
    pl_test_path = dict_Path['test'][0]['pl']
    # sfc_test_path = "Cams/SeasonalDataset/cams_reanalysis_testSet_2021-07-01_2021-07-02_levtype_sfc.nc"
    # pl_test_path = "Cams/SeasonalDataset/cams_reanalysis_testSet_2021-07-01_2021-07-02_levtype_pl.nc"
    testP = dict_Path['testPeriod'][0]
    if args.origData == "Al-EUR":
        test_dataset = DatasetAladinEur(list_dates=testDates, end_date=None, add_vars=add_vars, step=args.testStep, horizon=args.horizon,
                                        log=args.log, extract_norm_wind=args.extract_norm_wind, normal=args.normal)
    else:
        if 'Hybrid' in args.fusion:
            test_dataset = DatasetCamsHybrid(pathSfcList=[sfc_test_path],
                                       pathPlList=[pl_test_path],
                                       var=args.aer, horizon=args.horizon, log=args.log,
                                       extract_norm_wind=args.extract_norm_wind, normal=args.normal)
        else:
            test_dataset = DatasetCams(pathSfcList=[sfc_test_path],
                                                     pathPlList=[pl_test_path],
                                                     var=args.aer, horizon=args.horizon, log=args.log,
                                                     add_vars=add_vars, extract_norm_wind=args.extract_norm_wind, normal=args.normal)
    test_loader = DataLoader(test_dataset, batch_size=1, shuffle=False)
    model.test(dataLoader=test_loader, testDateList=test_dataset.listDates, testPeriod=testP, maxTarg=test_dataset.maxTest, minTarg=test_dataset.minTest,
                              error=True, e_bw=True, log=args.log, unfilter=args.unfilter, QA=args.QA)
    # else:
    #     for i in range(len(dict_Path['test'])):
    #         sfc_testPath_i = dict_Path['test'][i]['sfc']
    #         pl_testPath_i = dict_Path['test'][i]['pl']
    #         testPeriod_i = dict_Path['testPeriod'][i]
    #         test_dataset = DatasetCams(pathSfcList=[sfc_testPath_i],
    #                                                  pathPlList=[pl_testPath_i],
    #                                                  var=args.aer, horizon=args.horizon, log=args.log, add_vars=add_vars, extract_norm_wind=args.extract_norm_wind, normal=args.normal)
    #         test_loader = DataLoader(test_dataset, batch_size=1, shuffle=False)
    #         model.test(dataLoader=test_loader, testDateList=test_dataset.listDates, testPeriod=testPeriod_i, maxTarg=test_dataset.maxTest, minTarg=test_dataset.minTest,
    #                                   error=True, e_bw=True, log=args.log, unfilter=args.unfilter)

# Saving the arguments
os.makedirs(model.sourceFolder + model.name_model + "/", exist_ok=True)
with open(model.sourceFolder + model.name_model + "/arguments.txt", "w+") as f:
    json.dump(args.__dict__, f, indent=2)
    f.close()