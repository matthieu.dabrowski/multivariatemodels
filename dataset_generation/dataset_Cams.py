# Importing libraries and modules
import datetime
# Import libraries
import matplotlib.pyplot as plt
import numpy as np
import torch
from netCDF4 import Dataset as netcdfFile
from torch.utils.data import Dataset
from .transforms import MRtoConc2D, FilterAod, MultAod, ConvPM, Distrib
from torchvision import transforms
np_eps = np.finfo(float).eps
plt.ioff()

# If you can, run this example on a GPU, it will be a lot faster.
cuda = torch.cuda.is_available()
device_name = "cuda" if cuda else "cpu"
device = torch.device(device_name)

# dictionary of all variable names for the type of aerosol considered
list_aermr_tot = ["aermr02", "aermr03", "aermr05", "aermr06", "aermr07", "aermr08", "aermr09", "aermr10", "aermr11"]
dict_aer = {"du": (["aermr04", "aermr05", "aermr06"], "duaod550"),
            "bc": (["aermr09", "aermr10"], "bcaod550"),
            "om": (["aermr07", "aermr08"], "omaod550"),
            "ss": (["aermr01", "aermr02", "aermr03"], "ssaod550"),
            "su": (["aermr11"], "suaod550"),
            "pm": (["pm2p5"], "aod550")}

def hours2dateCams(nb_hours):
    """ Converts numbers of hours (as used in our data) to datetime format. """
    # The initial date is 01/01/1900
    return datetime.datetime(1900, 1, 1, 0) + datetime.timedelta(hours=int(nb_hours))

def fromArray_hours2dateCamsArray(list_nb_hours):
    """ Converts a list of number of hours (as used in our data) to a set of dates in datetime format."""
    list_dates = []
    for i in range(len(list_nb_hours)):
        list_dates.append(hours2dateCams(nb_hours=int(list_nb_hours[i])))
    return np.array(list_dates)

def normalize(t):
    t = np.array(t)
    return (t-np.min(t))/(np.max(t)-np.min(t))

class DatasetCams(Dataset):
    """ Object inheriting from the torch.Dataset class, customized to fit our need.
        Uses inputs and targets from data files.
        Specificity of the DatasetCamsGroundTarget class : for the target, we only take a matrix corresponding to the surface/ground level values.
        Indeed, here we will build a model that will only try to predict the targeted values at ground level, and the other values, corresponding to higher levels, are irrelevant.
    """

    def __init__(self, pathSfcList, pathPlList, var, add_vars=None, extract_norm_wind=True, horizon=0, log=False, normal=False):
        """ Creates a Dataset object.
            input:
                pathSfcList: paths to the NetCDF files with sfc level type (as values can be located in several different files corresponding to different periods of time)
                pathPlList: paths to the NetCDF files with pl level type
                var: name of the NetCDF variable that will predicted (must be present in file_sfc)
                add_var (optional) : additional variables to use as input
                extract_norm_wind (optional) : when using the wind speed, if this is True then norm and angle of wind speed are used rather than speeds of wind northbound and eastbound
                horizon (optional) : horizon of prediction
                log (optional) : determines if a logarithm should be applied on the values to smooth their distribution
        """
        if add_vars is None:
            add_vars = []
        # Getting the name of the values to extract from the files
        aod = dict_aer[var][1]
        list_var = dict_aer[var][0]
        Time = np.array([])
        for j in range(len(pathSfcList)):
            pathSfc = pathSfcList[j]
            pathPl = pathPlList[j]
            file_sfc = netcdfFile(pathSfc, "r", format="NETCDF4")
            Time = np.concatenate((Time, file_sfc.variables['time'][:]))
            file_pl = netcdfFile(pathPl, "r", format="NETCDF4")
            # for each file, extract aod and aerosol concentration
            InputVarTemp = file_sfc.variables[aod][:]
            if var == "pm":
                TargetVarTemp = np.zeros_like(file_sfc.variables[list_var[0]][:])
                for varT in list_var:
                    TargetVarTemp += file_sfc.variables[varT][:]
            else:
                # We are only interested in the aerosol concentration at ground level
                TargetVarTemp = np.zeros_like(file_pl.variables[list_var[0]][:, -1])
                for varT in list_var:
                    TargetVarTemp += file_pl.variables[varT][:, -1]
            if len(add_vars) > 0:
                # Adding additional input variables
                i = 0
                for av in add_vars:
                    if av == "r":
                        # humidity is found in the "levtype_pl" file, while other additional variables are from the "levtype_sfc" file
                        tmp_var = file_pl.variables[av][:]
                        tmp_var = tmp_var[:, None]
                        if normal:
                            tmp_var = normalize(tmp_var)
                        if i == 0:
                            AddVarTemp = tmp_var
                        else:
                            AddVarTemp = np.concatenate((AddVarTemp, tmp_var), 1)
                    elif av == "wind":
                        u10 = file_sfc.variables['u10'][:]
                        v10 = file_sfc.variables['v10'][:]
                        if extract_norm_wind:
                            # Computing norm and angle of wind sped
                            norm = np.sqrt(u10 ** 2 + v10 ** 2)
                            dir = np.arctan(v10 / u10) * (180 / np.pi)
                            norm = norm[:, None]
                            dir = dir[:, None]
                            if normal:
                                norm = normalize(norm)
                                dir = normalize(dir)
                            tmp_var = np.concatenate((norm, dir), 1)
                        else:
                            # Keeping the wind speed variables as they are
                            u10 = u10[:, None]
                            v10 = v10[:, None]
                            if normal:
                                u10 = normalize(u10)
                                v10 = normalize(v10)
                            tmp_var = np.concatenate((u10, v10), 1)
                        if i == 0:
                            AddVarTemp = tmp_var
                        else:
                            AddVarTemp = np.concatenate((AddVarTemp, tmp_var), 1)
                    elif av == "angstrom":
                        # Computing the angstöm coefficient from two AOD at two different wavelengths
                        aod865 = file_sfc.variables["aod865"][:]
                        # Filtering out any negative values
                        aod865 = np.where(aod865 <= 0, np_eps, aod865)
                        InputVarTempB = np.where(InputVarTemp <= 0, np_eps, InputVarTemp)
                        # The next line corresponds to the formula to use to compute the angström coefficient
                        tmp_var = (-1 / np.log(865 / 550)) * np.log(aod865 / InputVarTempB)
                        tmp_var = np.expand_dims(tmp_var, axis=1)
                        if normal:
                            tmp_var = normalize(tmp_var)
                        if i == 0:
                            AddVarTemp = tmp_var
                        else:
                            AddVarTemp = np.concatenate((AddVarTemp, tmp_var), 1)
                    else:
                        tmp_var = file_sfc.variables[av][:]
                        tmp_var = tmp_var[:, None]
                        if av == "sp":
                            # Converting pressure values in atm (instead of Pa)
                            tmp_var = tmp_var/101325
                        elif av == "t2m":
                            # Converting temperature values in °C (instead of K)
                            tmp_var = tmp_var-273.15
                        if normal:
                            tmp_var = normalize(tmp_var)
                        if i == 0:
                            AddVarTemp = tmp_var
                        else:
                            AddVarTemp = np.concatenate((AddVarTemp, tmp_var), 1)
                    i += 1
            else:
                self.AddVar = None
            # Concatenating the different variables across all files and time periods
            if j == 0:
                self.InputVar = InputVarTemp
                self.TargetVar = TargetVarTemp
                if len(add_vars) > 0:
                    self.AddVar = AddVarTemp
            else:
                self.InputVar = np.concatenate((self.InputVar, InputVarTemp), 0)
                self.TargetVar = np.concatenate((self.TargetVar, TargetVarTemp), 0)
                if len(add_vars) > 0:
                    self.AddVar = np.concatenate((self.AddVar, AddVarTemp), 0)
        sh = self.InputVar.shape
        self.height = sh[1]
        self.width = sh[2]
        # self.listDates is a list allowing us to extract the corresponding date for each sample (useful during the test, to display the results)
        self.listDates = fromArray_hours2dateCamsArray(Time)
        self.transf = transforms.Compose([FilterAod(), MultAod()])
        self.var = var
        if var != "pm":
            self.target_transf = transforms.Compose([MRtoConc2D()])
        else:
            self.target_transf = transforms.Compose([ConvPM()])
        self.h_i = horizon//3 # horizon is in hours, self.h_i represents a step
        # saving the min and max of our targetVar is useful for one of our metrics
        self.maxTest = torch.max(self.target_transf(torch.from_numpy(self.TargetVar))).float()
        self.minTest = torch.min(self.target_transf(torch.from_numpy(self.TargetVar))).float()
        self.meanTest = torch.mean(self.target_transf(torch.from_numpy(self.TargetVar))).float()
        self.maxTrain = self.maxTest
        self.minTrain = self.minTest
        self.log = log
        if log:
            self.dis = Distrib()
            self.maxTrain = torch.max(self.dis(self.target_transf(torch.from_numpy(self.TargetVar)))).float()
            self.minTrain = torch.min(self.dis(self.target_transf(torch.from_numpy(self.TargetVar)))).float()

    def __len__(self):
        """ Returns the length of the Dataset. """
        return len(self.InputVar)-self.h_i

    def __getitem__(self, idx):
        """ Returns a pair (input_, target) from an index (idx).
            Useful for DataLoader."""
        input_ = torch.from_numpy(self.InputVar[idx:idx+1])
        # We apply transform to the input
        input_ = self.transf(input_)
        target = torch.from_numpy(self.TargetVar[idx+self.h_i:idx+self.h_i+1])
        # We apply target_transform to the target
        target = self.target_transf(target)
        if self.log:
            # applying the log to the input and target
            input_ = self.dis(input_)
            target = self.dis(target)
        if self.AddVar is not None:
            # if there are additional variables, they are concatenated with the input
            addVar = torch.from_numpy(self.AddVar[idx])
            input_ = torch.cat((input_, addVar), 0)
        return input_, target

class DatasetCamsGAN(Dataset):
    """ Object inheriting from the torch.Dataset class, customized to fit our need.
        Use inputs and targets from data files.
        Specificity of the CustomDatasetGroundTarget class : for the targetGen, we only take a matrix corresponding to the surface/ground level values.
        Indeed, here we will build a model that will only try to predict the targeted values at ground level, and the other values, corresponding to higher levels, are irrelevant.
    """

    def __init__(self, pathSfcGenList, pathPlGenList, pathSfcDiscList, pathPlDiscList, var, add_vars=None, extract_norm_wind=True, horizon=0, log=False, normal=False):
        """ Creates a Dataset object.
            input:
                pathSfcGenList: paths to the NetCDF files with sfc level type (for generator training)
                pathPlGenList: paths to the NetCDF files with pl level type (for generator training)
                pathSfcDiscList: paths to the NetCDF files with sfc level type (for discriminator training)
                pathPlDiscList: paths to the NetCDF files with pl level type (for discriminator training)
                var: name of the NetCDF variable that will be used as input (must be present in file_sfc)
                add_var (optional) : additional variables to use as input
                extract_norm_wind (optional) : when using the wind speed, if this is True then norm and angle of wind speed are used rather than speeds of wind northbound and eastbound
                horizon (optional) : horizon of prediction
                log (optional) : determines if a logarithm should be applied on the values to smooth their distribution
        """
        # Here, the same process as for DatasetCams are realised, but two times (one for the generator's samples, and one for the discriminator's samples)
        # We choose to work with one dataset object instance rather than two as this allows for the transition between generator's training and discriminator's training
        # to happen at the batch level, rather than at the epoch level, thus allowing for a faster training of the GAN in the beginning of the training
        if add_vars is None:
            add_vars = []
        list_var = dict_aer[var][0]
        TimeGen = np.array([])
        TimeDisc = np.array([])
        for j in range(len(pathSfcGenList)):
            pathSfcGen = pathSfcGenList[j]
            pathPlGen = pathPlGenList[j]
            pathSfcDisc = pathSfcDiscList[j]
            pathPlDisc = pathPlDiscList[j]
            file_sfc_Gen = netcdfFile(pathSfcGen, "r", format="NETCDF4")
            TimeGen = np.concatenate((TimeGen, file_sfc_Gen.variables['time'][:]))
            file_sfc_Disc = netcdfFile(pathSfcDisc, "r", format="NETCDF4")
            TimeDisc = np.concatenate((TimeDisc, file_sfc_Disc.variables['time'][:]))
            file_pl_Gen = netcdfFile(pathPlGen, "r", format="NETCDF4")
            file_pl_Disc = netcdfFile(pathPlDisc, "r", format="NETCDF4")
            aod = dict_aer[var][1]
            InputVarGenTemp = file_sfc_Gen.variables[aod][:]
            InputVarDiscTemp = file_sfc_Disc.variables[aod][:]
            if normal:
                InputVarGenTemp = normalize(InputVarGenTemp)
                InputVarDiscTemp = normalize(InputVarDiscTemp)
            if var == "pm":
                TargetVarGenTemp = np.zeros_like(file_sfc_Gen.variables[list_var[0]][:])
                TargetVarDiscTemp = np.zeros_like(file_sfc_Disc.variables[list_var[0]][:])
                for varT in list_var:
                    TargetVarGenTemp += file_sfc_Gen.variables[varT][:]
                    TargetVarDiscTemp += file_sfc_Disc.variables[varT][:]
            else:
                TargetVarGenTemp = np.zeros_like(file_pl_Gen.variables[list_var[0]][:, -1])
                TargetVarDiscTemp = np.zeros_like(file_pl_Disc.variables[list_var[0]][:, -1])
                for varT in list_var:
                    TargetVarGenTemp += file_pl_Gen.variables[varT][:, -1]
                    TargetVarDiscTemp += file_pl_Disc.variables[varT][:, -1]
            if len(add_vars) > 0:
                i = 0
                for av in add_vars:
                    if av == "r":
                        tmp_var_Gen = file_pl_Gen.variables[av][:]
                        tmp_var_Gen = tmp_var_Gen[:, None]
                        tmp_var_Disc = file_pl_Disc.variables[av][:]
                        tmp_var_Disc = tmp_var_Disc[:, None]
                        if normal:
                            tmp_varDisc = normalize(tmp_varDisc)
                            tmp_varGen = normalize(tmp_varGen)
                        if i == 0:
                            AddVarGenTemp = tmp_var_Gen
                            AddVarDiscTemp = tmp_var_Disc
                        else:
                            AddVarGenTemp = np.concatenate((AddVarGenTemp, tmp_var_Gen), 1)
                            AddVarDiscTemp = np.concatenate((AddVarDiscTemp, tmp_var_Disc), 1)
                    elif av == "wind":
                        u10Gen = file_sfc_Gen.variables['u10'][:]
                        v10Gen = file_sfc_Gen.variables['v10'][:]
                        u10Disc = file_sfc_Disc.variables['u10'][:]
                        v10Disc = file_sfc_Disc.variables['v10'][:]
                        if extract_norm_wind:
                            normGen = np.sqrt(u10Gen ** 2 + v10Gen ** 2)
                            dirGen = np.arctan(v10Gen / u10Gen) * (180 / np.pi)
                            normGen = normGen[:, None]
                            dirGen = dirGen[:, None]
                            normDisc = np.sqrt(u10Disc ** 2 + v10Disc ** 2)
                            dirDisc = np.arctan(v10Disc / u10Disc) * (180 / np.pi)
                            normDisc = normDisc[:, None]
                            dirDisc = dirDisc[:, None]
                            if normal:
                                normGen = normalize(normGen)
                                dirGen = normalize(dirGen)
                                normDisc = normalize(normDisc)
                                dirDisc = normalize(dirDisc)
                            tmp_var_Gen = np.concatenate((normGen, dirGen), 1)
                            tmp_var_Disc = np.concatenate((normDisc, dirDisc), 1)
                        else:
                            u10Gen = u10Gen[:, None]
                            v10Gen = v10Gen[:, None]
                            u10Disc = u10Disc[:, None]
                            v10Disc = v10Disc[:, None]
                            if normal:
                                u10Gen = normalize(u10Gen)
                                v10Gen = normalize(v10Gen)
                                u10Disc = normalize(u10Disc)
                                v10Disc = normalize(v10Disc)
                            tmp_var_Gen = np.concatenate((u10Gen, v10Gen), 1)
                            tmp_var_Disc = np.concatenate((u10Disc, v10Disc), 1)
                        if i == 0:
                            AddVarGenTemp = tmp_var_Gen
                            AddVarDiscTemp = tmp_var_Disc
                        else:
                            AddVarGenTemp = np.concatenate((AddVarGenTemp, tmp_var_Gen), 1)
                            AddVarDiscTemp = np.concatenate((AddVarDiscTemp, tmp_var_Disc), 1)
                    elif av == "angstrom":
                        aod865_Gen = file_sfc_Gen.variables["aod865"][:]
                        aod865_Gen = np.where(aod865_Gen <= 0, np_eps, aod865_Gen)
                        InputVarGenTempB = np.where(InputVarGenTemp <= 0, np_eps, InputVarGenTemp)
                        tmp_var_Gen = (-1 / np.log(865 / 550)) * np.log(aod865_Gen / InputVarGenTempB)
                        tmp_var_Gen = np.expand_dims(tmp_var_Gen, axis=1)
                        aod865_Disc = file_sfc_Disc.variables["aod865"][:]
                        aod865_Disc = np.where(aod865_Disc <= 0, np_eps, aod865_Disc)
                        InputVarDiscTempB = np.where(InputVarDiscTemp <= 0, np_eps, InputVarDiscTemp)
                        tmp_var_Disc = (-1 / np.log(865 / 550)) * np.log(aod865_Disc / InputVarDiscTempB)
                        tmp_var_Disc = np.expand_dims(tmp_var_Disc, axis=1)
                        if normal:
                            tmp_varDisc = normalize(tmp_varDisc)
                            tmp_varGen = normalize(tmp_varGen)
                        if i == 0:
                            AddVarGenTemp = tmp_var_Gen
                            AddVarDiscTemp = tmp_var_Disc
                        else:
                            AddVarGenTemp = np.concatenate((AddVarGenTemp, tmp_var_Gen), 1)
                            AddVarDiscTemp = np.concatenate((AddVarDiscTemp, tmp_var_Disc), 1)
                    else:
                        tmp_var_Gen = file_sfc_Gen.variables[av][:]
                        tmp_var_Gen = tmp_var_Gen[:, None]
                        tmp_var_Disc = file_sfc_Disc.variables[av][:]
                        tmp_var_Disc = tmp_var_Disc[:, None]
                        if av == "sp":
                            # Conversion de la pression en atm (au lieu de Pa)
                            tmp_var_Gen = tmp_var_Gen/101325
                            tmp_var_Disc = tmp_var_Disc / 101325
                        elif av == "t2m":
                            # Conversion de la température en °C (au lieu de K)
                            tmp_var_Gen = tmp_var_Gen-273.15
                            tmp_var_Disc = tmp_var_Disc - 273.15
                        if normal:
                            tmp_varDisc = normalize(tmp_varDisc)
                            tmp_varGen = normalize(tmp_varGen)
                        if i == 0:
                            AddVarGenTemp = tmp_var_Gen
                            AddVarDiscTemp = tmp_var_Disc
                        else:
                            AddVarGenTemp = np.concatenate((AddVarGenTemp, tmp_var_Gen), 1)
                            AddVarDiscTemp = np.concatenate((AddVarDiscTemp, tmp_var_Disc), 1)
                    i += 1
            else:
                self.AddVarGen = None
                self.AddVarDisc = None
            if j == 0:
                self.InputVarGen = InputVarGenTemp
                self.InputVarDisc = InputVarDiscTemp
                self.TargetVarGen = TargetVarGenTemp
                self.TargetVarDisc = TargetVarDiscTemp
                if len(add_vars) > 0:
                    self.AddVarGen = AddVarGenTemp
                    self.AddVarDisc = AddVarDiscTemp
            else:
                self.InputVarGen = np.concatenate((self.InputVarGen, InputVarGenTemp), 0)
                self.InputVarDisc = np.concatenate((self.InputVarDisc, InputVarDiscTemp), 0)
                self.TargetVarGen = np.concatenate((self.TargetVarGen, TargetVarGenTemp), 0)
                self.TargetVarDisc = np.concatenate((self.TargetVarDisc, TargetVarDiscTemp), 0)
                if len(add_vars) > 0:
                    self.AddVarGen = np.concatenate((self.AddVarGen, AddVarGenTemp), 0)
                    self.AddVarDisc = np.concatenate((self.AddVarDisc, AddVarDiscTemp), 0)
        self.h_i = horizon // 3
        sh = self.InputVarGen.shape
        self.height = sh[1]
        self.width = sh[2]
        # We save two lists of dates as well since there are tow main period of time (one for the training of the generator, and the other for the discrminator)
        self.listDatesGen = fromArray_hours2dateCamsArray(TimeGen)
        self.listDatesDisc = fromArray_hours2dateCamsArray(TimeDisc)
        self.transf = transforms.Compose([FilterAod(), MultAod()])
        self.var = var
        if var == "pm":
            self.target_transf = transforms.Compose([ConvPM()])
        else:
            self.target_transf = transforms.Compose([MRtoConc2D()])
        maxTestGen = torch.max(self.target_transf(torch.from_numpy(self.TargetVarGen)))
        maxTestDisc = torch.max(self.target_transf(torch.from_numpy(self.TargetVarDisc)))
        self.maxTest = torch.max(maxTestGen, maxTestDisc).float()
        minTestGen = torch.min(self.target_transf(torch.from_numpy(self.TargetVarGen)))
        minTestDisc = torch.min(self.target_transf(torch.from_numpy(self.TargetVarDisc)))
        self.minTest = torch.min(minTestGen, minTestDisc).float()
        self.maxTrain = self.maxTest
        self.minTrain = self.minTest
        self.log = log
        if log:
            self.dis = Distrib()
            maxTrainGen = torch.max(self.dis(self.target_transf(torch.from_numpy(self.TargetVarGen))))
            maxTrainDisc = torch.max(self.dis(self.target_transf(torch.from_numpy(self.TargetVarDisc))))
            self.maxTrain = torch.max(maxTrainGen, maxTrainDisc).float()
            minTrainGen = torch.min(self.dis(self.target_transf(torch.from_numpy(self.TargetVarGen))))
            minTrainDisc = torch.min(self.dis(self.target_transf(torch.from_numpy(self.TargetVarDisc))))
            self.minTrain = torch.min(minTrainGen, minTrainDisc).float()

    def __lenGen__(self):
        return len(self.InputVarGen) - self.h_i

    def __lenDisc__(self):
        return len(self.InputVarDisc) - self.h_i

    def __len__(self):
        """ Returns the length of the Dataset. """
        self.len = max(self.__lenDisc__(), self.__lenGen__())
        self.smallerLen = min(self.__lenDisc__(), self.__lenGen__())
        if self.len == self.__lenDisc__():
            self.biggerSet = "disc"
        else:
            self.biggerSet = "gen"
        return self.len

    def __getitem__(self, idx):
        """ Returns a pair (input_, target) from an index (idx).
            Useful for DataLoader."""
        # One of the two sets (complete and sparse) may be bigger than the other
        # At each iteration, elements from both datasets are taken
        # This allows for both the generator and discriminator to train from each sample
        # Instead of going through one entire set then the other
        if self.biggerSet == "disc":
            inputDisc = torch.from_numpy(self.InputVarDisc[idx: idx + 1])
            targetDisc = torch.from_numpy(
                self.TargetVarDisc[idx + self.h_i:idx + self.h_i + 1])
            # We apply transform to the input
            inputDisc = self.transf(inputDisc)
            # We apply target_transform to the target
            targetDisc = self.target_transf(targetDisc)
            if self.log:
                inputDisc = self.dis(inputDisc)
                targetDisc = self.dis(targetDisc)
            if self.AddVarDisc is not None:
                # Adding additional variables
                addVarDisc = torch.from_numpy(self.AddVarDisc[idx])
                inputDisc = torch.cat((inputDisc, addVarDisc), 0)
            if idx < self.smallerLen:
                # We only take these elements if they exist
                # idx being based on the bigger set, it may be too high an index for the smaller set
                inputGen = torch.from_numpy(self.InputVarGen[idx: idx + 1])
                targetGen = torch.from_numpy(self.TargetVarGen[idx + self.h_i:idx + self.h_i + 1])
                # We apply transform to the input
                inputGen = self.transf(inputGen)
                # We apply target_transform to the target
                targetGen = self.target_transf(targetGen)
                if self.log:
                    inputGen = self.dis(inputGen)
                    targetGen = self.dis(targetGen)
                if self.AddVarGen is not None:
                    # Adding additional variables
                    addVarGen = torch.from_numpy(self.AddVarGen[idx])
                    inputGen = torch.cat((inputGen, addVarGen), 0)
            else:
                inputGen = None
                targetGen = None

        else:
            inputGen = torch.from_numpy(self.InputVarGen[idx:idx + 1])
            targetGen = torch.from_numpy(
                self.TargetVarGen[idx + self.h_i:idx + self.h_i + 1])
            # We apply transform to the input
            inputGen = self.transf(inputGen)
            # We apply target_transform to the target
            targetGen = self.target_transf(targetGen)
            if self.log:
                inputGen = self.dis(inputGen)
                targetGen = self.dis(targetGen)
            if self.AddVarGen is not None:
                # Adding additional variables
                addVarGen = torch.from_numpy(self.AddVarGen[idx])
                inputGen = torch.cat((inputGen, addVarGen), 0)
            if idx < self.smallerLen:
                # We only take these elements if they exist
                # idx being based on the bigger set, it may be too high an index for the smaller set
                inputDisc = torch.from_numpy(self.InputVarDisc[idx:idx + 1])
                targetDisc = torch.from_numpy(self.TargetVarDisc[idx + self.h_i:idx + self.h_i + 1])
                # We apply transform to the input
                inputDisc = self.transf(inputDisc)
                # We apply target_transform to the target
                targetDisc = self.target_transf(targetDisc)
                if self.log:
                    inputDisc = self.dis(inputDisc)
                    targetDisc = self.dis(targetDisc)
                if self.AddVarDisc is not None:
                    # Adding additional variables
                    addVarDisc = torch.from_numpy(self.AddVarDisc[idx])
                    inputDisc = torch.cat((inputDisc, addVarDisc), 0)
            else:
                inputDisc = None
                targetDisc = None

        return inputGen, targetGen, inputDisc, targetDisc






class DatasetCamsHybrid(Dataset):
    """ Object inheriting from the torch.Dataset class, customized to fit our need.
        Uses inputs and targets from data files.
        Specificity of the DatasetCamsGroundTarget class : for the target, we only take a matrix corresponding to the surface/ground level values.
        Indeed, here we will build a model that will only try to predict the targeted values at ground level, and the other values, corresponding to higher levels, are irrelevant.
    """

    def __init__(self, pathSfcList, pathPlList, var, extract_norm_wind=True, horizon=0, log=False, normal=False):
        """ Creates a Dataset object.
            input:
                pathSfcList: paths to the NetCDF files with sfc level type (as values can be located in several different files corresponding to different periods of time)
                pathPlList: paths to the NetCDF files with pl level type
                var: name of the NetCDF variable that will predicted (must be present in file_sfc)
                add_var (optional) : additional variables to use as input
                extract_norm_wind (optional) : when using the wind speed, if this is True then norm and angle of wind speed are used rather than speeds of wind northbound and eastbound
                horizon (optional) : horizon of prediction
                log (optional) : determines if a logarithm should be applied on the values to smooth their distribution
        """
        # Getting the name of the values to extract from the files
        aod = dict_aer[var][1]
        list_var = dict_aer[var][0]
        Time = np.array([])
        for j in range(len(pathSfcList)):
            pathSfc = pathSfcList[j]
            pathPl = pathPlList[j]
            file_sfc = netcdfFile(pathSfc, "r", format="NETCDF4")
            Time = np.concatenate((Time, file_sfc.variables['time'][:]))
            file_pl = netcdfFile(pathPl, "r", format="NETCDF4")
            # for each file, extract aod and aerosol concentration
            InputVarTemp = file_sfc.variables[aod][:]
            if var == "pm":
                TargetVarTemp = np.zeros_like(file_sfc.variables[list_var[0]][:])
                for varT in list_var:
                    TargetVarTemp += file_sfc.variables[varT][:]
            else:
                # We are only interested in the aerosol concentration at ground level
                TargetVarTemp = np.zeros_like(file_pl.variables[list_var[0]][:, -1])
                for varT in list_var:
                    TargetVarTemp += file_pl.variables[varT][:, -1]
            # humidity is found in the "levtype_pl" file, while other additional variables are from the "levtype_sfc" file
            humid = file_pl.variables["r"][:]
            humid = humid[:, None]
            if normal:
                humid = normalize(humid)
            AddVarTemp = humid
            u10 = file_sfc.variables['u10'][:]
            v10 = file_sfc.variables['v10'][:]
            if extract_norm_wind:
                # Computing norm and angle of wind sped
                norm = np.sqrt(u10 ** 2 + v10 ** 2)
                dir = np.arctan(v10 / u10) * (180 / np.pi)
                norm = norm[:, None]
                dir = dir[:, None]
                if normal:
                    norm = normalize(norm)
                    dir = normalize(dir)
                v1 = norm
                v2 = dir
            else:
                # Keeping the wind speed variables as they are
                u10 = u10[:, None]
                v10 = v10[:, None]
                if normal:
                    u10 = normalize(u10)
                    v10 = normalize(v10)
                v1 = u10
                v2 = v10
            # Computing the angstöm coefficient from two AOD at two different wavelengths
            aod865 = file_sfc.variables["aod865"][:]
            # Filtering out any negative values
            aod865 = np.where(aod865 <= 0, np_eps, aod865)
            InputVarTempB = np.where(InputVarTemp <= 0, np_eps, InputVarTemp)
            # The next line corresponds to the formula to use to compute the angström coefficient
            angs = (-1 / np.log(865 / 550)) * np.log(aod865 / InputVarTempB)
            angs = np.expand_dims(angs, axis=1)
            if normal:
                angs = normalize(angs)
            AddVarTemp = np.concatenate((AddVarTemp, angs), 1)
            press = file_sfc.variables["sp"][:]
            press = press[:, None]
            press = press/101325
            temp = file_sfc.variables["t2m"][:]
            temp = temp[:, None]
            temp = temp-273.15
            if normal:
                temp = normalize(temp)
                press = normalize(press)
            AddVarTemp = np.concatenate((AddVarTemp, press), 1)
            # Variables to add to FF
            AddVarTemp = np.concatenate((AddVarTemp, v1, v2, temp), 1)
            # Concatenating the different variables across all files and time periods
            if j == 0:
                self.InputVar = InputVarTemp
                self.TargetVar = TargetVarTemp
                self.AddVar = AddVarTemp
                # self.v1 = v1
                # self.v2 = v2
                # self.temp = temp
            else:
                self.InputVar = np.concatenate((self.InputVar, InputVarTemp), 0)
                self.TargetVar = np.concatenate((self.TargetVar, TargetVarTemp), 0)
                self.AddVar = np.concatenate((self.AddVar, AddVarTemp), 0)
                # self.v1 = np.concatenate((self.v1, v1), 0)
                # self.v2 = np.concatenate((self.v2, v2), 0)
                # self.temp = np.concatenate((self.temp, temp), 0)
        sh = self.InputVar.shape
        self.height = sh[1]
        self.width = sh[2]
        # self.listDates is a list allowing us to extract the corresponding date for each sample (useful during the test, to display the results)
        self.listDates = fromArray_hours2dateCamsArray(Time)
        self.transf = transforms.Compose([FilterAod(), MultAod()])
        self.var = var
        if var != "pm":
            self.target_transf = transforms.Compose([MRtoConc2D()])
        else:
            self.target_transf = transforms.Compose([ConvPM()])
        self.h_i = horizon//3 # horizon is in hours, self.h_i represents a step
        # saving the min and max of our targetVar is useful for one of our metrics
        self.maxTest = torch.max(self.target_transf(torch.from_numpy(self.TargetVar))).float()
        self.minTest = torch.min(self.target_transf(torch.from_numpy(self.TargetVar))).float()
        self.maxTrain = self.maxTest
        self.minTrain = self.minTest
        self.log = log
        if log:
            self.dis = Distrib()
            self.maxTrain = torch.max(self.dis(self.target_transf(torch.from_numpy(self.TargetVar)))).float()
            self.minTrain = torch.min(self.dis(self.target_transf(torch.from_numpy(self.TargetVar)))).float()

    def __len__(self):
        """ Returns the length of the Dataset. """
        return len(self.InputVar)-self.h_i

    def __getitem__(self, idx):
        """ Returns a pair (input_, target) from an index (idx).
            Useful for DataLoader."""
        input_ = torch.from_numpy(self.InputVar[idx:idx+1])
        # We apply transform to the input
        input_ = self.transf(input_)
        target = torch.from_numpy(self.TargetVar[idx+self.h_i:idx+self.h_i+1])
        # We apply target_transform to the target
        target = self.target_transf(target)
        if self.log:
            # applying the log to the input and target
            input_ = self.dis(input_)
            target = self.dis(target)
        # if there are additional variables, they are concatenated with the input
        addVar = torch.from_numpy(self.AddVar[idx])
        input_ = torch.cat((input_, addVar), 0)
        return input_, target