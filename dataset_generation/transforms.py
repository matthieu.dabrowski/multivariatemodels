# Importing libraries and modules
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import random as rd
import torch
import torch.nn as nn
from netCDF4 import Dataset as netcdfFile

# If you can, run this example on a GPU, it will be a lot faster.
cuda = torch.cuda.is_available()
device_name = "cuda" if cuda else "cpu"
device = torch.device(device_name)
cpu = torch.device("cpu")

class MRtoConc3D(object):
    """ Here we create a class that will be used as a transform to convert a mixing ratio matrix to a concentration matrix.
        Values in the output will be in ug/m³."""

    def __init__(self):
        self.helpText = "Custom transform converting Mixing Ratio (kg/kg) into Concentration (ug/m³) for a 3D Tensor."
        self.list_dens = list(pd.read_csv('Cams/Pressure_levels.csv')['Density [kg/m^3]'])

    def __call__(self, aermr: torch.Tensor):
        concentration = torch.zeros(aermr.shape)
        # Applying the conversion from formula from Mixing Ratio to Concentration
        for i in range(aermr.shape[1]):
            concentration[:, i] = (aermr[:, i] / (1 + aermr[:, i])) * self.list_dens[i] * (10**9)
        return concentration

    def __repr__(self):
        return self.helpText

class MRtoConc2D(object):
    """ Here we create a class that will be used as a transform to convert a mixing ratio matrix to a concentration matrix.
            Values in the output will be in ug/m³."""

    def __init__(self):
        self.helpText = "Custom transform converting Mixing Ratio (kg/kg) into Concentration (ug/m³) for a 2D Tensor."
        self.dens = list(pd.read_csv('Cams/Pressure_levels.csv')['Density [kg/m^3]'])[-1]

    def __call__(self, aermr: torch.Tensor):
        # Applying the conversion from formula from Mixing Ratio to Concentration
        concentration = (aermr / (1 + aermr)) * self.dens * (10 ** 9)
        return concentration

    def __repr__(self):
        return self.helpText

class ConvPM(object):
    """ Here we create a class that will be used as a transform to convert a concentration matrix from kg/m³ to ug/m³.
            Values in the output will be in ug/m³."""

    def __init__(self):
        self.helpText = "Custom transform Concentration (kg/m³) into Concentration (ug/m³) for a 2D Tensor."

    def __call__(self, pm: torch.Tensor):
        # Converting the concentration values from kg/m^3 to ug/m³
        concentration = pm * (10 ** 9)
        return concentration

    def __repr__(self):
        return self.helpText

class FilterAod(object):
    """ Here we create a class that will be used to filter out the noise from the aod values. """

    def __init__(self):
        self.helpText = "Custom transform filtering out noise from values of Aerosol Optical Depth."
        self.threshold = 0.005

    def __call__(self, aod: torch.Tensor):
        # all values of AOD below threshold are set to 0 (filtered)
        aod[aod < self.threshold] = 0.0
        return aod

    def __repr__(self):
        return self.helpText

def Filter(target: torch.Tensor):
    # all values of concentration below threshold are set to 0 (filtered)
    # this function is applied to concentration matrices
    threshold = 1
    mask = torch.where(target > threshold, 1, 0)
    return target * mask

class MultAod(object):
    """ This class allows us to multiply the aod values by 1000 in order to obtain values superior to one."""

    def __init__(self):
        self.helpText = "Custom transform multiplying the values of Aerosol Optical Depth by 10³."

    def __call__(self, aod: torch.Tensor):
        aod = aod*(10**3)
        return aod

    def __repr__(self):
        return self.helpText

def BCnaive():
    """ This class allows us to extract boundary conditions (of aerosol concentration) from the target values.
        Useful for weakly supervised training.
        This version uses the literal boundaries of the matrix/image tensor for boundary conditions."""
    netFile = netcdfFile("Cams/land-sea_mask.nc", "r", format="NETCDF4")
    shape = netFile.variables['lsm'][0].shape
    bc = torch.full(shape, False)
    bc[0, :] = True
    bc[-1, :] = True
    bc[:, 0] = True
    bc[:, -1] = True
    return bc

def BCrandom(p, origData='Cams', ratio=2):
    """ This class allows us to extract boundary conditions (of aerosol concentration) from the target values.
        Useful for weakly supervised training.
        This version uses target values at random points inside the matrix/image tensor."""
    if origData=='Al-LOCAL':
        netFile = netcdfFile("AladinClim/pm25_MED-11_ECMWF-ERA5_evaluation_r1i1p1_CNRM-ALADIN64_v1_1hrPt_200001010100-202107010000_domaineLOA.nc", "r", format="NETCDF4")
        shape = netFile.variables['pm25'][0].shape
    elif origData=='Cams':
        netFile = netcdfFile("Cams/land-sea_mask.nc", "r", format="NETCDF4")
        shape = netFile.variables['lsm'][0].shape
    elif origData=='Al-EUR':
        netFile = netcdfFile("AeroClim2/pm25_MED-11_ECMWF-ERA5_evaluation_r1i1p1_CNRM-ALADIN64_v1_1hrPt_200001010100-200101010000.nc")
        shape = netFile.variables['pm25'][0].shape
    else:
        netFile = netcdfFile("Cams/Forecast/train/cams_forecast_year_train_2021-07-01_2021-10-01_levtype_sfc.nc")
        shape1 = netFile.variables['pm2p5'][0].shape
        shape = (int(shape1[-2] / ratio), int(shape1[-1] / ratio))
    if p < 0 or p > 100:
        raise Exception("The argument p is a percentage, it should be an number between 0 and 100. Got ", p)
    else:
        p = p/100
    bc = torch.full(shape, False)
    n_lat = bc.shape[-2]
    n_lon = bc.shape[-1]
    # proba_bc x total nb of pixels = nb of pixels for bc
    q = int(n_lat * n_lon * p)
    coord = []
    while len(coord) < q:
        # We need q random pixels for our bc
        lat = rd.randint(0, n_lat-1)
        lon = rd.randint(0, n_lon-1)
        cd = (lat, lon)
        # Adding the location to the list only if it is not already in it
        if cd not in coord:
            coord.append(cd)
            bc[lat, lon] = True
    return bc

def BCrandomLand(p):
    """ This function allows us to extract boundary conditions (of aerosol concentration) from the target values.
        Useful for weakly supervised training.
        This version uses target values at random points inside the matrix/image tensor,
        except all points are located on the land and never on water.
        This is to better simulate real positions of sensors, which can usually be found only on the land."""
    file = netcdfFile("Cams/land-sea_mask.nc", "r", format="NETCDF4")
    # If the value of lsm matrix is greater than 0 we consider that we are on the land, else we are on water
    # We only have access to that information for Cams, so this is the only case where BcrandomLand can be used
    lsm = file.variables["lsm"][0] > 0
    # Therefore in variable lsm, True means we are on land and False that we are on the sea
    bc = torch.full(lsm.shape, False)
    n_lat = bc.shape[0]
    n_lon = bc.shape[1]
    if p < 0 or p > 100:
        raise Exception("The argument p is a percentage, it should be an number between 0 and 100. Got ", p)
    else:
        p = p/100
    # proba_bc x total nb of pixels = nb of pixels for bc
    q = int(n_lat * n_lon * p)
    coord = []
    while len(coord) < q:
        # We need q random pixels for our bc
        lat = rd.randint(0, n_lat-1)
        lon = rd.randint(0, n_lon-1)
        cd = (lat, lon)
        # Adding the location to the list only if it is not already in it
        # We add the point only if it is on land
        if cd not in coord and lsm[lat, lon]:
            coord.append(cd)
            bc[lat, lon] = True
    return bc

def BCfromFile():
    """ This class allows us to extract boundary conditions (of aerosol concentration) from the target values.
        Useful for weakly supervised training.
        This version uses the file station_locations.csv to get coordinates of real sensors,
        and uses the target values at those coordinates."""
    file = pd.read_csv('Cams/station_locations.csv')
    LongC = np.array(file['Longitude'])
    LatC = np.array(file['Latitude'])
    netFile = netcdfFile("Cams/land-sea_mask.nc", "r", format="NETCDF4")
    longitude = np.array(netFile.variables['longitude'][:])
    latitude = np.array(netFile.variables['latitude'][:])
    shape = netFile.variables['lsm'][0].shape
    tolLong = (longitude[1] - longitude[0])/2
    tolLat = (latitude[1] - latitude[0])/2
    bc = torch.full(shape, False)
    for long, lat in zip(LongC, LatC):
        long_i = np.where(np.isclose(long, longitude, atol=tolLong))[0][0]
        lat_j = np.where(np.isclose(lat, latitude, atol=tolLat))[0][0]
        bc[lat_j, long_i] = True
    return bc

class ApplyBC(object):
    """ This class allows us to apply known boundary conditions to a prediction.
        Useful for weakly supervised training.
        This alternative version uses other functions to apply BC. The goal is to see if it has a different effect on the results."""

    def __init__(self, bc: torch.Tensor):
        self.helpText = "Custom transform applying known boundary conditions to predicted values."
        self.bc = bc

    def bcTargPred(self):
        self.bcTarg = torch.where(self.bc, 1, 0)
        self.bcPred = torch.where(self.bc, 0, 1)

    def __call__(self, target: torch.Tensor, pred: torch.Tensor):
        if len(target.shape) == 3:
            self.bc = self.bc.repeat(target.shape[0], 1, 1)
        elif len(target.shape) == 4:
            self.bc = self.bc.repeat(target.shape[0], target.shape[1], 1, 1)
        else:
            n = len(target.shape)
            raise Exception("The target (and prediction) should have 3 or 4 dimensions (2D or 3D + channels). "
                            "\n Got " + str(n) + " dimensions. Target shape is : " + str(target.shape) + ". Target is : \n", target)
        self.bcTargPred()
        predBC = target*self.bcTarg + pred*self.bcPred
        return predBC

    def __repr__(self):
        return self.helpText

class BCloss(object):
    """ This class allows us to compute a loss based on boundary conditions to further guide training.
        Useful for weakly supervised training. Deals with BC using a 'soft' method."""

    def __init__(self, bc: torch.Tensor):
        self.helpText = "Custom transform useful to compute BC loss"
        self.bc = bc

    def __call__(self, target: torch.Tensor, pred: torch.Tensor):
        if len(target.shape) == 3:
            bc = self.bc.repeat(target.shape[0], 1, 1)
        elif len(target.shape) == 4:
            bc = self.bc.repeat(target.shape[0], target.shape[1], 1, 1)
        else:
            n = len(target.shape)
            raise Exception("The target (and prediction) should have 3 or 4 dimensions (2D or 3D + channels). "
                            "\n Got " + str(n) + " dimensions. Target shape is : " + str(target.shape) + ". Target is : \n",
                            target)
        tbc = torch.where(bc, 1, 0)
        return nn.MSELoss(reduction='none')(pred*tbc, target*tbc)

    def __repr__(self):
        return self.helpText

class PlotPrep(object):
    """ This class applies a treatment to our tensors so that they can be displayed.
        It also displays boundary conditions if needed."""

    def __init__(self):
        self.helpText = "Custom transform treating input tensors so they can be displayed, displaying boundary conditions as well if necessary."
        self.cmap_jet = plt.get_cmap("jet")
        self.cmap_gray = plt.get_cmap("gray")

    def __call__(self, t: torch.Tensor, bc=None, error=False, bw=False, norm=False, maxTarg=None, minTarg=None):
        t = t.float().to(device)
        n = len(t.shape)
        if n == 2:
            # Adding a third dimension will facilitate the next steps
            t = t[None, :]
            n = 3
        if bc is not None and len(bc.shape) == 2:
            # Same for bc
            bc = bc[None, :]
        # Modify the order of the dimensions to ease the printing
        if n == 3:
            c = t.shape[0]
            t = t.permute(1, 2, 0)
            if bc is not None:
                bc = bc.repeat(1, 1, 1).permute(1, 2, 0)
        elif n == 4:
            c = t.shape[0]
            t = t.permute(1, 2, 3, 0)
            if bc is not None:
                bc = bc.repeat(t.shape[0], 1, 1, 1).permute(0, 2, 3, 1)
        else:
            raise Exception("The input should have 3 or 4 dimensions (2D or 3D + channels). "
                            "\n Got " + str(n) + " dimensions. Tensor is : \n", t)
        maxVal = torch.amax(t) * 2 + 1
        # maxVal represents a theoretical max (255 for more classical images)
        if (c != 1) and (c != 2) and (c != 3):
            raise Exception("The input should have 1, 2 or 3 channels. Got : ", str(c))
        if (c == 2) and (not error):
            # If it has 2 channels : add an empty channel
            if n == 4:
                z = torch.zeros_like(t[:, :, :, 0:1])
            elif n == 3:
                z = torch.zeros_like(t[:, :, 0:1])
            t = torch.cat([t, z], dim=(n - 1))
            # Then display BC in a shade of purple/magenta (to maximize visibility)
            if bc is not None:
                if n == 4:
                    tR = torch.where(bc, maxVal / 10, t[:, :, :, 0:1])
                    tG = torch.where(bc, maxVal / 15, t[:, :, :, 1:2])
                    tB = torch.where(bc, maxVal, t[:, :, :, 2:3])
                elif n == 3:
                    tR = torch.where(bc, maxVal / 10, t[:, :, 0:1])
                    tG = torch.where(bc, maxVal / 15, t[:, :, 1:2])
                    tB = torch.where(bc, maxVal, t[:, :, 2:3])
                t = torch.cat([tR, tG, tB], dim=(n - 1))
            return t.to(cpu).detach().numpy()
        elif (c >= 2) and error:
            # Add channels between them
            t = torch.sum(t, dim=(n - 1), keepdim=True)
            c = 1
        if (c == 1):
            # Display values as a heatmap
            if n == 4:
                t = t[:, :, :, 0]
            elif n == 3:
                t = t[:, :, 0]
            if norm:
                # minTarg and maxTarg are either given or computed from t
                if minTarg==None:
                    minTarg = torch.min(t)
                else:
                    minTarg = torch.tensor(minTarg)
                minTarg = minTarg.to(device)
                if maxTarg==None:
                    maxTarg = torch.max(t)
                else:
                    maxTarg = torch.tensor(maxTarg)
                maxTarg = maxTarg.to(device)
                # all values of t are supposed to be between minTarg and maxTarg
                t = torch.where(t <= maxTarg, t, maxTarg)
                # then we normalize the values of t
                t = ((t - minTarg) / (maxTarg - minTarg))*25
            t = t.to(cpu).detach().numpy()
            # applying a cmap leads to the obtention of an image with 3 channels
            if bw:
                rgb_t = self.cmap_gray(t)
            else:
                rgb_t = self.cmap_jet(t)[:, :, 0:3]
            t = torch.from_numpy(rgb_t).float()
            c = 3
        if c == 3:
            # If it already has 3 channels: only display BC in white (to maximize visibility)
            if bc is not None:
                if not bw:
                    if n == 4:
                        tR = torch.where(bc, maxVal, t[:, :, :, 0:1])
                        tG = torch.where(bc, maxVal, t[:, :, :, 1:2])
                        tB = torch.where(bc, maxVal, t[:, :, :, 2:3])
                    elif n == 3:
                        tR = torch.where(bc, maxVal, t[:, :, 0:1])
                        tG = torch.where(bc, maxVal, t[:, :, 1:2])
                        tB = torch.where(bc, maxVal, t[:, :, 2:3])
                    t = torch.cat([tR, tG, tB], dim=(n - 1))
                else:
                    # If the image is in black and white then display BC in a shade of purple/magenta (to maximize visibility)
                    if n == 4:
                        tR = torch.where(bc, maxVal / 10, t[:, :, :, 0:1])
                        tG = torch.where(bc, maxVal / 15, t[:, :, :, 1:2])
                        tB = torch.where(bc, maxVal, t[:, :, :, 2:3])
                    elif n == 3:
                        tR = torch.where(bc, maxVal / 10, t[:, :, 0:1])
                        tG = torch.where(bc, maxVal / 15, t[:, :, 1:2])
                        tB = torch.where(bc, maxVal, t[:, :, 2:3])
                    t = torch.cat([tR, tG, tB], dim=(n - 1))
            return t.to(cpu).detach().numpy()

    def __repr__(self):
        return self.helpText

class Distrib(object):
    """ This class applies a transformation to the data to change its distribution. The reverse function applies the opposite transformation."""

    def __init__(self):
        self.torch_eps = 1e-7
        self.helpText = "This class applies a transformation to the data to change its distribution. The reverse function applies the opposite transformation."

    def __call__(self, t: torch.Tensor):
        t = t.float()
        # filtering values of t (values <= -1 are set to epsilon-1)
        t = torch.where(t <= -1, torch.tensor(self.torch_eps-1).float(), t)
        # applying log(t+1) so we can reverse it easily and 0s remain 0s
        return torch.log(t+1)

    def reverse(self, t: torch.Tensor):
        # reversing log(t+1)
        return torch.exp(t)-1

    def __repr__(self):
        return self.helpText

def PrepKriDataFromSparse(t: torch.Tensor, lat=None, lon=None):
    """ The purpose of this function is to produce data that is ready to be used for the kriging models """
    te = t.to(cpu)
    while te.shape[0] == 1:
        te = te[0]
    s = te.shape
    X = np.array([])
    Y = np.array([])
    Z = np.array([])
    # 2D
    if len(s) == 2:
        for i in range(s[0]):
            for j in range(s[1]):
                if te[i][j] != 0:
                    # X and Y either contain indices (for both dimensions of the image) or latitude and longitude
                    if lat is not None and lon is not None:
                        Y = np.append(Y, lat[i])
                        X = np.append(X, lon[j])
                    else:
                        Y = np.append(Y, i)
                        X = np.append(X, j)
                    # Z always contains values
                    Z = np.append(Z, te[i][j])
        # X and Y allow for the creation of a grid, and Z contains the values that can be found in this grid
        return X, Y, Z
    # 3D
    elif len(s) == 3:
        W = np.array([])
        for i in range(s[0]):
            for j in range(s[1]):
                for k in range(s[2]):
                    if te[i][j] != 0:
                        # X and Y either contain indices (for both dimensions of the image) or latitude and longitude
                        if lat is not None and lon is not None:
                            Y = np.append(Y, lat[i])
                            X = np.append(X, lon[j])
                        else:
                            Y = np.append(Y, i)
                            X = np.append(X, j)
                        # W contains the level or altitude
                        W = np.append(W, k)
                        # Z always contains values
                        Z = np.append(Z, te[i][j][k])
        # X, Y and W allow for the creation of a grid, and Z contains the values that can be found in this grid
        return X, Y, W, Z

class AirQualityQuantif(object):
    def __init__(self):
        self.thresholds = [12, 35.4, 55.4, 150.4, 250.4, 350.4]
        self.labels = [0, 1, 2, 3, 4, 5, 6]

    def __call__(self, t: torch.Tensor):
        q = torch.zeros_like(t)
        for thresh in self.thresholds:
            q += torch.where(t > thresh, 1, 0)
        return q