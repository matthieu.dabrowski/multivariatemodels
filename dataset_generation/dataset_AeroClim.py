# Importing libraries and modules
import datetime
# Import libraries
import matplotlib.pyplot as plt
import numpy as np
import torch
from netCDF4 import Dataset as netcdfFile
from torch.utils.data import Dataset
from dataset_generation.transforms import FilterAod, MultAod, ConvPM, Distrib
from torchvision import transforms
import os
np_eps = np.finfo(float).eps
plt.ioff()

def hours2dateAer(nb_hours):
    """ Converts numbers of hours (as used in our data) to datetime format. """
    # The initial date is 01/01/1900
    return datetime.datetime(1850, 1, 1, 0) + datetime.timedelta(days=float(nb_hours))

def fromArray_hours2dateAerArray(list_nb_hours):
    """ Converts a list of number of hours (as used in our data) to a set of dates in datetime format."""
    # list_nb_hours = np.array(list_nb_hours)
    list_dates = []
    for i in range(len(list_nb_hours)):
        # The initial date is 01/01/1900
        list_dates.append(hours2dateAer(list_nb_hours[i]))
    return np.array(list_dates)

class timeFormatter(object):
    """ This object allows for an easier extraction of the variables from the different files """
    def __init__(self, list_dates, start_date=None, end_date=None, step=1):
        pmS = "pm25"
        self.debFileName = "AeroClim2/"
        self.midFileName = "_MED-11_ECMWF-ERA5_evaluation_r1i1p1_CNRM-ALADIN64_v1_1hrPt_"
        self.ext = ".nc"
        self.listDates = np.array([])
        self.list_dates_strings = list_dates
        for i in range(len(list_dates)):
            # The name of the file depends both on the variables we want to extract and the chosen time period
            path = self.debFileName + pmS + self.midFileName + self.list_dates_strings[i] + self.ext
            if i == 0:
                self.pmTemp = netcdfFile(path, "r", format="NETCDF4").variables[pmS][:]
            else:
                self.pmTemp = np.concatenate((self.pmTemp, netcdfFile(path, "r", format="NETCDF4").variables[pmS][:]), axis=0)
            self.listDates = np.concatenate(
                (self.listDates, fromArray_hours2dateAerArray(netcdfFile(path, "r", format="NETCDF4").variables['time'][:])))
        # We get an initial list of dates, which will be useful to then select a time period from this list of dates
        if start_date == None and end_date == None:
            self.idxTime = np.where(self.listDates)[0][::step]
        elif end_date == None:
            self.idxTime = np.where(self.listDates >= start_date)[0][::step]
        elif start_date == None:
            self.idxTime = np.where(self.listDates <= end_date)[0][::step]
        else:
            self.idxTime = np.where(np.logical_and(self.listDates >= start_date, self.listDates <= end_date))[0][::step]
        self.listDates = self.listDates[self.idxTime]
        # We get pmTemp from the start (instead of using the getVar() function, as we need to extract the list of dates from a file anyway
        # and the pm concentration is always a useful value to obtain
        self.pmTemp = self.pmTemp[self.idxTime, :, :]

    def getListDates(self):
        return self.listDates

    def getPm(self):
        return self.pmTemp

    def getVar(self, var):
        for i in range(len(self.list_dates_strings)):
            # The name of the file depends both on the variables we want to extract and the chosen time period
            path = self.debFileName + var + self.midFileName + self.list_dates_strings[i] + self.ext
            if i == 0:
                varTemp = netcdfFile(path, "r", format="NETCDF4").variables[var][:]
            else:
                varTemp = np.concatenate((varTemp, netcdfFile(path, "r", format="NETCDF4").variables[var][:]), axis=0)
        # we use self.idxTime as it already corresponds to the chosen list of dates
        return varTemp[self.idxTime, :, :]

def normalize(t):
    t = np.array(t)
    return (t-np.min(t))/(np.max(t)-np.min(t))

class DatasetAladinEur(Dataset):
    # This time, we extract all variables using a timeFormatter object, which makes the DatasetAladinEur object easier to understand
    def __init__(self, list_dates, start_date=None, end_date=None, add_vars=None, extract_norm_wind=True, horizon=0, log=False, step=1, normal=False):
        if add_vars is None:
            add_vars = []
        tf = timeFormatter(list_dates=list_dates, start_date=start_date, end_date=end_date, step=step)
        self.listDates = tf.getListDates()
        self.TargetVar = tf.getPm()
        self.InputVar = tf.getVar('od550aer')
        if normal:
            self.InputVar = normalize(self.InputVar)
        if len(add_vars)>0:
            for i in range(len(add_vars)):
                av = add_vars[i]
                if av == "wind":
                    u10 = tf.getVar('uas')
                    v10 = tf.getVar('vas')
                    if extract_norm_wind:
                        norm = np.sqrt(u10 ** 2 + v10 ** 2)
                        dir = np.arctan(v10 / u10) * (180 / np.pi)
                        norm = norm[:, None]
                        dir = dir[:, None]
                        if normal:
                            norm = normalize(norm)
                            dir = normalize(dir)
                        tmp_var = np.concatenate((norm, dir), 1)
                    else:
                        u10 = u10[:, None]
                        v10 = v10[:, None]
                        if normal:
                            u10 = normalize(u10)
                            v10 = normalize(v10)
                        tmp_var = np.concatenate((u10, v10), 1)
                elif av == "angstrom":
                    aod1000 = tf.getVar('od1000aer')
                    aodB = np.where(self.InputVar <= 0, np_eps, self.InputVar)
                    aod1000 = np.where(aod1000 <= 0, np_eps, aod1000)
                    tmp_var = (-1 / np.log(1000 / 550)) * np.log(aod1000 / aodB)
                    if normal:
                        tmp_var = normalize(tmp_var)
                    tmp_var = np.expand_dims(tmp_var, axis=1)
                else:
                    tmp_var = tf.getVar(av)
                    if av == 'psl':
                        if normal:
                            tmp_var = normalize(tmp_var)
                        else:
                            # Conversion de la pression en atm (au lieu de Pa)
                            tmp_var = tmp_var / 101325
                    elif av == 'tas':
                        if normal:
                            tmp_var = normalize(tmp_var)
                        else:
                            # Conversion de la température en °C (au lieu de K)
                            tmp_var = tmp_var - 273.15
                    else:
                        if normal:
                            tmp_var = normalize(tmp_var)
                    tmp_var = np.expand_dims(tmp_var, axis=1)
                if i == 0:
                    self.AddVar = tmp_var
                else:
                    self.AddVar = np.concatenate((self.AddVar, tmp_var), 1)
        else:
            self.AddVar = None
        sh = self.InputVar.shape
        self.height = sh[1]
        self.width = sh[2]

        self.transf = transforms.Compose([FilterAod(), MultAod()])
        self.target_transf = transforms.Compose([ConvPM()])
        self.h_i = horizon
        self.maxTest = torch.max(self.target_transf(torch.from_numpy(self.TargetVar))).float()
        self.minTest = torch.min(self.target_transf(torch.from_numpy(self.TargetVar))).float()
        self.meanTest = torch.mean(self.target_transf(torch.from_numpy(self.TargetVar))).float()
        self.maxTrain = self.maxTest
        self.minTrain = self.minTest
        self.log = log
        if log:
            self.dis = Distrib()
            self.maxTrain = torch.max(self.dis(self.target_transf(torch.from_numpy(self.TargetVar)))).float()
            self.minTrain = torch.min(self.dis(self.target_transf(torch.from_numpy(self.TargetVar)))).float()

    def __len__(self):
        """ Returns the length of the Dataset. """
        return len(self.InputVar) - self.h_i

    def __getitem__(self, idx):
        """ Returns a pair (input_, target) from an index (idx).
            Useful for DataLoader."""
        input_ = torch.from_numpy(self.InputVar[idx:idx + 1])
        # We apply transform to the input
        input_ = self.transf(input_)
        target = torch.from_numpy(self.TargetVar[idx + self.h_i:idx + self.h_i + 1])
        # We apply target_transform to the input
        target = self.target_transf(target)
        if self.log:
            input_ = self.dis(input_)
            target = self.dis(target)
        if self.AddVar is not None:
            addVar = torch.from_numpy(self.AddVar[idx])
            input_ = torch.cat((input_, addVar), 0)
        return input_, target

class DatasetAladinEurGAN(Dataset):
    # This time, we extract all variables using two timeFormatter objects, which makes the DatasetAladinEur object easier to understand
    # (one timeFormatter object for the generator, another one for the discriminator)
    def __init__(self, list_dates, start_date_Gen, end_date_Disc, add_vars=None, extract_norm_wind=True, horizon=0, log=False, step=1, normal=False):
        if add_vars is None:
            add_vars = []
        tfGen = timeFormatter(list_dates=list_dates, start_date=start_date_Gen, step=step)
        tfDisc = timeFormatter(list_dates=list_dates, end_date=end_date_Disc, step=step)
        self.listDatesDisc = tfDisc.getListDates()
        self.listDatesGen = tfGen.getListDates()
        self.TargetVarDisc = tfDisc.getPm()
        self.TargetVarGen = tfGen.getPm()
        self.InputVarDisc = tfDisc.getVar('od550aer')
        self.InputVarGen = tfGen.getVar('od550aer')
        if normal:
           self.InputVarGen = normalize(self.InputVarGen)
           self.InputVarDisc = normalize(self.InputVarDisc)
        if len(add_vars) > 0:
            for i in range(len(add_vars)):
                av = add_vars[i]
                if av == "wind":
                    u10Disc = tfDisc.getVar('uas')
                    v10Disc = tfDisc.getVar('vas')
                    u10Gen = tfGen.getVar('uas')
                    v10Gen = tfGen.getVar('vas')
                    if extract_norm_wind:
                        normGen = np.sqrt(u10Gen ** 2 + v10Gen ** 2)
                        dirGen = np.arctan(v10Gen / u10Gen) * (180 / np.pi)
                        normGen = normGen[:, None]
                        dirGen = dirGen[:, None]
                        normDisc = np.sqrt(u10Disc ** 2 + v10Disc ** 2)
                        dirDisc = np.arctan(v10Disc / u10Disc) * (180 / np.pi)
                        normDisc = normDisc[:, None]
                        dirDisc = dirDisc[:, None]
                        if normal:
                            normGen = normalize(normGen)
                            dirGen = normalize(dirGen)
                            normDisc = normalize(normDisc)
                            dirDisc = normalize(dirDisc)
                        tmp_varGen = np.concatenate((normGen, dirGen), 1)
                        tmp_varDisc = np.concatenate((normDisc, dirDisc), 1)
                    else:
                        u10Gen = u10Gen[:, None]
                        v10Gen = v10Gen[:, None]
                        u10Disc = u10Disc[:, None]
                        v10Disc = v10Disc[:, None]
                        if normal:
                            u10Gen = normalize(u10Gen)
                            v10Gen = normalize(v10Gen)
                            u10Disc = normalize(u10Disc)
                            v10Disc = normalize(v10Disc)
                        tmp_varGen = np.concatenate((u10Gen, v10Gen), 1)
                        tmp_varDisc = np.concatenate((u10Disc, v10Disc), 1)
                elif av == "angstrom":
                    aod1000Gen = tfGen.getVar('od1000aer')
                    aodBGen = np.where(self.InputVarGen <= 0, np_eps, self.InputVarGen)
                    aod1000Gen = np.where(aod1000Gen <= 0, np_eps, aod1000Gen)
                    tmp_varGen = (-1 / np.log(1000 / 550)) * np.log(aod1000Gen / aodBGen)
                    tmp_varGen = np.expand_dims(tmp_varGen, axis=1)
                    aod1000Disc = tfDisc.getVar('od1000aer')
                    aodBDisc = np.where(self.InputVarDisc <= 0, np_eps, self.InputVarDisc)
                    aod1000Disc = np.where(aod1000Disc <= 0, np_eps, aod1000Disc)
                    tmp_varDisc = (-1 / np.log(1000 / 550)) * np.log(aod1000Disc / aodBDisc)
                    tmp_varDisc = np.expand_dims(tmp_varDisc, axis=1)
                    if normal:
                        tmp_varDisc = normalize(tmp_varDisc)
                        tmp_varGen = normalize(tmp_varGen)
                else:
                    tmp_varGen = tfGen.getVar(av)
                    tmp_varDisc = tfDisc.getVar(av)
                    if av == 'psl':
                        # Conversion de la pression en atm (au lieu de Pa)
                        tmp_varGen = tmp_varGen / 101325
                        tmp_varDisc = tmp_varDisc / 101325
                    elif av == 'tas':
                        # Conversion de la température en °C (au lieu de K)
                        tmp_varGen = tmp_varGen - 273.15
                        tmp_varDisc = tmp_varDisc - 273.15
                    tmp_varGen = np.expand_dims(tmp_varGen, axis=1)
                    tmp_varDisc = np.expand_dims(tmp_varDisc, axis=1)
                    if normal:
                        tmp_varDisc = normalize(tmp_varDisc)
                        tmp_varGen = normalize(tmp_varGen)
                if i == 0:
                    self.AddVarGen = tmp_varGen
                    self.AddVarDisc = tmp_varDisc
                else:
                    self.AddVarGen = np.concatenate((self.AddVarGen, tmp_varGen), 1)
                    self.AddVarDisc = np.concatenate((self.AddVarDisc, tmp_varDisc), 1)
        else:
            self.AddVarGen = None
            self.AddVarDisc = None
        sh = self.InputVarGen.shape
        self.height = sh[1]
        self.width = sh[2]
        self.transf = transforms.Compose([FilterAod(), MultAod()])
        self.target_transf = transforms.Compose([ConvPM()])
        self.h_i = horizon
        maxTestGen = torch.max(self.target_transf(torch.from_numpy(self.TargetVarGen))).float()
        maxTestDisc = torch.max(self.target_transf(torch.from_numpy(self.TargetVarDisc))).float()
        self.maxTest = torch.max(maxTestGen, maxTestDisc).float()
        minTestGen = torch.min(self.target_transf(torch.from_numpy(self.TargetVarGen))).float()
        minTestDisc = torch.min(self.target_transf(torch.from_numpy(self.TargetVarDisc))).float()
        self.minTest = torch.min(minTestGen, minTestDisc).float()
        self.maxTrain = self.maxTest
        self.minTrain = self.minTest
        self.log = log
        if log:
            self.dis = Distrib()
            maxTrainGen = torch.max(self.dis(self.target_transf(torch.from_numpy(self.TargetVarGen)))).float()
            maxTrainDisc = torch.max(self.dis(self.target_transf(torch.from_numpy(self.TargetVarDisc)))).float()
            self.maxTrain = torch.max(maxTrainGen, maxTrainDisc).float()
            minTrainGen = torch.min(self.dis(self.target_transf(torch.from_numpy(self.TargetVarGen)))).float()
            minTrainDisc = torch.min(self.dis(self.target_transf(torch.from_numpy(self.TargetVarDisc)))).float()
            self.minTrain = torch.min(minTrainGen, minTrainDisc)

    def __lenGen__(self):
        return len(self.InputVarGen) - self.h_i

    def __lenDisc__(self):
        return len(self.InputVarDisc) - self.h_i

    def __len__(self):
        """ Returns the length of the Dataset. """
        self.len = max(self.__lenDisc__(), self.__lenGen__())
        self.smallerLen = min(self.__lenDisc__(), self.__lenGen__())
        if self.len == self.__lenDisc__():
            self.biggerSet = "disc"
        else:
            self.biggerSet = "gen"
        return self.len


    def __getitem__(self, idx):
        """ Returns a pair (input_, target) from an index (idx).
            Useful for DataLoader."""
        # One of the two sets (complete and sparse) may be bigger than the other
        # At each iteration, elements from both datasets are taken
        # This allows for both the generator and discriminator to train from each sample
        # Instead of going through one entire set then the other
        if self.biggerSet == "disc":
            inputDisc = torch.from_numpy(self.InputVarDisc[idx: idx + 1])
            targetDisc = torch.from_numpy(
                self.TargetVarDisc[idx + self.h_i:idx + self.h_i + 1])
            # We apply transform to the input
            inputDisc = self.transf(inputDisc)
            # We apply target_transform to the target
            targetDisc = self.target_transf(targetDisc)
            if self.log:
                inputDisc = self.dis(inputDisc)
                targetDisc = self.dis(targetDisc)
            if self.AddVarDisc is not None:
                # Adding additional variables
                addVarDisc = torch.from_numpy(self.AddVarDisc[idx])
                inputDisc = torch.cat((inputDisc, addVarDisc), 0)
            if idx < self.smallerLen:
                # We only take these elements if they exist
                # idx being based on the bigger set, it may be too high an index for the smaller set
                inputGen = torch.from_numpy(self.InputVarGen[idx: idx + 1])
                targetGen = torch.from_numpy(self.TargetVarGen[idx + self.h_i:idx + self.h_i + 1])
                # We apply transform to the input
                inputGen = self.transf(inputGen)
                # We apply target_transform to the target
                targetGen = self.target_transf(targetGen)
                if self.log:
                    inputGen = self.dis(inputGen)
                    targetGen = self.dis(targetGen)
                if self.AddVarGen is not None:
                    # Adding additional variables
                    addVarGen = torch.from_numpy(self.AddVarGen[idx])
                    inputGen = torch.cat((inputGen, addVarGen), 0)
            else:
                inputGen = None
                targetGen = None

        else:
            inputGen = torch.from_numpy(self.InputVarGen[idx:idx + 1])
            targetGen = torch.from_numpy(
                self.TargetVarGen[idx + self.h_i:idx + self.h_i + 1])
            # We apply transform to the input
            inputGen = self.transf(inputGen)
            # We apply target_transform to the target
            targetGen = self.target_transf(targetGen)
            if self.log:
                inputGen = self.dis(inputGen)
                targetGen = self.dis(targetGen)
            if self.AddVarGen is not None:
                # Adding additional variables
                addVarGen = torch.from_numpy(self.AddVarGen[idx])
                inputGen = torch.cat((inputGen, addVarGen), 0)
            if idx < self.smallerLen:
                # We only take these elements if they exist
                # idx being based on the bigger set, it may be too high an index for the smaller set
                inputDisc = torch.from_numpy(self.InputVarDisc[idx:idx + 1])
                targetDisc = torch.from_numpy(self.TargetVarDisc[idx + self.h_i:idx + self.h_i + 1])
                # We apply transform to the input
                inputDisc = self.transf(inputDisc)
                # We apply target_transform to the target
                targetDisc = self.target_transf(targetDisc)
                if self.log:
                    inputDisc = self.dis(inputDisc)
                    targetDisc = self.dis(targetDisc)
                if self.AddVarDisc is not None:
                    # Adding additional variables
                    addVarDisc = torch.from_numpy(self.AddVarDisc[idx])
                    inputDisc = torch.cat((inputDisc, addVarDisc), 0)
            else:
                inputDisc = None
                targetDisc = None

        return inputGen, targetGen, inputDisc, targetDisc
