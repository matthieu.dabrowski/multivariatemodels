# Importing libraries and modules
import datetime
# Import libraries
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import torch
from netCDF4 import Dataset as netcdfFile
from torch.utils.data import Dataset
from .transforms import MRtoConc2D, FilterAod, MultAod, ConvPM, Distrib, BCrandom, BCrandomLand, BCfromFile, BCnaive
from torchvision import transforms
np_eps = np.finfo(float).eps

matplotlib.use('Agg')
plt.ioff()

# If you can, run this example on a GPU, it will be a lot faster.
cuda = torch.cuda.is_available()
device_name = "cuda" if cuda else "cpu"
device = torch.device(device_name)

list_aermr_tot = ["aermr02", "aermr03", "aermr05", "aermr06", "aermr07", "aermr08", "aermr09", "aermr10", "aermr11"]
dict_aer = {"du": (["aermr04", "aermr05", "aermr06"], "duaod550"),
            "bc": (["aermr09", "aermr10"], "bcaod550"),
            "om": (["aermr07", "aermr08"], "omaod550"),
            "ss": (["aermr01", "aermr02", "aermr03"], "ssaod550"),
            "su": (["aermr11"], "suaod550"),
            "pmT": (list_aermr_tot, "aod550"),
            "pm": (["pm2p5"], "aod550")}

class DatasetCamsKri(Dataset):
    """ Object inheriting from the torch.Dataset class, customized to fit our need.
        Uses inputs and targets from data files.
        Specificity of the DatasetCamsGroundTarget class : for the target, we only take a matrix corresponding to the surface/ground level values.
        Indeed, here we will build a model that will only try to predict the targeted values at ground level, and the other values, corresponding to higher levels, are irrelevant.
    """

    def __init__(self, pathSfcList, pathPlList, bcType, bcP, var, horizon=0, cokri=False, log=False):
        """ Creates a Dataset object.
            input:
                pathSfcList: paths to the NetCDF files with sfc level type (as values can be located in several different files corresponding to different periods of time)
                pathPlList: paths to the NetCDF files with pl level type
                bcType: type of boundary conditions to use (naive, file, random or randomOnLand)
                bcP: proportion of pixels that will be used as BC (only useful if bcType is random or randomOnLand)
                var: name of the NetCDF variable that will be predicted (must be present in file_sfc)
                horizon (optional) : horizon of prediction
                cokri (optional) : determines if kriging or cokriging data is needed
                log (optional) : determines if a logarithm should be applied on the values to smooth their distribution
        """
        # Setting up the BC
        if bcType == "naive":
            self.bc = BCnaive()
        elif bcType == "file":
            self.bc = BCfromFile()
        elif bcType == "random":
            if bcP <= 0:
                self.bc = None
            self.bc = BCrandom(bcP, origData='Cams')
        elif bcType == "randomOnLand":
            if bcP <= 0:
                self.bc = None
            self.bc = BCrandomLand(bcP)
        aod = dict_aer[var][1]
        list_var = dict_aer[var][0]
        for j in range(len(pathSfcList)):
            pathSfc = pathSfcList[j]
            pathPl = pathPlList[j]
            # We do not set file_sfc and file_pl as attributes of the DatasetCams as they are very heavy and it would not be useful.
            file_sfc = netcdfFile(pathSfc, "r", format="NETCDF4")
            InputVarTemp = file_sfc.variables[aod][:]
            if var == "pm":
                TargetVarTemp = np.zeros_like(file_sfc.variables[list_var[0]][:])
                for var in list_var:
                    TargetVarTemp += file_sfc.variables[var][:]
            else:
                file_pl = netcdfFile(pathPl, "r", format="NETCDF4")
                # Here we only select the data that corresponds to ground-level concentration
                TargetVarTemp = np.zeros_like(file_pl.variables[list_var[0]][:, -1])
                for var in list_var:
                    TargetVarTemp += file_pl.variables[var][:, -1]
            if j == 0:
                self.InputVar = InputVarTemp
                self.TargetVar = TargetVarTemp
            else:
                self.InputVar = np.concatenate((self.InputVar, InputVarTemp), 0)
                self.TargetVar = np.concatenate((self.TargetVar, TargetVarTemp), 0)
        self.transf = transforms.Compose([FilterAod(), MultAod()])
        if var == "pm":
            self.target_transf = transforms.Compose([ConvPM()])
        else:
            self.target_transf = transforms.Compose([MRtoConc2D()])
        self.h_i = horizon//3
        self.var = var
        self.maxTest = torch.max(self.target_transf(torch.from_numpy(self.TargetVar))).float()
        self.minTest = torch.min(self.target_transf(torch.from_numpy(self.TargetVar))).float()
        self.maxTrain = self.maxTest
        self.minTrain = self.minTest
        self.log = log
        if log:
            self.dis = Distrib()
            self.maxTrain = torch.max(self.dis(self.target_transf(torch.from_numpy(self.TargetVar)))).float()
            self.minTrain = torch.min(self.dis(self.target_transf(torch.from_numpy(self.TargetVar)))).float()
        self.cokri = cokri
        self.latitude = np.array(file_sfc.variables["latitude"][:])
        self.longitude = np.array(file_sfc.variables["longitude"][:])

    def __len__(self):
        """ Returns the length of the Dataset. """
        return len(self.InputVar)-self.h_i

    def __getitem__(self, idx):
        """ Returns a pair (input_, target) from an index (idx).
            Useful for DataLoader."""
        input_ = torch.from_numpy(self.InputVar[idx:idx+1])
        # We apply transform to the input
        input_ = self.transf(input_)
        target = torch.from_numpy(self.TargetVar[idx+self.h_i:idx+self.h_i+1])
        # We apply target_transform to the input
        target = self.target_transf(target)
        if self.log:
            input_ = self.dis(input_)
            target = self.dis(target)
        bcSparse = torch.where(self.bc, 1, 0)*target
        if self.cokri:
            return input_, target, bcSparse
        else:
            return target, bcSparse


def hours2dateAer(nb_hours):
    """ Converts numbers of hours (as used in our data) to datetime format. """
    # The initial date is 01/01/1900
    return datetime.datetime(1850, 1, 1, 0) + datetime.timedelta(days=float(nb_hours))

def fromArray_hours2dateAerArray(list_nb_hours):
    """ Converts a list of number of hours (as used in our data) to a set of dates in datetime format."""
    # list_nb_hours = np.array(list_nb_hours)
    list_dates = []
    for i in range(len(list_nb_hours)):
        # The initial date is 01/01/1900
        list_dates.append(hours2dateAer(list_nb_hours[i]))
    return np.array(list_dates)

class timeFormatter(object):
    """ This object allows for an easier extraction of the variables from the different files """
    def __init__(self, list_dates, start_date=None, end_date=None, step=1):
        pmS = "pm25"
        self.debFileName = "AeroClim2/"
        self.midFileName = "_MED-11_ECMWF-ERA5_evaluation_r1i1p1_CNRM-ALADIN64_v1_1hrPt_"
        self.ext = ".nc"
        self.listDates = np.array([])
        self.list_dates_strings = list_dates
        for i in range(len(list_dates)):
            # The name of the file depends both on the variables we want to extract and the chosen time period
            path = self.debFileName + pmS + self.midFileName + self.list_dates_strings[i] + self.ext
            if i == 0:
                self.pmTemp = netcdfFile(path, "r", format="NETCDF4").variables[pmS][:]
            else:
                self.pmTemp = np.concatenate((self.pmTemp, netcdfFile(path, "r", format="NETCDF4").variables[pmS][:]), axis=0)
            self.listDates = np.concatenate(
                (self.listDates, fromArray_hours2dateAerArray(netcdfFile(path, "r", format="NETCDF4").variables['time'][:])))
        # We get an initial list of dates, which will be useful to then select a time period from this list of dates
        if start_date == None and end_date == None:
            self.idxTime = np.where(self.listDates)[0][::step]
        elif end_date == None:
            self.idxTime = np.where(self.listDates >= start_date)[0][::step]
        elif start_date == None:
            self.idxTime = np.where(self.listDates <= end_date)[0][::step]
        else:
            self.idxTime = np.where(np.logical_and(self.listDates >= start_date, self.listDates <= end_date))[0][::step]
        self.listDates = self.listDates[self.idxTime]
        # We get pmTemp from the start (instead of using the getVar() function, as we need to extract the list of dates from a file anyway
        # and the pm concentration is always a useful value to obtain
        self.pmTemp = self.pmTemp[self.idxTime, :, :]

    def getListDates(self):
        return self.listDates

    def getPm(self):
        return self.pmTemp

    def getVar(self, var):
        for i in range(len(self.list_dates_strings)):
            # The name of the file depends both on the variables we want to extract and the chosen time period
            path = self.debFileName + var + self.midFileName + self.list_dates_strings[i] + self.ext
            if i == 0:
                varTemp = netcdfFile(path, "r", format="NETCDF4").variables[var][:]
            else:
                varTemp = np.concatenate((varTemp, netcdfFile(path, "r", format="NETCDF4").variables[var][:]), axis=0)
        # we use self.idxTime as it already corresponds to the chosen list of dates
        return varTemp[self.idxTime, :, :]

def normalize(t):
    t = np.array(t)
    return (t-np.min(t))/(np.max(t)-np.min(t))

class DatasetAladinEurKri(Dataset):
    # This time, we extract all variables using a timeFormatter object, which makes the DatasetAladinEur object easier to understand
    def __init__(self, list_dates, start_date=None, end_date=None, horizon=0, bcP=5, log=False, step=1, normal=False, cokri=False):
        tf = timeFormatter(list_dates=list_dates, start_date=start_date, end_date=end_date, step=step)
        self.bc = BCrandom(bcP, origData="Al-EUR")
        self.listDates = tf.getListDates()
        self.TargetVar = tf.getPm()
        self.InputVar = tf.getVar('od550aer')
        if normal:
            self.InputVar = normalize(self.InputVar)
        sh = self.InputVar.shape
        self.height = sh[1]
        self.width = sh[2]

        self.transf = transforms.Compose([FilterAod(), MultAod()])
        self.target_transf = transforms.Compose([ConvPM()])
        self.h_i = horizon
        self.maxTest = torch.max(self.target_transf(torch.from_numpy(self.TargetVar))).float()
        self.minTest = torch.min(self.target_transf(torch.from_numpy(self.TargetVar))).float()
        self.maxTrain = self.maxTest
        self.minTrain = self.minTest
        self.log = log
        self.cokri = cokri
        if log:
            self.dis = Distrib()
            self.maxTrain = torch.max(self.dis(self.target_transf(torch.from_numpy(self.TargetVar)))).float()
            self.minTrain = torch.min(self.dis(self.target_transf(torch.from_numpy(self.TargetVar)))).float()

    def __len__(self):
        """ Returns the length of the Dataset. """
        return len(self.InputVar) - self.h_i

    def __getitem__(self, idx):
        """ Returns a pair (input_, target) from an index (idx).
            Useful for DataLoader."""
        input_ = torch.from_numpy(self.InputVar[idx:idx + 1])
        # We apply transform to the input
        input_ = self.transf(input_)
        target = torch.from_numpy(self.TargetVar[idx + self.h_i:idx + self.h_i + 1])
        # We apply target_transform to the input
        target = self.target_transf(target)
        if self.log:
            input_ = self.dis(input_)
            target = self.dis(target)
        bcSparse = torch.where(self.bc, 1, 0) * target
        if self.cokri:
            return input_, target, bcSparse
        else:
            return target, bcSparse
