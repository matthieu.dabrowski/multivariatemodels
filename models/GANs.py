# Importing libraries and modules
import os
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import math
# PyTorch libraries and modules
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from piq import FSIMLoss
from dataset_generation import ApplyBC, BCloss, PlotPrep, Filter, Distrib, AirQualityQuantif
import time
import json
import numpy as np
eps = np.finfo('float').eps
matplotlib.use('Agg')
from .baseModel import baseModel, convert
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
plt.ioff()

cuda = torch.cuda.is_available()
device_name = "cuda" if cuda else "cpu"
device = torch.device(device_name)
cpu = torch.device("cpu")


class CGAN(nn.Module):
    """ This class allows us to wrap a generator model and a discriminator in order to build a CGAN."""

    def __init__(self, name_model, device, generator: baseModel, discriminator: baseModel):
        """ Creating a CGAN object, and propagating some of its properties to generator and discriminator objects."""
        super().__init__()
        self.name_model = name_model
        # The source folder depends on the data source
        if 'Al-LOCAL' in self.name_model:
            sourceFolder = "savedModels/Al-Local/"
        elif 'Al-EUR' in self.name_model:
            sourceFolder = "savedModels/Al-Europe/"
        else:
            sourceFolder = "savedModels/Cams/"
        self.sourceFolder = sourceFolder
        self.device = device
        # Each GAN contains both a generator model and a discriminator
        self.generator = generator
        self.discriminator = discriminator

        self.adversarial_loss = nn.CrossEntropyLoss()

    def loadGAN(self):
        """ We load both models and output both losses. """
        # Loading the GAN implies loading its generator and discriminator
        losses_G = self.generator.loadModel(name_GAN=self.name_model)
        losses_D = self.discriminator.loadModel(name_GAN=self.name_model)
        return [losses_G, losses_D]

    def saveGAN(self, loss_G, loss_D, train_time):
        """ Again, use the saveModel functions of generator and discriminator. """
        # Saving the GAN implies saving its generator and discriminator
        self.generator.saveModel(loss_G, train_time, name_GAN=self.name_model)
        self.discriminator.saveModel(loss_D, train_time, name_GAN=self.name_model)

    def saveCheckpointGAN(self, epoch, lossG, lossD, losses_G, losses_D, train_time):
        """ Use the saveCheckpoint functions of generator and discriminator. """
        self.generator.saveCheckpoint(epoch, lossG, losses_G, train_time, name_GAN=self.name_model)
        self.discriminator.saveCheckpoint(epoch, lossD, losses_D, train_time, name_GAN=self.name_model)

    def loadCheckpointGAN(self):
        """ Use the loadCheckpoints methods of generator and discriminator. """
        [epoch1, lossG, losses_G, train_timeG] = self.generator.loadCheckpoint(name_GAN=self.name_model)
        [epoch2, lossD, losses_D, train_timeD] = self.discriminator.loadCheckpoint(name_GAN=self.name_model)
        epoch = min(epoch1, epoch2)
        train_time = max(train_timeD, train_timeG)
        return [epoch, lossG, lossD, losses_G, losses_D, train_time]

    def trainGAN(self, dataLoader, num_epochs, save=True, parallel=True):
        """ This function allows to train the GAN (two models) using data from a torch.DataLoader object.
            We also need to specify the number of epochs, and a boolean specifying if we want to save our trained model (default is True).
            It returns a list of losses. """
        losses_G = {"Iteration": [], "Adversarial loss": []}
        if self.generator.supervised:
            losses_G["Supervised loss"] = []
        if (self.generator.bc is not None):
            if self.generator.hardBC:
                appBC = ApplyBC(self.generator.bc)
            else:
                lossBC = BCloss(self.generator.bc)
                losses_G["BC loss"] = []
        if ((not self.generator.hardBC) and (self.generator.bc is not None)):
            losses_G["Total loss"] = []
        losses_D = {"Iteration": [], "Loss for real samples": [], "Loss for generated samples": [], "Total loss": []}
        if self.discriminator.supervised:
            losses_D["Supervised loss"] = []
        if os.path.exists(self.sourceFolder + self.name_model + "/checkpoint.pl"):
            [start_epoch, lossG, lossD, losses_G, losses_D, train_time] = self.loadCheckpointGAN()
        else:
            start_epoch = 0
            train_time = 0
        if cuda and parallel:
            generator_forward = nn.DataParallel(self.generator, output_device=self.device)
            discriminator_forward = nn.DataParallel(self.discriminator, output_device=self.device)
        else:
            generator_forward = self.generator.forward
            discriminator_forward = self.discriminator.forward
        print("Starting training")
        start_time = time.time()
        for epoch in range(start_epoch, num_epochs):
            for idx_batch, (inputGen, targetGen, inputDisc, targetDisc) in enumerate(dataLoader):
                # At each iteration, we get two inputs (one for the generator and one for the discriminator) and two targets (same reason)
                iteration = int(len(dataLoader) * epoch + idx_batch)
                losses_G["Iteration"].append(iteration)
                ##################
                if (inputGen is not None) and (targetGen is not None):
                    inputGen = inputGen.float()
                    targetGen = targetGen.float()
                    inputGen = inputGen.to(self.device)
                    targetGen = targetGen.to(self.device)
                    # Generator training
                    self.generator.train()
                    self.discriminator.eval()
                    # Most forward way to realise the inference
                    pred = generator_forward(inputGen)
                    pred = pred.float()
                    pred = pred.to(self.device)
                    if self.generator.supervised:
                        # We keep a supervised loss (comparison between prediction and target)
                        supervised_loss = torch.mean(self.generator.criterion(pred, targetGen))
                        # We put the losses on a list in dictionary for saving purposes
                        losses_G["Supervised loss"].append(supervised_loss.item())
                    else:
                        if (self.generator.bc is not None):
                            if self.generator.hardBC:
                                pred = appBC(targetGen, pred)
                            else:
                                bc_loss = torch.mean(lossBC(targetGen, pred))
                                losses_G["BC loss"].append(bc_loss.item())
                    batch_size = inputGen.shape[0]
                    # Valid is a tensor of one, similar to what we would obtain if the discriminator classified all our results as valid
                    valid = Variable(torch.FloatTensor(batch_size, 1).fill_(1.0), requires_grad=False)
                    valid = valid.to(self.device)
                    # Validity is what the discriminator actually classified our results as
                    # Most forward way to realise the inference
                    validity = discriminator_forward(inputGen, pred)
                    # The difference between validity and valid (class True) is our loss : the generator's "goal" is to fool the discriminator
                    # In other words to maje sure the discriminator classifies all our results as valid
                    adv_loss = self.adversarial_loss(validity, valid)
                    losses_G["Adversarial loss"].append(adv_loss.item())
                    if self.generator.supervised:
                        lossG = (supervised_loss + adv_loss) / 2
                    else:
                        if self.generator.hardBC or (self.generator.bc is None):
                            lossG = adv_loss
                        else:
                            lossG = (adv_loss + bc_loss)/2
                    if ((not self.generator.hardBC) and (self.generator.bc is not None)):
                        losses_G["Total loss"].append(lossG.item())
                    self.generator.optimizer.zero_grad()
                    lossG.backward()
                    self.generator.optimizer.step()
                ##################
                if (inputDisc is not None) and (targetDisc is not None):
                    # Training of the discriminator
                    inputDisc = inputDisc.float()
                    targetDisc = targetDisc.float()
                    inputDisc = inputDisc.to(self.device)
                    targetDisc = targetDisc.to(self.device)
                    iteration = int(len(dataLoader) * epoch + idx_batch)
                    losses_D["Iteration"].append(iteration)
                    self.discriminator.train()
                    self.generator.eval()
                    batch_size = inputDisc.shape[0]
                    # Valid is a tensor of one, similar to what we would obtain if the discriminator classified all our results as valid
                    valid = Variable(torch.FloatTensor(batch_size, 1).fill_(1.0), requires_grad=False)
                    valid = valid.to(self.device)
                    # We also need a tensor of zeros, such as what we would obtain if the discriminator classified all our results as valid
                    fake = Variable(torch.FloatTensor(batch_size, 1).fill_(0.0), requires_grad=False)
                    fake = fake.to(self.device)
                    # Most forward way to realise the inferences
                    pred = generator_forward(inputDisc)
                    pred = pred.float()
                    pred = pred.to(self.device)
                    val_real = discriminator_forward(inputDisc, targetDisc)
                    val_gen = discriminator_forward(inputDisc, pred.detach())
                    # Computing the adversarial losses
                    loss_real = self.adversarial_loss(val_real, valid)
                    loss_fake = self.adversarial_loss(val_gen, fake)
                    losses_D["Loss for real samples"].append(loss_real.item())
                    losses_D["Loss for generated samples"].append(loss_fake.item())
                    if self.discriminator.supervised:
                        supervised_loss_D = torch.mean(self.discriminator.criterion(pred, targetDisc))
                        losses_D["Supervised loss"].append(supervised_loss_D.item())
                        lossD = (loss_real + loss_fake + supervised_loss_D) / 3
                    else:
                        lossD = (loss_real + loss_fake) / 2
                    losses_D["Total loss"].append(lossD.item())
                    self.discriminator.optimizer.zero_grad()
                    lossD.backward()
                    self.discriminator.optimizer.step()
                ##################
            if (epoch + 1) % 10 == 0 or (epoch + 1) == num_epochs:
                print(f"Epoch : {epoch + 1} | Loss generator : {lossG.item():.4e} | Loss discriminator : {lossD.item():.4e}")
                train_time = train_time + (time.time() - start_time)
                start_time = time.time()
                # Saving a checkpoint once every 10 epochs or so
                self.saveCheckpointGAN(epoch=epoch, lossG=lossG, lossD=lossD, losses_G=losses_G, losses_D=losses_D,
                                       train_time=train_time)
        print("End of training")
        if save:
            # Saving the GAN once the training is complete
            self.saveGAN(losses_G, losses_D, train_time)
        return [losses_G, losses_D]

    def saveOne2D(self, target, predic, loss, Vtarg, Vpred, input_, r_loss, ct, cp, ce, mbe, mbe_r, sim_idx, date, testPeriod, error, e_bw, unfilter=False, minTarg=None, maxTarg=None):
        # We generate one file for each tuple (target, prediction)
        if testPeriod is not None:
            path = "results/" + self.name_model + "/" + testPeriod + "/results_test_" + date.strftime("%d-%m-%Y_%Hh") + ".pdf"
        else:
            path = "results/" + self.name_model + "/results_test_" + date.strftime("%d-%m-%Y_%Hh") + ".pdf"
        predic = predic[0]
        target = target[0]
        input_ = input_[0]
        inp = input_
        pred = predic
        targ = target
        # PlotPrep is a function that prepares our tensors to be displayed on a file
        # It mainly makes these matrices more easily interpretable
        prep = PlotPrep()
        pred = prep(pred, norm=True, minTarg=minTarg, maxTarg=maxTarg)
        inp = prep(inp, norm=True)
        targ = prep(targ, norm=True, minTarg=minTarg, maxTarg=maxTarg)
        loss = loss.item()
        r_loss = r_loss.item()
        # Only save the sample results if the relative error is greater than 1
        # Or if we choose to save them all
        if r_loss >= 1 or unfilter:
            # With the GAN, two additional metrics are obtained
            # Output for the discriminator for the target and model output (with the same input)
            Vtarg = Vtarg[0][0].item()
            Vpred = Vpred[0][0].item()
            mbe = mbe.item()
            mbe_r = mbe_r.item()
            sim_idx = sim_idx.item()
            plt.suptitle("Test results.")
            # Each file contains two images : one for the prediction and one for the target
            ax0 = plt.subplot(2, 2, 1)
            # Input at top-left
            i0=ax0.imshow(inp)
            # Printing a colorbar to enhance interpretability
            plt.colorbar(i0, ax=ax0, ticks=np.linspace(torch.min(input_).to(cpu), torch.max(input_).to(cpu), 5))
            ax0.set_title(f"Input (AOD). \n Test error : {loss:1.3f}\u03BCg/m\N{superscript three} or "
                              f"{r_loss*100:1.3f}%.")
            ax1 = plt.subplot(2, 2, 3)
            # Model output at bottom-left
            i1=ax1.imshow(pred)
            # Printing a colorbar to enhance interpretability
            plt.colorbar(i1, ax=ax1, ticks=np.linspace(minTarg.to(cpu), maxTarg.to(cpu), 5))
            # Writing the title with several metrics in it
            ax1.set_title(f"Prediction."
                              f" MBE : {mbe:1.3f}\u03BCg/m\N{superscript three} or {mbe_r*100:1.3f}%."
                              f" \n sim-index loss : {sim_idx*100:1.3f}%."
                              f" \n Discriminator result : {Vpred:1.3f}.")
            ax2 = plt.subplot(2, 2, 4)
            # Target at bottom-left
            i2=ax2.imshow(targ)
            # Printing a colorbar to enhance interpretability
            plt.colorbar(i2, ax=ax2, ticks=np.linspace(minTarg.to(cpu), maxTarg.to(cpu), 5))
            ax2.set_title(f"Target. \n Discriminator result : {Vtarg:1.3f}.")
            plt.savefig(path)
            plt.close()
            if error:
                # If the error parameter is true, images describing the model error are also saved
                ct = ct[0] / 4
                cp = cp[0] / 4
                ce = ce.item()
                ct = PlotPrep()(ct)
                cp = PlotPrep()(cp)
                # We generate one file for each tuple (target, prediction)
                if testPeriod is not None:
                    path2 = "results/" + self.name_model + "/" + testPeriod + "/error_test_" + date.strftime(
                        "%d-%m-%Y_%Hh") + ".pdf"
                else:
                    path2 = "results/" + self.name_model + "/error_test_" + date.strftime(
                        "%d-%m-%Y_%Hh") + ".pdf"
                errorMat = torch.abs(target - predic)
                # if (not self.supervised) and (self.bc is not None):
                #     errorMat = PlotPrep()(errorMat, bc=self.bc, error=True, bw=e_bw, norm=True)
                # else:
                errorMat = PlotPrep()(errorMat, error=True, bw=e_bw, norm=True)
                plt.suptitle("Test error visualisation.")
                ax1 = plt.subplot(2, 2, 1)
                if e_bw:
                    ax1.imshow(errorMat, cmap='gray')
                else:
                    ax1.imshow(errorMat)
                ax1.set_title(
                    f"Error visualisation. Test error : {loss:1.3f}\u03BCg/m\N{superscript three} or {r_loss * 100:1.3f}%.")
                ax2 = plt.subplot(2, 2, 3)
                ax2.imshow(cp)
                # Instead of displaying the discriminator results we compute the discriminator's error (on both target and generator output)
                Verr = (Vpred + 1 - Vtarg) / 2
                ax2.set_title(f"Segmented prediction. \n Classification error : {ce:1.3f}. \n Discriminator error : {Verr:1.3f}.")
                ax3 = plt.subplot(2, 2, 4)
                ax3.imshow(ct)
                ax3.set_title("Segmented target.")
                plt.savefig(path2)
                plt.close()

    def testGAN(self, dataLoader, testDateList, maxTarg, minTarg, save=True, error=True, log=False, e_bw=True, parallel=True, unfilter=False, QA=False):
        """ This function allows us to test our GAN using data from a torch.DataLoader object.
                    It returns a list of the targets of this test, the predictions obtained, and the losses. """
        if save:
            os.makedirs("results/" + self.name_model + "/", exist_ok=True)
            # os.makedirs("results/" + self.name_model + "/" + testPeriod + "/", exist_ok=True)
        testDates = mdates.date2num(np.array(testDateList))
        self.generator.eval()
        self.discriminator.eval()
        # Creating lists to store the metrics
        losses = []
        r_losses = []
        times = []
        closs = []
        MBE = []
        MBE_R = []
        sim_idces = []
        if QA:
            qas = []
        # Setting up the FSIM loss
        newMax = (maxTarg - minTarg)
        simLoss = FSIMLoss(chromatic=False, data_range=newMax + 0.1)
        # Doing parallel processing to speed up testing whenever possible
        if cuda and parallel:
            generator_forward = nn.DataParallel(self.generator, output_device=self.device)
            discriminator_forward = nn.DataParallel(self.discriminator, output_device=self.device)
        else:
            generator_forward = self.generator.forward
            discriminator_forward = self.discriminator.forward
        if log:
            dis = Distrib()
        if (not self.generator.supervised) and self.generator.hardBC and (self.generator.bc is not None):
            appBC = ApplyBC(self.generator.bc)
        with torch.no_grad():
            for idx_batch, (input_, target) in enumerate(dataLoader):
                input_ = input_.float()
                target = target.float()
                input_ = input_.to(self.device)
                target = target.to(self.device)
                start_time = time.time()
                # Most forward way to realise the inference
                pred = generator_forward(input_)
                pred_time = time.time() - start_time
                pred = pred.float()
                pred = pred.to(self.device)
                if (not self.generator.supervised) and self.generator.hardBC and (self.generator.bc is not None):
                    pred = appBC(target, pred)
                # Most straightforward way to procede to the discriminator's evaluation
                Vtarg = discriminator_forward(input_, target)
                Vpred = discriminator_forward(input_, pred)
                if input_.shape[1] > 1:
                    # Separating the main input (AOD) from the rest if necessary
                    input_ = input_[:, 0:1]
                if log:
                    # If log parameter is true, then we reverse this transformation before displaying the values
                    pred = dis.reverse(pred)
                    target = dis.reverse(target)
                    input_ = dis.reverse(input_)
                # Filtering out irrelevant concentration values
                pred = Filter(pred)
                target = Filter(target)
                # Using the quantiles to compute the Quantized Error
                q1, q2, q3 = torch.quantile(target, 0.25), torch.quantile(target, 0.5), torch.quantile(target, 0.75)
                ct = torch.where(target > q1, 1, 0) + torch.where(target > q2, 1, 0) + torch.where(target > q3, 1, 0)
                cp = torch.where(pred > q1, 1, 0) + torch.where(pred > q2, 1, 0) + torch.where(pred > q3, 1, 0)
                ce = torch.mean(torch.abs(ct-cp).float())
                # absolute MAE
                loss = torch.mean(torch.sqrt(self.generator.criterion(pred, target)))
                # relative MAE
                r_loss = loss/torch.mean(target)
                # absolute MBE
                mbe = torch.mean(pred - target)
                # relative MBE
                mbe_r = mbe/torch.mean(target)
                # A bit of processing is necessary to compute the FSIM
                predF = torch.nan_to_num((pred - torch.min(pred)), nan=torch.nan, posinf=newMax, neginf=-newMax)
                predF = ((predF > 0) * (predF < newMax)) * predF + (predF >= newMax) * newMax
                targetF = torch.nan_to_num((target - minTarg), nan=torch.nan, posinf=newMax, neginf=-newMax)
                targetF = ((targetF > 0) * (targetF < newMax)) * targetF + (targetF >= newMax) * newMax
                fsim_loss = torch.mean(simLoss(predF, targetF))
                if QA:
                    AQQ = AirQualityQuantif()
                    predq = AQQ(pred[0])
                    targetq = AQQ(target[0])
                    qa = torch.mean(torch.abs(predq - targetq))
                    qas.append(qa.item())
                    if unfilter:
                        sm = matplotlib.cm.ScalarMappable(norm=None, cmap=plt.get_cmap("jet"))
                        path = "results/" + self.name_model + "/results_AirQuality_" + testDateList[
                            idx_batch].strftime(
                            "%d-%m-%Y_%Hh") + ".pdf"
                        prep = PlotPrep()
                        cm = confusion_matrix(y_true=torch.flatten(targetq).detach().cpu().numpy(), y_pred=torch.flatten(predq).detach().cpu().numpy())
                        predQP = prep(predq)
                        targetQP = prep(targetq)
                        ax0 = plt.subplot(2, 2, 1)
                        ax0.set_title("Air Quality - target")
                        ax0.imshow(targetQP)
                        plt.colorbar(sm, ax=ax0,
                                     ticks=np.linspace(torch.min(targetq).to(cpu), torch.max(targetq).to(cpu), 5),
                                     orientation="vertical", label="no units (Air Quality Classes)")
                        ax1 = plt.subplot(2, 2, 2)
                        ax1.set_title("Air Quality - prediction")
                        ax1.imshow(predQP)
                        ax2 = plt.subplot(2, 2, 3)
                        ax2.set_title("Air Quality - confusion matrix")
                        ax2.imshow(cm)
                        plt.colorbar(sm, ax=ax2,
                                     ticks=np.linspace(np.min(cm), np.max(cm), 5),
                                     orientation="vertical", label="nb of samples (including pixels)")
                        plt.savefig(path)
                        plt.close()
                if save:
                    # Saving images and metrics for this specific sample
                    self.saveOne2D(target, pred, loss, Vtarg, Vpred, input_, r_loss, ct, cp, ce, mbe, mbe_r, fsim_loss, date=testDateList[idx_batch], testPeriod=None, error=error, e_bw=e_bw, unfilter=unfilter, minTarg=minTarg, maxTarg=maxTarg)
                # Saving metrics in lists
                times.append(pred_time)
                closs.append(ce.item())
                r_losses.append(r_loss.item())
                losses.append(loss.item())
                MBE.append(mbe.item())
                MBE_R.append(mbe_r.item())
                sim_idces.append(fsim_loss.item())
        if save:
            path = "results/" + self.name_model + "/MAE_evolution.jpg"
            # Plotting the evolution of the MAE during testing
            plt.suptitle("MAE per sample number")
            fig, ax = plt.subplots(1,1)
            ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y-%H'))
            ax.xaxis.set_major_locator(mdates.DayLocator(interval=100))
            ax.plot(testDates, r_losses)
            plt.savefig(path)
            # Saving a dictionary of metrics representing the model's performance
            dict_perf = {"Average inference time (s)": np.mean(times),
                         "STD of inference time (s)": np.std(times),
                         "Average prediction error (\u03BCg/m\N{superscript three})": np.mean(losses),
                         "STD of prediction error  (\u03BCg/m\N{superscript three})": np.std(losses),
                         "Average relative error (%)": 100 * np.mean(r_losses),
                         "STD of relative error (%)": 100 * np.std(r_losses),
                         "Average classification error": np.mean(closs),
                         "STD of classification error": np.std(closs),
                         "Average bias error (\u03BCg/m\N{superscript three})": np.mean(MBE),
                         "STD of bias error (\u03BCg/m\N{superscript three})": np.std(MBE),
                         "Average relative bias error (%)": 100 * np.mean(MBE_R),
                         "STD of relative bias error (%)": 100 * np.std(MBE_R),
                         "Average FSIM loss (%)": 100 * np.mean(sim_idces),
                         "STD of FSIM loss (%)": 100 * np.std(sim_idces)
                         }
            if QA:
                dict_perf["Average QA loss (%)"] = 100 * np.mean(qas)/6
                dict_perf["STD of QA loss (%)"] = 100 * np.std(qas)/6
            with open("results/" + self.name_model + "/perf_test.txt", "w+") as f:
                json.dump(dict_perf, f, indent=2)
                f.close()

class ClsDiscriminator(baseModel):
    """This class is an abstract class, therefore it should never be called.
     It is designed to create a discriminator model for a GAN.
    As such, it does not need training and testing functions, as the GAN already has those."""

    def __init__(self, name_model, device, dSup):
        super().__init__(name_model, device)
        self.supervised = dSup

    def blockC2d(self, Cin, Cout, kernel_size, stride=1, padding_mode='circular', normalize=True):
        """ Read as 'block Convolutional 2D' """
        # We define blocks tampering with both 2D data.
        # The kernel_size should be odd.
        # The padding is computed from the kernel size so that the convolution never impacts the dimensionality of the data.
        # The padding is circular because our images are map of the earth : the left-most and right-most pixel are geographical neighbors
        layers = [nn.Conv2d(Cin, Cout, kernel_size=kernel_size, padding=int((kernel_size-1)/2), padding_mode=padding_mode, stride=stride)]
        if normalize:
            layers.append(nn.BatchNorm2d(Cout))
            layers.append(nn.LeakyReLU())
        return layers

    def blockLinear(self, dim_in, dim_out, normalize=False):
        """ Block for Fully Connected layers """
        # Here we need to specify the dimensions of the data going in and out of each layer
        # This means that, when using this block, the initial shape of our images do have an impact on the model's architecture
        # (this is not necessarily the case with a fully convolutional network)
        layers = [nn.Linear(dim_in, dim_out)]
        if normalize:
            layers.append(nn.Sigmoid())
        return layers

class Cls_2Dto2D(ClsDiscriminator):
    """ This one takes two 2D inputs."""

    def __init__(self, name_model, device, chan_mult, kS1, kS2, kS3, latent_dim, lr, dSup, height, width, stride=5, n_chan=1, inp_chan=1):
        super().__init__(name_model, device, dSup)
        self.stride = stride
        lh = math.ceil(height / (self.stride ** 2))
        lw = math.ceil(width / (self.stride ** 2))
        # Convolutional layers for input
        self.block1 = nn.Sequential(*self.blockC2d(inp_chan, chan_mult, kernel_size=kS1))
        self.block2 = nn.Sequential(*self.blockC2d(chan_mult, chan_mult**2, kernel_size=kS2, stride=self.stride))
        self.block3 = nn.Sequential(*self.blockC2d(chan_mult**2, chan_mult**3, kernel_size=kS3, stride=self.stride))
        # Convolutional layers for target or generator output
        self.block1bis = nn.Sequential(*self.blockC2d(n_chan, chan_mult, kernel_size=kS1))
        self.block2bis = nn.Sequential(*self.blockC2d(chan_mult, chan_mult ** 2, kernel_size=kS2, stride=self.stride))
        self.block3bis = nn.Sequential(*self.blockC2d(chan_mult ** 2, chan_mult ** 3, kernel_size=kS3, stride=self.stride))
        # Fully connected layers
        self.blockL4 = nn.Sequential(*self.blockLinear((chan_mult**3) * 2 * lh * lw, latent_dim))
        self.blockL5 = nn.Sequential(*self.blockLinear(latent_dim, 1, normalize=True))

        # Other parameters such as learning rate, criterion and optimizer
        self.lr = lr
        self.criterion = nn.MSELoss(reduction='none')
        self.optimizer = optim.Adam(self.parameters(), lr=lr)

    def forward(self, inp, pr):
        # Treat input
        inpY = self.block1(inp)
        inpY = self.block2(inpY)
        inpY = self.block3(inpY)
        inpY = inpY.view(inpY.shape[0], -1)

        # Treat pred (or target)
        prY = self.block1bis(pr)
        prY = self.block2bis(prY)
        prY = self.block3bis(prY)
        prY = prY.view(prY.shape[0], -1)

        # Concatenation of input and prediction / target happens just before fully connected layers
        y = torch.cat([inpY, prY], 1)
        y = self.blockL4(y)
        out = self.blockL5(y)

        return out

class FFCls_2Dto2D(ClsDiscriminator):
    """ This one takes two 2D inputs."""

    def __init__(self, name_model, device, chan_mult, kS1, kS2, kS3, latent_dim, lr, dSup, height, width, stride=5, n_chan=1, inp_chan=1):
        super().__init__(name_model, device, dSup)
        self.stride = stride
        lh = math.ceil(height / (self.stride ** 2))
        lw = math.ceil(width / (self.stride ** 2))
        # These lists contain "copies" of different layers
        # nn.ModuleList allows to make sure the model recognises these layers as parts of itself
        self.block1 = nn.ModuleList([])
        self.block2 = nn.ModuleList([])
        self.block3 = nn.ModuleList([])

        for i in range(inp_chan):
            # Convolutional layers for input
            # Each layer has a number of "copies" equivalent to the number of input channels
            # These "copies" are created using the same parameters, but will in the end have different weights
            self.block1.append(nn.Sequential(*self.blockC2d(1, chan_mult, kernel_size=kS1)).to(self.device))
            self.block2.append(nn.Sequential(*self.blockC2d(chan_mult, chan_mult**2, kernel_size=kS2, stride=self.stride)).to(self.device))
            self.block3.append(nn.Sequential(*self.blockC2d(chan_mult**2, chan_mult**3, kernel_size=kS3, stride=self.stride)).to(self.device))

        # Convolutional layers for target or generator output
        self.block1bis = nn.Sequential(*self.blockC2d(n_chan, chan_mult, kernel_size=kS1))
        self.block2bis = nn.Sequential(*self.blockC2d(chan_mult, chan_mult ** 2, kernel_size=kS2, stride=self.stride))
        self.block3bis = nn.Sequential(*self.blockC2d(chan_mult ** 2, chan_mult ** 3, kernel_size=kS3, stride=self.stride))

        # Fully connected layers
        self.blockL4 = nn.Sequential(*self.blockLinear((chan_mult**3) * (inp_chan+1) * lh * lw, latent_dim))
        self.blockL5 = nn.Sequential(*self.blockLinear(latent_dim, 1, normalize=True))

        # Other parameters such as learning rate, criterion and optimizer
        self.lr = lr
        self.criterion = nn.MSELoss(reduction='none')
        self.optimizer = optim.Adam(self.parameters(), lr=lr)

    def forward(self, inp, pr):
        c = inp.shape[1]
        # Treat input
        # One list is created to store feature obtained at this level for different channels
        LinpY = [None]*c
        for i in range(c):
            # Each element of the list LinpY is the feature obtained for a different input channel
            tempY = self.block1[i](inp[:, i:i+1])
            tempY = self.block2[i](tempY)
            LinpY[i] = self.block3[i](tempY)
        # All features inside LinpY should have the same shape
        # So that we can fuse them together
        inpY = torch.cat(LinpY, dim=1)
        inpY = inpY.view(inpY.shape[0], -1)

        # Treat pred (or target)
        prY = self.block1bis(pr)
        prY = self.block2bis(prY)
        prY = self.block3bis(prY)
        prY = prY.view(prY.shape[0], -1)

        # Concatenation of input and prediction / target happens just before fully connected layers
        y = torch.cat([inpY, prY], 1)
        y = self.blockL4(y)
        out = self.blockL5(y)

        return out

class DFCls_2Dto2D(ClsDiscriminator):
    def __init__(self, name_model, device, chan_mult, kS1, kS2, kS3, latent_dim, lr, dSup, height, width, stride=5, inp_chan=1):
        super().__init__(name_model, device, dSup)
        self.cls = nn.ModuleList([])
        self.weights = nn.ParameterList([])
        # Creating submodules and parameters, and making sure the model recognises them as its own
        for i in range(inp_chan):
            # Creating one entire model for each input channel
            self.cls.append(Cls_2Dto2D(name_model + "-Cls-" + str(i), self.device, chan_mult, kS1, kS2, kS3, latent_dim, lr, dSup, height, width, stride=stride, inp_chan=1).to(self.device))
            # Creating one additional learnable weight for each input channel
            self.weights.append(nn.Parameter(torch.tensor(1.).float().to(self.device)))

        # Other parameters such as learning rate, criterion and optimizer
        self.lr = lr
        self.criterion = nn.MSELoss(reduction='none')
        self.optimizer = optim.Adam(self.parameters(), lr=lr)

    def forward(self, inp, pred):
        c = inp.shape[1]
        Ly = []
        for i in range(c):
            # Each input channel is sent to a different model
            # Ly contains all the model outputs, multiplied by a learnable weight
            Ly.append(self.cls[i].forward(inp[:, i:i + 1], pred))
            if i == 0:
                s = Ly[i] * self.weights[i]
            else:
                s = s + Ly[i] * self.weights[i]
        # The final output is the weighted mean of all outputs from all submodels (models that are part of the bigger one)
        y = s / sum(self.weights)
        return y

