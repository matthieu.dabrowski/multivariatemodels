# PyTorch libraries and modules
import matplotlib
import matplotlib.pyplot as plt
import math
import torch
import torch.nn as nn
import torch.optim as optim
matplotlib.use('Agg')
from .encoderdecoders import ED
plt.ioff()

########################################################################################################################
# Here we create object class (as opposed to abstract class)
# These class can therefore be called

# Create ED Model

class UNet_2Dto2D(ED):
    def __init__(self, name_model, device, linear_layer, latent_dim, ws, bcType, hardBC, p, kS1, kS2, kS3, kS4, chan_mult, lr, simLoss, height, width, stride=5, inp_chan=1, origData='Cams', padding_mode='circular'):
        super().__init__(name_model, device, ws, bcType, hardBC, p, simLoss, origData)
        self.linear_layer = linear_layer
        self.stride = stride
        # Computing the height and width of the tensors that will be sent to the fully connected layer (as dimension fo the data is needed for this layer)
        lh = math.ceil(height/(self.stride**2))
        lw = math.ceil(width/(self.stride**2))
        # Building the layers using the ED class functions
        # Convolutional layers
        self.block1 = nn.Sequential(*self.blockC2d(inp_chan, chan_mult, kernel_size=kS1, padding_mode=padding_mode))
        self.block2 = nn.Sequential(*self.blockC2d(chan_mult, chan_mult ** 2, kernel_size=kS2, stride=self.stride, padding_mode=padding_mode))
        self.block3 = nn.Sequential(*self.blockC2d(chan_mult ** 2, chan_mult ** 3, kernel_size=kS3, stride=self.stride, padding_mode=padding_mode))
        self.block4 = nn.Sequential(*self.blockC2d(chan_mult ** 3, chan_mult ** 4, kernel_size=kS4, padding_mode=padding_mode))
        self.block4bis = nn.Sequential(*self.blockC2d(chan_mult ** 3, chan_mult ** 4, kernel_size=kS4, padding_mode=padding_mode))
        # Some parameters depend on the presence of fully connected layers
        if self.linear_layer:
            self.blockNeg4 = nn.Sequential(*self.blockC2d(2*chan_mult ** 4, chan_mult ** 3, kernel_size=kS4, padding_mode=padding_mode))
        else:
            self.blockNeg4 = nn.Sequential(*self.blockC2d(chan_mult ** 4, chan_mult ** 3, kernel_size=kS4, padding_mode=padding_mode))
        self.blockNeg3 = nn.Sequential(*self.blockC2d(2*chan_mult ** 3, chan_mult ** 2, kernel_size=kS3, padding_mode=padding_mode))
        self.blockNeg2 = nn.Sequential(*self.blockC2d(2*chan_mult ** 2, chan_mult, kernel_size=kS2, padding_mode=padding_mode))
        self.blockNeg1 = nn.Sequential(*self.blockC2d(2*chan_mult, 1, kernel_size=kS1, padding_mode=padding_mode))
        # Fully connected layers
        self.blockLinEnc1 = nn.Sequential(*self.blockLinear((chan_mult ** 4) * lh * lw, latent_dim * 2))
        self.blockLinEnc2 = nn.Sequential(*self.blockLinear(latent_dim * 2, latent_dim))
        self.blockLinEnc2bis = nn.Sequential(*self.blockLinear(latent_dim * 2, latent_dim))
        self.blockLinDec2 = nn.Sequential(*self.blockLinear(latent_dim, latent_dim * 2))
        self.blockLinDec1 = nn.Sequential(*self.blockLinear(2*latent_dim * 2, (chan_mult ** 4) * lh * lw))

        # Other parameters such as learning rate, criterion and optimizer
        self.lr = lr
        self.criterion = nn.MSELoss(reduction='none')
        self.optimizer = optim.Adam(self.parameters(), lr=lr)

    def forward(self, x):
        # Encoding
        y1 = self.block1(x)
        # Shapes are saved to be used by the Upsample function
        shape1 = y1.shape
        y2 = self.block2(y1)
        shape2 = y2.shape
        y3 = self.block3(y2)
        # We just predict the latent representation
        # (in that case, y is not yet the latent representation)
        y4 = self.block4(y3)
        if self.linear_layer:
            # This shape is saved to reshape the 1D output of the last fully connected layer back to a 2D tensor
            shapeF = y4.shape
            yf = y4.view(shapeF[0], -1)
            yf1 = self.blockLinEnc1(yf)
            yf2 = self.blockLinEnc2(yf1)
            # yf1 is the smallest feature, the latent representation
            # Decoding
            ynegf2 = self.blockLinDec2(yf2)
            ynegf1 = self.blockLinDec1(torch.cat([ynegf2, yf1], dim=1))
            yneg = ynegf1.view(shapeF)
            yneg4 = self.blockNeg4(torch.cat([y4, yneg], dim=1))
        else:
            yneg4 = self.blockNeg4(y4)
        yneg3 = self.blockNeg3(torch.cat([yneg4, y3], dim=1))
        # The upsample function needs a shape as parameter
        # The last shape saved is the first used : the encoder and decoder are symmetrical
        up2 = torch.nn.Upsample(size=(shape2[2], shape2[3]), mode="bilinear")
        yneg3 = up2(yneg3)
        yneg2 = self.blockNeg2(torch.cat([yneg3, y2], dim=1))
        up1 = torch.nn.Upsample(size=(shape1[2], shape1[3]), mode="bilinear")
        yneg2 = up1(yneg2)
        decoded = self.blockNeg1(torch.cat([yneg2, y1], dim=1))
        return decoded

    def predict(self, x):
        return self.forward(x)

class FFUNet_2Dto2D(ED):
    def __init__(self, name_model, device, linear_layer, latent_dim, ws, bcType, hardBC, p, kS1, kS2, kS3, kS4, chan_mult, lr, simLoss, height, width, stride=5, inp_chan=1, origData='Cams'):
        super().__init__(name_model, device, ws, bcType, hardBC, p, simLoss, origData)
        self.linear_layer = linear_layer
        self.stride = stride
        lh = math.ceil(height/(self.stride**2))
        lw = math.ceil(width/(self.stride**2))
        # These lists contain "copies" of different layers
        # nn.ModuleList allows to make sure the model recognises these layers as parts of itself
        self.block1 = nn.ModuleList([])
        self.block2 = nn.ModuleList([])
        self.block3 = nn.ModuleList([])
        self.block4 = nn.ModuleList([])

        if self.linear_layer:
            self.blockLinEnc1 = nn.ModuleList([])
            self.blockLinEnc2 = nn.ModuleList([])

        for i in range(inp_chan):
            # For the encoder, each layer has a number of "copies" equal to the number of input channels
            # These "copies" are created using the same parameters, but will in the end have different weights
            self.block1.append(nn.Sequential(*self.blockC2d(1, chan_mult, kernel_size=kS1)).to(self.device))
            self.block2.append(nn.Sequential(*self.blockC2d(chan_mult, chan_mult ** 2, kernel_size=kS2, stride=self.stride)).to(self.device))
            self.block3.append(nn.Sequential(*self.blockC2d(chan_mult ** 2, chan_mult ** 3, kernel_size=kS3, stride=self.stride)).to(self.device))
            self.block4.append(nn.Sequential(*self.blockC2d(chan_mult ** 3, chan_mult ** 4, kernel_size=kS4)).to(self.device))

            if self.linear_layer:
                # Fully connected layers
                self.blockLinEnc1.append(nn.Sequential(*self.blockLinear((chan_mult ** 4) * lh * lw, latent_dim * 2)).to(self.device))
                self.blockLinEnc2.append(nn.Sequential(*self.blockLinear(latent_dim * 2, latent_dim)).to(self.device))

        # There is still only one decoder
        # Convolutional layers
        if self.linear_layer:
            self.blockNeg4 = nn.Sequential(*self.blockC2d((inp_chan+1)*chan_mult ** 4, chan_mult ** 3, kernel_size=kS4))
        else:
            self.blockNeg4 = nn.Sequential(*self.blockC2d(inp_chan*chan_mult ** 4, chan_mult ** 3, kernel_size=kS4))
        self.blockNeg3 = nn.Sequential(*self.blockC2d((inp_chan+1)*chan_mult ** 3, chan_mult ** 2, kernel_size=kS3))
        self.blockNeg2 = nn.Sequential(*self.blockC2d((inp_chan+1)*chan_mult ** 2, chan_mult, kernel_size=kS2))
        self.blockNeg1 = nn.Sequential(*self.blockC2d((inp_chan+1)*chan_mult, 1, kernel_size=kS1))
        # Fully connected layers
        self.blockLinDec2 = nn.Sequential(*self.blockLinear(inp_chan * latent_dim, latent_dim * 2))
        self.blockLinDec1 = nn.Sequential(*self.blockLinear((inp_chan + 1) * latent_dim * 2, (chan_mult ** 4) * lh * lw))

        # Other parameters such as learning rate, criterion and optimizer
        self.lr = lr
        self.criterion = nn.MSELoss(reduction='none')
        self.optimizer = optim.Adam(self.parameters(), lr=lr)

    def forward(self, x):
        c = x.shape[1]
        # We create lists of features
        # One list for each dimensionality / feature level
        # All features in the same list have the same size
        Ly1 = []
        Ly2 = []
        Ly3 = []
        Ly4 = []
        Lyf = []
        Lyf1 = []
        Lyf2 = []
        for i in range(c):
            # Encoding
            Ly1.append(self.block1[i](x[:, i:i+1]))
            Ly2.append(self.block2[i](Ly1[i]))
            Ly3.append(self.block3[i](Ly2[i]))
            Ly4.append(self.block4[i](Ly3[i]))
            if self.linear_layer:
                Lyf.append(Ly4[i].view(Ly4[i].shape[0], -1))
                Lyf1.append(self.blockLinEnc1[i](Lyf[i]))
                Lyf2.append(self.blockLinEnc2[i](Lyf1[i]))
        # All features inside the same list (and therefore with the same shape) are fused together
        # This way we still obtain one feature for each dimensionality level
        y1 = torch.cat(Ly1, dim=1)
        y2 = torch.cat(Ly2, dim=1)
        y3 = torch.cat(Ly3, dim=1)
        y4 = torch.cat(Ly4, dim=1)
        yf1 = torch.cat(Lyf1, dim=1)
        yf2 = torch.cat(Lyf2, dim=1)
        # These features will be sent to the decoder through the skip connections as usual
        # Decoding
        if self.linear_layer:
            shapeF = Ly4[0].shape
            ynegf2 = self.blockLinDec2(yf2)
            ynegf1 = self.blockLinDec1(torch.cat([ynegf2, yf1], dim=1))
            yneg = ynegf1.view(shapeF)
            yneg4 = self.blockNeg4(torch.cat([y4, yneg], dim=1))
        else:
            yneg4 = self.blockNeg4(y4)
        yneg3 = self.blockNeg3(torch.cat([yneg4, y3], dim=1))
        shape2 = Ly2[0].shape
        up2 = torch.nn.Upsample(size=(shape2[2], shape2[3]), mode="bilinear")
        yneg3 = up2(yneg3)
        yneg2 = self.blockNeg2(torch.cat([yneg3, y2], dim=1))
        shape1 = Ly1[0].shape
        up1 = torch.nn.Upsample(size=(shape1[2], shape1[3]), mode="bilinear")
        yneg2 = up1(yneg2)
        decoded = self.blockNeg1(torch.cat([yneg2, y1], dim=1))
        return decoded

    def predict(self, x):
        return self.forward(x)

class DFUNet_2Dto2D(ED):
    def __init__(self, name_model, device, linear_layer, latent_dim, ws, bcType, hardBC, p, kS1, kS2, kS3, kS4,
                 chan_mult, lr, simLoss, height, width, stride=5, inp_chan=1, origData='Cams'):
        super().__init__(name_model, device, ws, bcType, hardBC, p, simLoss, origData)
        self.unets = nn.ModuleList([])
        self.weights = nn.ParameterList([])
        # Creating submodules and parameters, and making sure the model recognises them as its own
        for i in range(inp_chan):
            # Creating one entire model for each input channel
            self.unets.append(UNet_2Dto2D(name_model + "-Unet-" + str(i), self.device, linear_layer, latent_dim, ws, bcType, hardBC, p, kS1, kS2, kS3, kS4, chan_mult, lr, simLoss, height, width, stride=stride, inp_chan=1).to(self.device))
            # Creating one additional learnable weight for each input channel
            self.weights.append(nn.Parameter(torch.tensor(1.)).float().to(self.device))

        # Other parameters such as learning rate, criterion and optimizer
        self.lr = lr
        self.criterion = nn.MSELoss(reduction='none')
        self.optimizer = optim.Adam(self.parameters(), lr=lr)

    def forward(self, x):
        c = x.shape[1]
        Ly = []
        for i in range(c):
            # Each input channel is sent to a different model
            # Ly contains all the model outputs, multiplied by a learnable weight
            Ly.append(self.unets[i].forward(x[:, i:i + 1]))
            if i == 0:
                s = Ly[i]*self.weights[i]
            else:
                s = s + Ly[i]*self.weights[i]
        # The final output is the weighted mean of all outputs from all submodels (models that are part of the bigger one)
        y = s/sum(self.weights)
        return y


class HybridUNet2FF(ED):
    # 3 encoders : one for the wind, one for the temperature, one for the rest
    def __init__(self, name_model, device, linear_layer, latent_dim, ws, bcType, hardBC, p, kS1, kS2, kS3, kS4,
                 chan_mult, lr, simLoss, height, width, stride=5, origData='Cams'):
        super().__init__(name_model, device, ws, bcType, hardBC, p, simLoss, origData)
        self.inp_chan = 3
        self.linear_layer = linear_layer
        self.stride = stride
        lh = math.ceil(height / (self.stride ** 2))
        lw = math.ceil(width / (self.stride ** 2))
        # These lists contain "copies" of different layers
        # nn.ModuleList allows to make sure the model recognises these layers as parts of itself
        self.block1 = nn.ModuleList([])
        self.block2 = nn.ModuleList([])
        self.block3 = nn.ModuleList([])
        self.block4 = nn.ModuleList([])

        if self.linear_layer:
            self.blockLinEnc1 = nn.ModuleList([])
            self.blockLinEnc2 = nn.ModuleList([])

        self.block1.append(nn.Sequential(*self.blockC2d(4, chan_mult, kernel_size=kS1)).to(self.device))
        self.block1.append(nn.Sequential(*self.blockC2d(2, chan_mult, kernel_size=kS1)).to(self.device))
        self.block1.append(nn.Sequential(*self.blockC2d(1, chan_mult, kernel_size=kS1)).to(self.device))

        for i in range(len(self.block1)):
            # For the encoder, each layer has a number of "copies" equal to the number of input channels
            # These "copies" are created using the same parameters, but will in the end have different weights

            self.block2.append(
                nn.Sequential(*self.blockC2d(chan_mult, chan_mult ** 2, kernel_size=kS2, stride=self.stride)).to(
                    self.device))
            self.block3.append(
                nn.Sequential(*self.blockC2d(chan_mult ** 2, chan_mult ** 3, kernel_size=kS3, stride=self.stride)).to(
                    self.device))
            self.block4.append(
                nn.Sequential(*self.blockC2d(chan_mult ** 3, chan_mult ** 4, kernel_size=kS4)).to(self.device))

            if self.linear_layer:
                # Fully connected layers
                self.blockLinEnc1.append(
                    nn.Sequential(*self.blockLinear((chan_mult ** 4) * lh * lw, latent_dim * 2)).to(self.device))
                self.blockLinEnc2.append(nn.Sequential(*self.blockLinear(latent_dim * 2, latent_dim)).to(self.device))

        # There is still only one decoder
        # Convolutional layers
        if self.linear_layer:
            self.blockNeg4 = nn.Sequential(
                *self.blockC2d((self.inp_chan + 1) * chan_mult ** 4, chan_mult ** 3, kernel_size=kS4))
        else:
            self.blockNeg4 = nn.Sequential(*self.blockC2d(self.inp_chan * chan_mult ** 4, chan_mult ** 3, kernel_size=kS4))
        self.blockNeg3 = nn.Sequential(*self.blockC2d((self.inp_chan + 1) * chan_mult ** 3, chan_mult ** 2, kernel_size=kS3))
        self.blockNeg2 = nn.Sequential(*self.blockC2d((self.inp_chan + 1) * chan_mult ** 2, chan_mult, kernel_size=kS2))
        self.blockNeg1 = nn.Sequential(*self.blockC2d((self.inp_chan + 1) * chan_mult, 1, kernel_size=kS1))
        # Fully connected layers
        self.blockLinDec2 = nn.Sequential(*self.blockLinear(self.inp_chan * latent_dim, latent_dim * 2))
        self.blockLinDec1 = nn.Sequential(
            *self.blockLinear((self.inp_chan + 1) * latent_dim * 2, (chan_mult ** 4) * lh * lw))

        # Other parameters such as learning rate, criterion and optimizer
        self.lr = lr
        self.criterion = nn.MSELoss(reduction='none')
        self.optimizer = optim.Adam(self.parameters(), lr=lr)

    def forward(self, input_):
        t = input_[:, -1:]
        v = input_[:, -3:-1]
        inp = input_[:, :-3]
        x = [inp, v, t]
        # We create lists of features
        # One list for each dimensionality / feature level
        # All features in the same list have the same size
        Ly1 = []
        Ly2 = []
        Ly3 = []
        Ly4 = []
        Lyf = []
        Lyf1 = []
        Lyf2 = []
        for i in range(len(x)):
            # Encoding
            Ly1.append(self.block1[i](x[i]))
            Ly2.append(self.block2[i](Ly1[i]))
            Ly3.append(self.block3[i](Ly2[i]))
            Ly4.append(self.block4[i](Ly3[i]))
            if self.linear_layer:
                Lyf.append(Ly4[i].view(Ly4[i].shape[0], -1))
                Lyf1.append(self.blockLinEnc1[i](Lyf[i]))
                Lyf2.append(self.blockLinEnc2[i](Lyf1[i]))
        # All features inside the same list (and therefore with the same shape) are fused together
        # This way we still obtain one feature for each dimensionality level
        y1 = torch.cat(Ly1, dim=1)
        y2 = torch.cat(Ly2, dim=1)
        y3 = torch.cat(Ly3, dim=1)
        y4 = torch.cat(Ly4, dim=1)
        yf1 = torch.cat(Lyf1, dim=1)
        yf2 = torch.cat(Lyf2, dim=1)
        # These features will be sent to the decoder through the skip connections as usual
        # Decoding
        if self.linear_layer:
            shapeF = Ly4[0].shape
            ynegf2 = self.blockLinDec2(yf2)
            ynegf1 = self.blockLinDec1(torch.cat([ynegf2, yf1], dim=1))
            yneg = ynegf1.view(shapeF)
            yneg4 = self.blockNeg4(torch.cat([y4, yneg], dim=1))
        else:
            yneg4 = self.blockNeg4(y4)
        yneg3 = self.blockNeg3(torch.cat([yneg4, y3], dim=1))
        shape2 = Ly2[0].shape
        up2 = torch.nn.Upsample(size=(shape2[2], shape2[3]), mode="bilinear")
        yneg3 = up2(yneg3)
        yneg2 = self.blockNeg2(torch.cat([yneg3, y2], dim=1))
        shape1 = Ly1[0].shape
        up1 = torch.nn.Upsample(size=(shape1[2], shape1[3]), mode="bilinear")
        yneg2 = up1(yneg2)
        decoded = self.blockNeg1(torch.cat([yneg2, y1], dim=1))
        return decoded

class HybridUNet1FF(ED):
    # 2 encoders : one for the wind and the temperature, one for the rest
    def __init__(self, name_model, device, linear_layer, latent_dim, ws, bcType, hardBC, p, kS1, kS2, kS3, kS4,
                 chan_mult, lr, simLoss, height, width, stride=5, origData='Cams'):
        super().__init__(name_model, device, ws, bcType, hardBC, p, simLoss, origData)
        self.inp_chan = 2
        self.linear_layer = linear_layer
        self.stride = stride
        lh = math.ceil(height / (self.stride ** 2))
        lw = math.ceil(width / (self.stride ** 2))
        # These lists contain "copies" of different layers
        # nn.ModuleList allows to make sure the model recognises these layers as parts of itself
        self.block1 = nn.ModuleList([])
        self.block2 = nn.ModuleList([])
        self.block3 = nn.ModuleList([])
        self.block4 = nn.ModuleList([])

        if self.linear_layer:
            self.blockLinEnc1 = nn.ModuleList([])
            self.blockLinEnc2 = nn.ModuleList([])

        self.block1.append(nn.Sequential(*self.blockC2d(4, chan_mult, kernel_size=kS1)).to(self.device))
        self.block1.append(nn.Sequential(*self.blockC2d(3, chan_mult, kernel_size=kS1)).to(self.device))

        for i in range(len(self.block1)):
            # For the encoder, each layer has a number of "copies" equal to the number of input channels
            # These "copies" are created using the same parameters, but will in the end have different weights

            self.block2.append(
                nn.Sequential(*self.blockC2d(chan_mult, chan_mult ** 2, kernel_size=kS2, stride=self.stride)).to(
                    self.device))
            self.block3.append(
                nn.Sequential(*self.blockC2d(chan_mult ** 2, chan_mult ** 3, kernel_size=kS3, stride=self.stride)).to(
                    self.device))
            self.block4.append(
                nn.Sequential(*self.blockC2d(chan_mult ** 3, chan_mult ** 4, kernel_size=kS4)).to(self.device))

            if self.linear_layer:
                # Fully connected layers
                self.blockLinEnc1.append(
                    nn.Sequential(*self.blockLinear((chan_mult ** 4) * lh * lw, latent_dim * 2)).to(self.device))
                self.blockLinEnc2.append(nn.Sequential(*self.blockLinear(latent_dim * 2, latent_dim)).to(self.device))

        # There is still only one decoder
        # Convolutional layers
        if self.linear_layer:
            self.blockNeg4 = nn.Sequential(
                *self.blockC2d((self.inp_chan + 1) * chan_mult ** 4, chan_mult ** 3, kernel_size=kS4))
        else:
            self.blockNeg4 = nn.Sequential(*self.blockC2d(self.inp_chan * chan_mult ** 4, chan_mult ** 3, kernel_size=kS4))
        self.blockNeg3 = nn.Sequential(*self.blockC2d((self.inp_chan + 1) * chan_mult ** 3, chan_mult ** 2, kernel_size=kS3))
        self.blockNeg2 = nn.Sequential(*self.blockC2d((self.inp_chan + 1) * chan_mult ** 2, chan_mult, kernel_size=kS2))
        self.blockNeg1 = nn.Sequential(*self.blockC2d((self.inp_chan + 1) * chan_mult, 1, kernel_size=kS1))
        # Fully connected layers
        self.blockLinDec2 = nn.Sequential(*self.blockLinear(self.inp_chan * latent_dim, latent_dim * 2))
        self.blockLinDec1 = nn.Sequential(
            *self.blockLinear((self.inp_chan + 1) * latent_dim * 2, (chan_mult ** 4) * lh * lw))

        # Other parameters such as learning rate, criterion and optimizer
        self.lr = lr
        self.criterion = nn.MSELoss(reduction='none')
        self.optimizer = optim.Adam(self.parameters(), lr=lr)

    def forward(self, input_):
        vt = input_[:, -3:]
        inp = input_[:, :-3]
        x = [inp, vt]
        # We create lists of features
        # One list for each dimensionality / feature level
        # All features in the same list have the same size
        Ly1 = []
        Ly2 = []
        Ly3 = []
        Ly4 = []
        Lyf = []
        Lyf1 = []
        Lyf2 = []
        for i in range(len(x)):
            # Encoding
            Ly1.append(self.block1[i](x[i]))
            Ly2.append(self.block2[i](Ly1[i]))
            Ly3.append(self.block3[i](Ly2[i]))
            Ly4.append(self.block4[i](Ly3[i]))
            if self.linear_layer:
                Lyf.append(Ly4[i].view(Ly4[i].shape[0], -1))
                Lyf1.append(self.blockLinEnc1[i](Lyf[i]))
                Lyf2.append(self.blockLinEnc2[i](Lyf1[i]))
        # All features inside the same list (and therefore with the same shape) are fused together
        # This way we still obtain one feature for each dimensionality level
        y1 = torch.cat(Ly1, dim=1)
        y2 = torch.cat(Ly2, dim=1)
        y3 = torch.cat(Ly3, dim=1)
        y4 = torch.cat(Ly4, dim=1)
        yf1 = torch.cat(Lyf1, dim=1)
        yf2 = torch.cat(Lyf2, dim=1)
        # These features will be sent to the decoder through the skip connections as usual
        # Decoding
        if self.linear_layer:
            shapeF = Ly4[0].shape
            ynegf2 = self.blockLinDec2(yf2)
            ynegf1 = self.blockLinDec1(torch.cat([ynegf2, yf1], dim=1))
            yneg = ynegf1.view(shapeF)
            yneg4 = self.blockNeg4(torch.cat([y4, yneg], dim=1))
        else:
            yneg4 = self.blockNeg4(y4)
        yneg3 = self.blockNeg3(torch.cat([yneg4, y3], dim=1))
        shape2 = Ly2[0].shape
        up2 = torch.nn.Upsample(size=(shape2[2], shape2[3]), mode="bilinear")
        yneg3 = up2(yneg3)
        yneg2 = self.blockNeg2(torch.cat([yneg3, y2], dim=1))
        shape1 = Ly1[0].shape
        up1 = torch.nn.Upsample(size=(shape1[2], shape1[3]), mode="bilinear")
        yneg2 = up1(yneg2)
        decoded = self.blockNeg1(torch.cat([yneg2, y1], dim=1))
        return decoded