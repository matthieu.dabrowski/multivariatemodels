# Importing libraries and modules
import math
import os
import matplotlib
import matplotlib.cm
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
import torch
import torch.nn as nn
from piq import FSIMLoss
from dataset_generation import BCrandom, BCrandomLand, BCfromFile, BCnaive, ApplyBC, BCloss, PlotPrep, Filter, Distrib, AirQualityQuantif
import time
import json
from sklearn.metrics import confusion_matrix

from .baseModel import baseModel
# torch.autograd.set_detect_anomaly(mode=True, check_nan=True)
cuda = torch.cuda.is_available()
device_name = "cuda" if cuda else "cpu"
device = torch.device(device_name)
cpu = torch.device("cpu")

matplotlib.use('Agg')
plt.ioff()

class ED(baseModel):
    """ This class is supposed to be used as an abstract class for all the autoencoders.
        As such, it should never be called."""
    def __init__(self, name_model, device, ws, bcType, hardBC, p, simLoss, origData):
        super().__init__(name_model, device)
        # Easier for the user to specify when a model is not supervised
        # But easier in the code to specify what happens for supervision
        self.supervised = not ws
        self.simLoss = simLoss
        if not self.supervised:
            self.hardBC = hardBC
            # Creating the bc according to the bc type
            if origData!='Cams':
                self.bc = BCrandom(p, origData).to(device)
            else:
                if bcType == "naive":
                    self.bc = BCnaive().to(device)
                elif bcType == "file":
                    self.bc = BCfromFile().to(device)
                elif bcType == "random":
                    if p <= 0:
                        self.bc = None
                    self.bc = BCrandom(p, origData).to(device)
                elif bcType == "randomOnLand":
                    if p <= 0:
                        self.bc = None
                    self.bc = BCrandomLand(p).to(device)
                else:
                    raise Exception("bc_type argument should be a string from the following list : \n"
                                                  "[naive, file, random, randomOnFile]. \n Got : "+bcType)

    # We start by defining several block that we may use as layers.
    def blockC2d(self, Cin, Cout, kernel_size, stride=1, padding_mode='circular', normalize=True):
        """ Read as 'block Convolutional 2D' """
        # We define blocks tampering with both 2D data.
        # The kernel_size should be odd.
        # The padding is computed from the kernel size so that the convolution never impacts the dimensionality of the data.
        # The padding is circular because our images are map of the earth : the left-most and right-most pixel are geographical neighbors
        layers = [nn.Conv2d(Cin, Cout, kernel_size=kernel_size, padding=int((kernel_size-1)/2), padding_mode=padding_mode, stride=stride)]
        if normalize:
            layers.append(nn.BatchNorm2d(Cout))
            layers.append(nn.LeakyReLU())
        return layers

    def blockLinear(self, dim_in, dim_out):
        """ Block for Fully Connected layers """
        # Here we need to specify the dimensions of the data going in and out of each layer
        # This means that, when using this block, the initial shape of our images do have an impact on the model's architecture
        # (this is not necessarily the case with a fully convolutional network)
        layers = [nn.Linear(dim_in, dim_out), nn.LeakyReLU()]
        return layers

    def trainModel(self, dataLoader, trainDateList, num_epochs, maxTarg: float, minTarg: float, save=True, parallel=True):
        """ This function allows to train the model using data from a torch.DataLoader object.
            We also need to specify the number of epochs, and a boolean specifying if we want to save our trained model (default is True).
            It returns a list of losses. """
        self.train()
        losses = {"Iteration":[]}
        # newMax is mainly useful for FSIMLoss
        newMax = (maxTarg - minTarg)
        print("newMax + 0.1 : ", newMax+0.1)
        if self.simLoss:
            fsimLoss = FSIMLoss(chromatic=False, data_range=newMax + 0.1, reduction='none')
        # Depending on the parameters, different losses can be used
        if self.supervised:
            losses["Supervised loss"] = []
            if self.simLoss:
                losses["FSIM loss"] = []
                losses["Total loss"] = []
        else:
            if self.bc is not None:
                if not self.hardBC:
                    losses["BC loss"] = []
                    lossBC = BCloss(self.bc)
                else:
                    appBC = ApplyBC(self.bc)
        def printLoss():
            # This function allows us to display the training loss and save the plot
            # This allows to check on the training process while debugging
            folder = self.sourceFolder+self.name_model
            x = losses["Iteration"]
            print("loss_train keys (except iteration) : \n", list(losses.keys())[1:])
            losses_disp = [{"Iteration": i} for i in losses["Iteration"]]
            for k in list(losses.keys())[1:]:
                # Plotting
                plt.plot(x, losses[k], label=k)
                for i in losses["Iteration"]:
                    # Writing a new dict of losses, easier to read
                    losses_disp[i][k] = losses[k][i]
            # back to plotting the losses and saving the plot
            plt.semilogy(base=10)
            plt.xlabel("Iterations")
            plt.ylabel("Loss value")
            plt.legend()
            os.makedirs(folder + "/", exist_ok=True)
            plt.savefig(folder + "/fig_losses_train.pdf")
            plt.close()
            # saving the train losses dictionary
            with open(folder + "/losses_train.txt", "w+") as fTrain:
                json.dump(losses_disp, fTrain, indent=2)
                fTrain.close()
            print("\n printLoss done, folder : " + folder + "/ \n")

        # Loading the checkpoint if it exists (there should be either 1 or 0)
        if os.path.exists(self.sourceFolder + self.name_model + "/checkpoint.pl"):
            [start_epoch, loss, losses, train_time] = self.loadCheckpoint()
        else:
            start_epoch = 0
            train_time = 0
        # Using data parallelism when possible to speed up the training
        if cuda and parallel:
            forward = nn.DataParallel(self, output_device=self.device)
        else:
            forward = self.forward
        print("Starting training")
        start_time = time.time()
        for epoch in range(start_epoch, num_epochs):
            for idx_batch, (input_, target) in enumerate(dataLoader):
                if torch.isnan(input_).any() or torch.isinf(input_).any():
                    printLoss()
                    raise TypeError("\n NaN or Infinity found in input for sample at date : ", trainDateList[idx_batch].strftime("%Hh-%d/%m/%Y"))
                if torch.isnan(target).any() or torch.isinf(target).any():
                    printLoss()
                    raise TypeError("\n NaN or Infinity found in target for sample at date : ", trainDateList[idx_batch].strftime("%Hh-%d/%m/%Y"))
                input_ = input_.float()
                target = target.float()
                input_ = input_.to(self.device)
                target = target.to(self.device)
                # Most straightforward inference process
                # Checking model parameters
                for name, param in self.named_parameters():
                    if torch.isnan(param).any() or torch.isinf(param).any():
                        printLoss()
                        raise TypeError("\n NaN or Infinity found in model parameter ", name,", at epoch : ", str(epoch), ", sample at date : ", trainDateList[idx_batch].strftime("%Hh-%d/%m/%Y"),
                                        "and here is the parameter in question : \n", param)
                pred = forward(input_)
                if torch.isnan(pred).any() or torch.isinf(pred).any():
                    printLoss()
                    raise TypeError("\n NaN or Infinity found in pred for sample at date : " + trainDateList[idx_batch].strftime("%Hh-%d/%m/%Y") + ", here is the tensor : \n", pred,
                                    "\n and here is the input : \n", input_)
                pred = pred.to(self.device)
                losses["Iteration"].append(int(len(dataLoader) * epoch + idx_batch))
                # Computing the losses
                if self.supervised:
                    supervised_loss = self.criterion(pred, target)
                    if torch.isnan(supervised_loss).any() or torch.isinf(supervised_loss).any():
                        printLoss()
                        raise TypeError("\n NaN or Infinity found in Supervised Loss : \n", supervised_loss)
                    # We put the losses on a list in dictionary for saving purposes
                    losses["Supervised loss"].append(torch.mean(supervised_loss).item())
                    loss = torch.mean(supervised_loss)
                    if self.simLoss:
                        # Some processing on the output and target is necessary for the FSIMLoss to be computed
                        # Essentially the values of these tensors must be comprised bteween 0 and newMax
                        pred = torch.nan_to_num((pred - min(torch.min(pred), minTarg)), nan=torch.nan, posinf=newMax, neginf=-newMax)
                        if torch.isnan(pred).any() or torch.isinf(pred).any():
                            printLoss()
                            raise TypeError("\n NaN or Infinity found in pred for sample at date : " + trainDateList[
                                idx_batch].strftime("%Hh-%d/%m/%Y") + ", here is the tensor : \n", pred)
                        pred = ((pred > 0) * (pred < newMax)) * pred + (pred >= newMax) * newMax
                        if torch.isnan(pred).any() or torch.isinf(pred).any():
                            printLoss()
                            raise TypeError("\n NaN or Infinity found in pred for sample at date : " + trainDateList[
                                idx_batch].strftime("%Hh-%d/%m/%Y") + ", here is the tensor : \n", pred)
                        target = torch.nan_to_num((target - minTarg), nan=torch.nan, posinf=newMax, neginf=-newMax)
                        if torch.isnan(target).any() or torch.isinf(target).any():
                            printLoss()
                            raise TypeError("\n NaN or Infinity found in target for sample at date : ",
                                            trainDateList[idx_batch].strftime("%Hh-%d/%m/%Y"))
                        target = ((target > 0) * (target < newMax)) * target + (target >= newMax) * newMax
                        if torch.isnan(target).any() or torch.isinf(target).any():
                            printLoss()
                            raise TypeError("\n NaN or Infinity found in target for sample at date : ",
                                            trainDateList[idx_batch].strftime("%Hh-%d/%m/%Y"))
                        fsim_loss = fsimLoss(pred, target)
                        if torch.isnan(fsim_loss).any() or torch.isinf(fsim_loss).any():
                            printLoss()
                            raise TypeError("\n NaN or Infinity found in FSIMloss : \n", fsim_loss)
                        losses["FSIM loss"].append(torch.mean(fsim_loss).item())
                        # When using both MSE and FSIM the total loss is a combination of the two, but MSE remains more important
                        sup_loss_rescaled = supervised_loss/newMax
                        loss = (3*torch.mean(sup_loss_rescaled) + torch.mean(fsim_loss))/4
                        losses["Total loss"].append(loss.item())
                else:
                    if (self.bc is not None):
                        if self.hardBC:
                            pred = appBC(target, pred)
                        else:
                            bc_loss = torch.mean(lossBC(target, pred))
                            losses["BC loss"].append(bc_loss)
                            loss = bc_loss
                if torch.isnan(loss).any() or torch.isinf(loss).any():
                    printLoss()
                    raise TypeError("\n NaN or Infinity found in loss : \n", loss)
                self.optimizer.zero_grad()
                # Retro-propagation
                loss.backward()
                # Clipping the gradients with a max set to 1
                # This avoids exploding gradients problems
                torch.nn.utils.clip_grad_norm_(self.parameters(), 1)
                self.optimizer.step()
            if (epoch + 1) % 10 == 0 or (epoch + 1) == num_epochs:
                # Regularly printing out epoch and loss to follow training
                print(f"Epoch : {epoch + 1} | Loss : {loss.item():.4e}")
                train_time = train_time + (time.time() - start_time)
                start_time = time.time()
                # Regularly saving a checkpoint in case the server stops for some reason
                self.saveCheckpoint(epoch=epoch, loss=loss, losses=losses, train_time=train_time)
        print("End of training")
        if save:
            # Saving the fully trained model
            self.saveModel(losses, train_time)
        return losses

    def saveOne2D(self, target, predic, loss, input_, r_loss, ct, cp, ce, mbe, mbe_r, sim_idx, testPeriod, date, error, e_bw, unfilter=False, minTarg=None, maxTarg=None):
        """ This function allows to save the test results of one single sample """
        # We generate one file for each tuple (target, prediction)
        if testPeriod is not None:
            path = "results/" + self.name_model + "/" + testPeriod + "/results_test_" + date.strftime(
                "%d-%m-%Y_%Hh") + ".pdf"
        else:
            path = "results/" + self.name_model + "/results_test_" + date.strftime(
                "%d-%m-%Y_%Hh") + ".pdf"
        predic = predic[0]
        target = target[0]
        input_ = input_[0]
        inp = input_
        pred = predic
        targ = target
        prep = PlotPrep()
        # PlotPrep is a function that prepares our tensors to be displayed on a file
        # It mainly makes these matrices more easily interpretable
        pred = prep(pred, norm=True, minTarg=minTarg, maxTarg=maxTarg/8)
        inp = prep(inp, norm=True)
        targ = prep(targ, norm=True, minTarg=minTarg, maxTarg=maxTarg/8)
        loss = loss.item()
        r_loss = r_loss.item()
        sm = matplotlib.cm.ScalarMappable(norm=None, cmap=plt.get_cmap("jet"))
        # Only save the sample results if the relative error is greater than 1
        # Or if we choose to save them all
        if r_loss >= 1 or unfilter:
            mbe = mbe.item()
            mbe_r = mbe_r.item()
            sim_idx = sim_idx.item()
            plt.suptitle("Test results.")
            # Each file contains two images : one for the prediction and one for the target
            ax0 = plt.subplot(2, 2, 1)
            # Input at top-left
            i0=ax0.imshow(inp)
            # Printing a colorbar to enhance interpretability
            plt.colorbar(sm, ax=ax0, ticks=np.linspace(torch.min(input_).to(cpu), torch.max(input_).to(cpu), 5), orientation="vertical", label="no units (AOD)")
            ax0.set_title("Input (AOD)")
            ax1 = plt.subplot(2, 2, 3)
            # Prediction at bottom-left
            i1=ax1.imshow(pred)
            # Printing a colorbar to enhance interpretability
            plt.colorbar(sm, ax=ax1, ticks=np.linspace(minTarg.to(cpu), maxTarg.to(cpu), 5), orientation="vertical", label="\u03BCg/m\N{superscript three}")
            # Writing the title with several metrics in it
            ax1.set_title(f"Prediction. Test error : {loss:1.3f}\u03BCg/m\N{superscript three} or {r_loss * 100:1.3f}%."
                          f" \n MBE : {mbe:1.3f}\u03BCg/m\N{superscript three} or {mbe_r * 100:1.3f}%."
                          f" \n sim-index loss : {sim_idx * 100:1.3f}%.")
            ax2 = plt.subplot(2, 2, 4)
            # Target at bottom-right
            i2=ax2.imshow(targ)
            # Printing a colorbar to enhance interpretability
            plt.colorbar(sm, ax=ax2, ticks=np.linspace(minTarg.to(cpu), maxTarg.to(cpu), 5), orientation="vertical", label="\u03BCg/m\N{superscript three}")
            ax2.set_title("Target.")
            # Saving test result
            plt.savefig(path)
            plt.close()
            if error:
                # If the error parameter is true, images describing the model error are also saved
                ct = ct[0] / 4
                cp = cp[0] / 4
                ce = ce.item()
                ct = prep(ct)
                cp = prep(cp)
                # We generate one file for each tuple (target, prediction)
                if testPeriod is not None:
                    path2 = "results/" + self.name_model + "/" + testPeriod + "/error_test_" + date.strftime(
                        "%d-%m-%Y_%Hh") + ".pdf"
                else:
                    path2 = "results/" + self.name_model + "/error_test_" + date.strftime(
                        "%d-%m-%Y_%Hh") + ".pdf"
                errorMat = torch.abs(target - predic)
                errorMat = prep(errorMat, error=True, bw=e_bw, norm=True)
                plt.suptitle("Test error visualisation.")
                ax1 = plt.subplot(2, 2, 1)
                if e_bw:
                    ax1.imshow(errorMat, cmap='gray')
                else:
                    ax1.imshow(errorMat)
                ax1.set_title(
                    f"Error visualisation. Test error : {loss:1.3f}\u03BCg/m\N{superscript three} or {r_loss * 100:1.3f}%.")
                ax2 = plt.subplot(2, 2, 3)
                ax2.imshow(cp)
                ax2.set_title(f"Segmented prediction. \n Classification error : {ce:1.3f}.")
                ax3 = plt.subplot(2, 2, 4)
                ax3.imshow(ct)
                ax3.set_title("Segmented target.")
                plt.savefig(path2)
                plt.close()


    def test(self, dataLoader, testDateList, maxTarg: float, minTarg: float, save=True, error=False, log=False, e_bw=False, parallel=True, unfilter=False, QA=False):
        """ This function allows us to test our model using data from a torch.DataLoader object.
            It returns a list of the targets of this test, the predictions obtained, and the losses. """
        if save:
            os.makedirs("results/" + self.name_model + "/", exist_ok=True)
            # os.makedirs("results/" + self.name_model + "/" + testPeriod + "/", exist_ok=True)
        testDates = mdates.date2num(np.array(testDateList))
        self.eval()
        # Creating lists to store the metrics
        losses = []
        r_losses = []
        times = []
        closs = []
        MBE = []
        MBE_R = []
        sim_idces = []
        if QA:
            qas = []
        # Setting up the FSIM loss
        newMax = (maxTarg-minTarg)
        fsimLoss = FSIMLoss(chromatic=False, data_range=newMax + 0.1)
        # Doing parallel processing to speed up testing whenever possible
        if cuda and parallel:
            forward = nn.DataParallel(self, output_device=self.device)
        else:
            forward = self.forward
        if log:
            dis = Distrib()
        if (not self.supervised) and self.hardBC and (self.bc is not None) :
            appBC = ApplyBC(self.bc)
        with torch.no_grad():
            for idx_batch, (input_, target) in enumerate(dataLoader):
                input_ = input_.float()
                target = target.float()
                input_ = input_.to(self.device)
                target = target.to(self.device)
                start_time = time.time()
                # Most forward way to realise the inference
                pred = forward(input_)
                pred = pred.to(self.device)
                pred_time = time.time() - start_time
                # Separating the main input (AOD) from the rest if necessary
                if input_.shape[1] > 1:
                    input_ = input_[:, 0:1]
                if (not self.supervised) and self.hardBC and (self.bc is not None):
                    pred = appBC(target, pred)
                if log:
                    # If log has been applied on the data then we reverse it before displaying the values
                    pred = dis.reverse(pred)
                    target = dis.reverse(target)
                    input_ = dis.reverse(input_)
                # Filtering out irrelevant concentration values
                pred = Filter(pred)
                target = Filter(target)
                # Using the quantiles to compute the Quantized Error
                q1, q2, q3 = torch.quantile(target, 0.25), torch.quantile(target, 0.5), torch.quantile(target, 0.75)
                ct = torch.where(target > q1, 1, 0) + torch.where(target > q2, 1, 0) + torch.where(target > q3, 1, 0)
                cp = torch.where(pred > q1, 1, 0) + torch.where(pred > q2, 1, 0) + torch.where(pred > q3, 1, 0)
                ce = torch.mean(torch.abs(ct-cp).float())
                # relative MAE
                r_loss = torch.mean(torch.sqrt(self.criterion(pred, target))) / torch.mean(target)
                r_losses.append(r_loss.item())
                # absolute MAE
                loss = torch.mean(torch.sqrt(self.criterion(pred, target)))
                # absolute MBE
                mbe = torch.mean(pred - target)
                # relative MBE
                mbe_r = mbe / torch.mean(target)
                # A bit of processing is necessary to compute the FSIM
                predF = torch.nan_to_num((pred - torch.min(pred)), nan=torch.nan, posinf=newMax, neginf=-newMax)
                predF = ((predF > 0) * (predF < newMax)) * predF + (predF >= newMax) * newMax
                targetF = torch.nan_to_num((target - minTarg), nan=torch.nan, posinf=newMax, neginf=-newMax)
                targetF = ((targetF > 0) * (targetF < newMax)) * targetF + (targetF >= newMax) * newMax
                fsim_loss = torch.mean(fsimLoss(predF, targetF))
                if QA:
                    AQQ = AirQualityQuantif()
                    predq = AQQ(pred[0])
                    targetq = AQQ(target[0])
                    qa = torch.mean(torch.abs(predq - targetq))
                    qas.append(qa.item())
                    if unfilter:
                        sm = matplotlib.cm.ScalarMappable(norm=None, cmap=plt.get_cmap("jet"))
                        path = "results/" + self.name_model + "/results_AirQuality_" + testDateList[idx_batch].strftime(
                            "%d-%m-%Y_%Hh") + ".pdf"
                        prep = PlotPrep()
                        cm = confusion_matrix(y_true=torch.flatten(targetq).detach().cpu().numpy(), y_pred=torch.flatten(predq).detach().cpu().numpy())
                        predQP = prep(predq)
                        targetQP = prep(targetq)
                        ax0 = plt.subplot(2, 2, 1)
                        ax0.set_title("Air Quality - target")
                        ax0.imshow(targetQP)
                        plt.colorbar(sm, ax=ax0,
                                     ticks=np.linspace(torch.min(targetq).to(cpu), torch.max(targetq).to(cpu), 5),
                                     orientation="vertical", label="no units (Air Quality Classes)")
                        ax1 = plt.subplot(2, 2, 2)
                        ax1.set_title("Air Quality - prediction")
                        ax1.imshow(predQP)
                        ax2 = plt.subplot(2, 2, 3)
                        ax2.set_title("Air Quality - confusion matrix")
                        ax2.imshow(cm, cmap=plt.get_cmap("jet"))
                        plt.colorbar(sm, ax=ax2,
                                     ticks=np.linspace(np.min(cm), np.max(cm), 5),
                                     orientation="vertical", label="nb of samples (including pixels)")
                        plt.savefig(path)
                        plt.close()
                if save:
                    # Saving images and metrics for this specific sample
                    self.saveOne2D(target, pred, loss, input_, r_loss, ct, cp, ce, mbe, mbe_r, fsim_loss, testPeriod=None, date=testDateList[idx_batch], error=error, e_bw=e_bw, unfilter=unfilter, minTarg=minTarg, maxTarg=maxTarg)
                # Saving metrics in a list
                times.append(pred_time)
                closs.append(ce.item())
                losses.append(loss.item())
                MBE.append(mbe.item())
                MBE_R.append(mbe_r.item())
                sim_idces.append(fsim_loss.item())
        if save:
            path = "results/" + self.name_model + "/MAE_evolution.jpg"
            # Plotting the evolution of the MAE during testing
            plt.suptitle("MAE per sample number")
            fig, ax = plt.subplots(1, 1)
            ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y-%H'))
            ax.xaxis.set_major_locator(mdates.DayLocator(interval=100))
            ax.plot(testDates, r_losses)
            plt.savefig(path)
            # Saving a dictionary of metrics representing the model's performance
            dict_perf = {"Average inference time (s)": np.mean(times),
                         "STD of inference time (s)":np.std(times),
                         "Average prediction error (\u03BCg/m\N{superscript three})": np.mean(losses),
                         "STD of prediction error  (\u03BCg/m\N{superscript three})": np.std(losses),
                         "Average relative error (%)": 100 * np.mean(r_losses),
                         "STD of relative error (%)": 100 * np.std(r_losses),
                         "Average classification error": np.mean(closs),
                         "STD of classification error": np.std(closs),
                         "Average bias error (\u03BCg/m\N{superscript three})": np.mean(MBE),
                         "STD of bias error (\u03BCg/m\N{superscript three})": np.std(MBE),
                         "Average relative bias error (%)": 100 * np.mean(MBE_R),
                         "STD of relative bias error (%)": 100 * np.std(MBE_R),
                         "Average FSIM loss (%)": 100 * np.mean(sim_idces),
                         "STD of FSIM loss (%)": 100 * np.std(sim_idces)
                         }
            if QA:
                dict_perf["Average QA loss (%)"] = 100 * np.mean(qas)/6
                dict_perf["STD of QA loss (%)"] = 100 * np.std(qas)/6
            with open("results/" + self.name_model + "/perf_test.txt", "w+") as f:
                json.dump(dict_perf, f, indent=2)
                f.close()

########################################################################################################################

# Create ED Model
# class ED_2Dto2D(ED):
#     def __init__(self, name_model, device, linear_layer, latent_dim, ws, bcType, hardBC, p, kS1, kS2, kS3, kS4, chan_mult, lr, simLoss, height, width, stride=5, inp_chan=1, origData='Cams'):
#         super().__init__(name_model, device, ws, bcType, hardBC, p, simLoss, origData)
#         self.linear_layer = linear_layer
#         self.stride = stride
#         lh = math.ceil(height/(self.stride**2))
#         lw = math.ceil(width/(self.stride**2))
#         self.block1 = nn.Sequential(*self.blockC2d(inp_chan, chan_mult, kernel_size=kS1))
#         self.block2 = nn.Sequential(*self.blockC2d(chan_mult, chan_mult ** 2, kernel_size=kS2, stride=self.stride))
#         self.block3 = nn.Sequential(*self.blockC2d(chan_mult ** 2, chan_mult ** 3, kernel_size=kS3, stride=self.stride))
#         self.block4 = nn.Sequential(*self.blockC2d(chan_mult ** 3, chan_mult ** 4, kernel_size=kS4))
#         self.blockNeg4 = nn.Sequential(*self.blockC2d(chan_mult ** 4, chan_mult ** 3, kernel_size=kS4))
#         self.blockNeg3 = nn.Sequential(*self.blockC2d(chan_mult ** 3, chan_mult ** 2, kernel_size=kS3))
#         self.blockNeg2 = nn.Sequential(*self.blockC2d(chan_mult ** 2, chan_mult, kernel_size=kS2))
#         self.blockNeg1 = nn.Sequential(*self.blockC2d(chan_mult, 1, kernel_size=kS1))
#         self.blockLinEnc1 = nn.Sequential(*self.blockLinear((chan_mult ** 4) * lh * lw, latent_dim * 2))
#         self.blockLinEnc2 = nn.Sequential(*self.blockLinear(latent_dim * 2, latent_dim))
#         self.blockLinDec2 = nn.Sequential(*self.blockLinear(latent_dim, latent_dim * 2))
#         self.blockLinDec1 = nn.Sequential(*self.blockLinear(latent_dim * 2, (chan_mult ** 4) * lh * lw))
#
#         self.lr = lr
#         self.criterion = nn.MSELoss(reduction='none')
#         self.optimizer = optim.Adam(self.parameters(), lr=lr)
#
#     def forward(self, x):
#         # Encoding
#         y = self.block1(x)
#         shape1 = y.shape
#         y = self.block2(y)
#         shape2 = y.shape
#         y = self.block3(y)
#         # Else we just predict the latent representation
#         # (in that case, y is not yet the latent representation)
#         y = self.block4(y)
#         if self.linear_layer:
#             shapeF = y.shape
#             y = y.view(shapeF[0], -1)
#             y = self.blockLinEnc1(y)
#             # We just predict the latent representation
#             y = self.blockLinEnc2(y)
#         # encoded = y
#         # Decoding
#             y = self.blockLinDec2(y)
#             y = self.blockLinDec1(y)
#             y = y.view(shapeF)
#         y = self.blockNeg4(y)
#         y = self.blockNeg3(y)
#         y = torch.nn.Upsample(size=(shape2[2], shape2[3]), mode="bilinear")(y)
#         y = self.blockNeg2(y)
#         y = torch.nn.Upsample(size=(shape1[2], shape1[3]), mode="bilinear")(y)
#         decoded = self.blockNeg1(y)
#         return decoded
#
#     def predict(self, x):
#         return self.forward(x)
