# Importing libraries and modules
import os
import json
import matplotlib.pyplot as plt
import matplotlib
# PyTorch libraries and modules
import torch
import torch.nn as nn
import warnings
matplotlib.use('Agg')
plt.ioff()

cuda = torch.cuda.is_available()
device_name = "cuda" if cuda else "cpu"
device = torch.device(device_name)

def convert(seconds):
    days = seconds // (24*3600)
    seconds %= (24 * 3600)
    hours = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
    return "%d days %d hours %02d minutes %02d seconds" % (days, hours, minutes, seconds)

class baseModel(nn.Module):
    """ This class is supposed to be an abstract class for all our models.
        As such, it should never be called."""

    def __init__(self, name_model, device):
        super().__init__()
        self.name_model = name_model
        if 'Al-LOCAL' in self.name_model:
            sourceFolder = "savedModels/Al-Local/"
        elif 'Al-EUR' in self.name_model:
            sourceFolder = "savedModels/Al-Europe/"
        elif 'CamsFC' in self.name_model:
            sourceFolder = "savedModels/CamsFC/"
        else:
            sourceFolder = "savedModels/Cams/"
        # the folder where we save the models depends on the chosen data source
        self.sourceFolder = sourceFolder
        self.device = device

    def loadModel(self, name_GAN=""):
        """
            This function allows us to load a previously saved model.
            Warning : the object used as input and the previously saved model must belong to the same class and have the same parameters.
        """
        if name_GAN == "":
            folder = self.sourceFolder+self.name_model
        else:
            # if the model is a GAN, then the model folder is contained within the GAN folder
            fold1 = self.sourceFolder+name_GAN
            folder = fold1+"/"+self.name_model
        # loading the model parameters and optimizer state
        self.load_state_dict(torch.load(folder + "/state_dict.pkl", map_location=device))
        self.optimizer.load_state_dict(torch.load(folder + "/optim_dict.pkl", map_location=device))
        with open(folder + "/losses_train.txt", "r") as fTrain:
            losses_train = json.load(fTrain)
            fTrain.close()
        # loading the train losses as well so we can check the training process too
        return losses_train

    def saveModel(self, loss_train, train_time, name_GAN=""):
        """
                This function allows us to save our model parameters as well as the train losses.
        """
        if name_GAN == "":
            folder = self.sourceFolder+self.name_model
            # creating the folder to save the model
            os.makedirs(folder + "/", exist_ok=True)
        else:
            # if the model is a GAN, then the model folder is contained within the GAN folder
            fold1 = self.sourceFolder+name_GAN
            os.makedirs(fold1 + "/", exist_ok=True)
            folder = fold1+"/"+self.name_model
            # creating the folder to save the model
            os.makedirs(folder + "/", exist_ok=True)
        # saving the model parameters and optimizer state
        with open(folder + "/state_dict.pkl", "wb") as fModel:
            torch.save(self.state_dict(), fModel)
            fModel.close()
        with open(folder + "/optim_dict.pkl", "wb") as fOptim:
            torch.save(self.optimizer.state_dict(), fOptim)
            fOptim.close()
        # Plotting the losses
        plt.title("Training loss(es)")
        x = loss_train["Iteration"]
        # Saving the losses in a .txt file, in a more readable format
        losses_disp = [{"Iteration": i} for i in loss_train["Iteration"]]
        print("loss_train keys (except iteration) : \n", list(loss_train.keys())[1:])
        for k in list(loss_train.keys())[1:]:
            # Plotting
            plt.plot(x, loss_train[k], label=k)
            for i in loss_train["Iteration"]:
                # Writing a new dict of losses, easier to read
                losses_disp[i][k] = loss_train[k][i]
        # back to plotting the losses and saving the plot
        plt.semilogy(base=10)
        plt.xlabel("Iterations")
        plt.ylabel("Loss value")
        plt.legend()
        plt.savefig(folder + "/fig_losses_train.pdf")
        plt.close()
        # saving the train losses dictionary
        with open(folder + "/losses_train.txt", "w+") as fTrain:
            json.dump(losses_disp, fTrain, indent=2)
            fTrain.close()
        dict_time = {"Training time": convert(train_time)}
        # saving the training times as well
        with open(folder + "/training_time.txt", "w+") as f:
            json.dump(dict_time, f, indent=2)
            f.close()
        # deleting the checkpoint as it is no longer needed
        os.remove(folder + "/checkpoint.pl")
        print("Model and losses saved !")

    def saveCheckpoint(self, epoch, loss, losses, train_time, name_GAN=""):
        """ This method allows us to save our model during training in order to resume training later.
            This may be necessary if we do not have time to end the training in one run (depends on the allocated time)."""
        if name_GAN == "":
            folder = self.sourceFolder+self.name_model
        else:
            # if the model is a GAN, then the model folder is contained within the GAN folder
            fold1 = self.sourceFolder+name_GAN
            os.makedirs(fold1 + "/", exist_ok=True)
            folder = fold1+"/"+self.name_model
        # creating the folder to save the model checkpoint
        os.makedirs(folder + "/", exist_ok=True)
        with open(folder + "/checkpoint.pl", "wb") as fCheck:
            # saving all useful parameters of the model as a dictionary
            torch.save({
                'epoch': epoch,
                'loss': loss,
                'losses': losses,
                'train_time': train_time,
                'bc': getattr(self, 'bc', None),
                'model_state_dict': self.state_dict(),
                'opt_state_dict': self.optimizer.state_dict()
            }, fCheck)
            fCheck.close()

    def loadCheckpoint(self, name_GAN=""):
        """ This method allows us to load a previously saved checkpoint of a model in order to continue training."""
        if name_GAN == "":
            folder = self.sourceFolder+self.name_model
        else:
            # if the model is a GAN, then the model folder is contained within the GAN folder
            fold1 = self.sourceFolder+name_GAN
            folder = fold1+"/"+self.name_model
        checkpoint = torch.load(folder + "/checkpoint.pl")
        # checkpoint is loaded as a dictionary
        # getting all parameters from it
        self.load_state_dict(checkpoint['model_state_dict'])
        self.optimizer.load_state_dict(checkpoint['opt_state_dict'])
        # Getting current loss, list of losses, current epoch and training time
        # This allows for the training to restart easily
        loss = checkpoint['loss']
        epoch = checkpoint['epoch']
        losses = checkpoint['losses']
        try:
            train_time = checkpoint['train_time']
        except:
            warnings.warn("train_time value not saved in this file")
            train_time = 0
        try:
            bc = checkpoint['bc']
        except:
            warnings.warn("bc value not saved in this file")
            bc = None
        if bc is not None:
            self.bc = bc
        return [epoch, loss, losses, train_time]
