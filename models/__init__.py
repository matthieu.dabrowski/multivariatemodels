from .encoderdecoders import ED
from .unets import UNet_2Dto2D, FFUNet_2Dto2D, DFUNet_2Dto2D, HybridUNet1FF, HybridUNet2FF
from .GANs import CGAN, Cls_2Dto2D, FFCls_2Dto2D, DFCls_2Dto2D