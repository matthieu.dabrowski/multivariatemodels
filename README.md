Code corresponding to the paper : (include DOI)

Files kriging.py, ML.py and LinearPred.py correspond to baseline methods used as comparison.

File Aod2SfcAer_2DED.py is used for all experiments on models using several variables.

command_Aladin.txt and command_Cams.txt contain all commands for experiments on the Aladin and Cams datasets, respectively. Less experiments were realised on Aladin than on Cams, as they take a very long time.

Regarding datasets paths, the Aladin dataset is considered to be located in the AeroClim2 subfolder of the current folder, and the Cams dataset in the Cams subfolder. However they could not be included in this repository because of the large size of the corresponding files. The data can be found at : (include link/DOI)
